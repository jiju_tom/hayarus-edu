<?php
App::uses('UsereventsController', 'Controller');

/**
 * UsereventsController Test Case
 *
 */
class UsereventsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.userevent',
		'app.user',
		'app.group',
		'app.expense',
		'app.invoice',
		'app.invoiceitem',
		'app.scholorship',
		'app.college',
		'app.collegecourse',
		'app.course',
		'app.areaofstudy',
		'app.courseexpense',
		'app.studentreference',
		'app.collegeimage'
	);

/**
 * testAdminIndex method
 *
 * @return void
 */
	public function testAdminIndex() {
		$this->markTestIncomplete('testAdminIndex not implemented.');
	}

/**
 * testAdminView method
 *
 * @return void
 */
	public function testAdminView() {
		$this->markTestIncomplete('testAdminView not implemented.');
	}

/**
 * testAdminAdd method
 *
 * @return void
 */
	public function testAdminAdd() {
		$this->markTestIncomplete('testAdminAdd not implemented.');
	}

/**
 * testAdminEdit method
 *
 * @return void
 */
	public function testAdminEdit() {
		$this->markTestIncomplete('testAdminEdit not implemented.');
	}

/**
 * testAdminDelete method
 *
 * @return void
 */
	public function testAdminDelete() {
		$this->markTestIncomplete('testAdminDelete not implemented.');
	}

/**
 * testAdminExport method
 *
 * @return void
 */
	public function testAdminExport() {
		$this->markTestIncomplete('testAdminExport not implemented.');
	}

}
