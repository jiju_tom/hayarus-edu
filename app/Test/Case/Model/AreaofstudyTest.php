<?php
App::uses('Areaofstudy', 'Model');

/**
 * Areaofstudy Test Case
 *
 */
class AreaofstudyTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.areaofstudy',
		'app.course'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Areaofstudy = ClassRegistry::init('Areaofstudy');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Areaofstudy);

		parent::tearDown();
	}

}
