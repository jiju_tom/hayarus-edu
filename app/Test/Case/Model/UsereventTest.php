<?php
App::uses('Userevent', 'Model');

/**
 * Userevent Test Case
 *
 */
class UsereventTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.userevent',
		'app.user',
		'app.group',
		'app.expense',
		'app.invoice',
		'app.invoiceitem',
		'app.scholorship',
		'app.college',
		'app.collegecourse',
		'app.course',
		'app.areaofstudy',
		'app.courseexpense',
		'app.studentreference',
		'app.collegeimage'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Userevent = ClassRegistry::init('Userevent');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Userevent);

		parent::tearDown();
	}

}
