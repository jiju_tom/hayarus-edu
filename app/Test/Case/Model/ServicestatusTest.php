<?php
App::uses('Servicestatus', 'Model');

/**
 * Servicestatus Test Case
 *
 */
class ServicestatusTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.servicestatus',
		'app.business',
		'app.invoice',
		'app.user',
		'app.group',
		'app.expense',
		'app.scholorship',
		'app.college',
		'app.collegecourse',
		'app.course',
		'app.areaofstudy',
		'app.courseexpense',
		'app.studentreference',
		'app.collegeimage',
		'app.invoiceitem'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Servicestatus = ClassRegistry::init('Servicestatus');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Servicestatus);

		parent::tearDown();
	}

}
