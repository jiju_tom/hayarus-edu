<?php
App::uses('Busnisesstype', 'Model');

/**
 * Busnisesstype Test Case
 *
 */
class BusnisesstypeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.busnisesstype'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Busnisesstype = ClassRegistry::init('Busnisesstype');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Busnisesstype);

		parent::tearDown();
	}

}
