<?php
App::uses('College', 'Model');

/**
 * College Test Case
 *
 */
class CollegeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.college',
		'app.collegecourse',
		'app.course',
		'app.areaofstudy',
		'app.courseexpense',
		'app.invoice',
		'app.location',
		'app.city',
		'app.state',
		'app.country',
		'app.student',
		'app.status',
		'app.user',
		'app.group',
		'app.expense',
		'app.studentreference',
		'app.collegeimage'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->College = ClassRegistry::init('College');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->College);

		parent::tearDown();
	}

}
