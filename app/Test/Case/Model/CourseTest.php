<?php
App::uses('Course', 'Model');

/**
 * Course Test Case
 *
 */
class CourseTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.course',
		'app.areaofstudy',
		'app.college',
		'app.courseexpense',
		'app.invoice',
		'app.location',
		'app.country',
		'app.state',
		'app.city',
		'app.student',
		'app.status',
		'app.user',
		'app.group',
		'app.expense',
		'app.studentreference'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Course = ClassRegistry::init('Course');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Course);

		parent::tearDown();
	}

}
