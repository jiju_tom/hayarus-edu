<?php
App::uses('Businesstype', 'Model');

/**
 * Businesstype Test Case
 *
 */
class BusinesstypeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.businesstype',
		'app.business',
		'app.invoice',
		'app.user',
		'app.group',
		'app.expense',
		'app.scholorship',
		'app.college',
		'app.collegecourse',
		'app.course',
		'app.areaofstudy',
		'app.courseexpense',
		'app.studentreference',
		'app.collegeimage',
		'app.invoiceitem'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Businesstype = ClassRegistry::init('Businesstype');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Businesstype);

		parent::tearDown();
	}

}
