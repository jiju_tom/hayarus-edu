<?php $this->Html->addCrumb('Scholarship Registrations', '/admin/users/scholarshipregistration'); ?>
<?php $this->Html->addCrumb('View', ''); ?>
<div style='padding-bottom:10px;'><?php echo $this->Session->flash(); ?></div>

<div class="row">
    <div class="col-md-12">
        <div class="tab-content">
            <div class="tab-pane active" id="tab_0">
                 <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                        	<i class="fa fa-reorder"></i><?php echo __('View'); ?>                        				</div>
	                        <div class="tools">
	                            <a href="javascript:;" class="collapse"></a>
	                        </div>
	                    </div>
                    	<div class="portlet-body form users">
		                    <div class="alert alert-danger display-hide">
		                        <button data-close="alert" class="close"></button>
		                        You have some form errors. Please check below.
		                    </div>
		                    <!-- BEGIN FORM-->
		                    <?php echo $this->Form->create('User', array('class' => 'form-horizontal')); ?>
		                    <div class="form-body">                      
		                        						
								<div class='form-group'>
									 <?php echo $this->Form->label('name:' ,null, array('class' => 'col-md-3 control-label')); ?>
									<div class='col-md-4'>
										<p class="form-control-static"><?php echo h($user['User']['first_name']);?></p>
									</div>
								</div>

								<div class='form-group'>
									 <?php echo $this->Form->label('phone:' ,null, array('class' => 'col-md-3 control-label')); ?>
									<div class='col-md-4'>
										<p class="form-control-static"><?php echo h($user['User']['phone']);?></p>
									</div>
								</div>
								
								<div class='form-group'>
									 <?php echo $this->Form->label('email:' ,null, array('class' => 'col-md-3 control-label')); ?>
									<div class='col-md-4'>
										<p class="form-control-static"><?php echo h($user['User']['email']);?></p>
									</div>
								</div>

								<div class='form-group'>
									 <?php echo $this->Form->label('code:' ,null, array('class' => 'col-md-3 control-label')); ?>
									<div class='col-md-4'>
										<p class="form-control-static"><?php echo h($user['User']['code']);?></p>
									</div>
								</div>

								
								<div class="tabbable tabbable-custom tabbable-custom-profile">
											<ul class="nav nav-tabs">
												<li class="active">
													<a href="#tab_1_11" data-toggle="tab">Referrals</a>
												</li>
												
											</ul>
											<div class="tab-content">

												<div class="table-responsive">
									                <table class="table table-striped table-bordered table-hover " id="sample_1">
									                  <thead>
									                      <tr>
									                        <th>Name</th>  
									                        <th>Phone</th>
									                        <th>Email</th>
									                        <th>Place</th>
									                        <th>Code</th>
									                        
									                      </tr>
									                  </thead>
									                  
									                  <tbody>
										                  <?php if(!empty($refusers)){ 

										                   foreach ($refusers as $key => $value) { ?>
											                  <tr>
										                     		<td><?php echo $value['User']['first_name']; ?></td>
										        					<td>
																	<?php echo $value['User']['phone']; ?>
												             		</td>
										                  			<td>
																	<?php echo $value['User']['email']; ?>
												              		</td>
												              		<td>
																	<?php echo $value['User']['school']; ?>
												              		</td>
												              		<td>
																	<?php echo $value['User']['code']; ?>
												              		</td>
											                  </tr>
														  <?php } 
																}else{ ?>


																<tr><td colspan='7' align='center'>No Records Found.</td></tr>
															<?php } ?>		
									                   
									                  </tbody>
									                </table>
									              </div>

												
											</div>
								</div>
		                        
		                        

								<div class="form-actions fluid">
		                            <div class="col-md-offset-3 col-md-9">
		                                <button type="button" class="btn blue pull-right" onclick="window.location='<?php echo $this->webroot.'admin/users/scholarshipregistration'; ?>'">Go Back</button>
		                            </div>
		                        </div>

		            		</div>
		                        <?php echo $this->Form->end(); ?>
		                        <!-- END FORM-->
		                    </div>
                	</div>
            </div>
        </div>
    </div>
</div>