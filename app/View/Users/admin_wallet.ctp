<?php $this->Html->addCrumb('My Wallet', '/admin/users'); ?>
<?php $this->Html->addCrumb('Summary', ''); ?>
<?php $groupId = $this->Session->read('Auth.Group.id'); ?>
<div style='padding-bottom:10px;'><?php //echo $this->Session->flash(); ?></div>
<style type="text/css">
.nav {
	padding-left: 9px!important;
}
</style>

<!-- BEGIN PAGE CONTENT-->
			<div class="row profile">
				<div class="col-md-12">
					<?php if($groupId == 5 && $scholorshipCount < 1){ ?>
					<div class="alert alert-warning">
						<strong> You can only apply in one college. once applied, can't redo.</strong>
					</div>
					<?php } ?>
					<!--BEGIN TABS-->
					<div class="tabbable tabbable-custom tabbable-full-width">
						<!-- <ul class="nav nav-tabs">
							<li class="active">
								<a href="#tab_1_1" data-toggle="tab">Overview</a>
							</li>
							
						</ul> -->
						<div class="tab-content">
							<div class="tab-pane active" id="tab_1_1">
								<div class="row">
									<div class="col-md-3">
										<ul class="list-unstyled profile-nav">
											<li>
												<img src="<?php echo $this->webroot; ?>assets/img/profile/profile-img.png" class="img-responsive" alt=""/>
												
											</li>
											
										</ul>
									</div>
									<div class="col-md-9">
										<div class="row">
											<div class="col-md-7 profile-info">
												<h1><?php echo $user['User']['first_name']; ?> &nbsp; <?php echo $user['User']['last_name']; ?></h1>
												<p>
													<i class="fa fa-user"></i> &nbsp; <?php echo $user['User']['email']; ?>
												</p>
												<p>
													<i class="fa fa-phone"></i> &nbsp; <?php echo $user['User']['phone']; ?>
												</p>
												<p>
													<i class="fa fa-key"></i> &nbsp; <?php echo $user['User']['school']; ?>
												</p>


												
											</div>
											<!--end col-md-8-->
											<div class="col-md-5">
												<div class="portlet box red">
													<div class="portlet-title">
														<div class="caption">
															<i class="fa fa-reorder"></i>Scholarship Summary
														</div>
														<div class="tools">
															<a href="javascript:;" class="collapse"></a>
														</div>
													</div>
													<div class="portlet-body">
														<dl>
															<dt>Referral code</dt>
															<dt style="border-bottom:#ccc 1px solid;"><h2> <?php echo $user['User']['code']; ?> </h2></dt>
														</br>
															<dt>Wallet balance</dt>
															<dt><h1> <i class="fa fa-inr" aria-hidden="true" style="font-size: 30px;"></i>10000</h1></dt>
															<dd></dd>
														</dl>
													</div>
												</div>
												
											</div>
											<!--end col-md-4-->
										</div>
										<!--end row-->
										
									</div>
								</div>
							</div>
							
						</div>
					</div>
					<!--END TABS-->

					<div class="tabbable tabbable-custom tabbable-custom-profile">
						<ul class="nav nav-tabs">
							<li class="active">
								<a href="#tab_1_11" data-toggle="tab">My Applications</a>
							</li>
							
						</ul>
						<div class="tab-content">

							<div class="table-responsive">
					            <table class="table table-striped table-bordered table-hover " id="sample_1">
					              <thead>
					                  <tr>
					                    <th>College</th>  
					                    <th>Course</th>
					                    <th>Status</th>
					                    
					                  </tr>
					              </thead>
					              
					              <tbody>
					                  <?php if(!empty($user['Scholorship'])){

					                   foreach ($user['Scholorship'] as $key => $value) { ?>
						                  <tr>
					                     		<td><?php echo $colleges[$value['college_id']]; ?></td>
					        					<td>
												<?php echo $courses[$value['course_id']]; ?>
							             		</td>
					                  			<td>
												<?php echo $scholorship_status[$value['status']]; ?>
							              		</td>
						                  </tr>
									  <?php } 
											}else{ ?>


											<tr><td colspan='7' align='center'>No Records Found.</td></tr>
										<?php } ?>		
					               
					              </tbody>
					            </table>
					          </div>

							<?php if($groupId == 5 && $scholorshipCount == 1){  ?>
								<div class="alert alert-success ">
								<strong> Thank you. You will get a call from the college co-operative office once it reviewed. Share your Referral code (<?php echo $user['User']['code']; ?>) with the team to redeem the scholarship.</strong>
								</div>
							<?php  } ?>
						</div>
					</div>

					<div class="tabbable tabbable-custom tabbable-custom-profile">
											<ul class="nav nav-tabs">
												<li class="active" style="border-top: 3px solid #30cc4b;">
													<a href="#tab_1_11" data-toggle="tab">My Referrals</a>
												</li>
												
											</ul>
											<div class="tab-content">

												<div class="table-responsive">
									                <table class="table table-striped table-bordered table-hover " id="sample_1">
									                  <thead>
									                      <tr>
									                        <th>Name</th>  
									                        <th>Place</th>
									                        <th>Status</th>
									                        
									                      </tr>
									                  </thead>
									                  
									                  <tbody>
										                  <?php if(!empty($refusers)){

										                   foreach ($refusers as $key => $value) { ?>
											                  <tr>
										                     		<td><?php echo $value['User']['first_name']; ?></td>
										        					<td>
																	<?php echo $value['User']['school']; ?>
												             		</td>
										                  			<td>
																	<?php if(!empty($value['Scholorship'])){ echo $scholorship_status[$value['Scholorship'][0]['status']]; } else{ echo "Not Applied"; } ?>
												              		</td>
											                  </tr>
														  <?php } 
																}else{ ?>


																<tr><td colspan='7' align='center'>No Records Found.</td></tr>
															<?php } ?>		
									                   
									                  </tbody>
									                </table>
									              </div>

												<!-- <?php if($groupId == 5 && $scholorshipCount == 1){  ?>
													<div class="alert alert-success ">
													<strong> Thank you. You will get a call from the college co-operative office once it reviewed. Share your Referral code (<?php echo $user['User']['code']; ?>) with the team to redeem the scholarship.</strong>
													</div>
												<?php  } ?> -->
											</div>
										</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->

			