<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Find Your Dream Course and Apply in One Click</title>
    <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>registration/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>registration/css/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>registration/css/iofrm-style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>registration/css/iofrm-theme4.css">

<style>
    .overlay{
        display: none;
        position: fixed;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        z-index: 999;
        background: rgba(255,255,255,0.8) url("<?php echo $this->webroot; ?>assets/img/ajax-loading.gif") center no-repeat;
    }
    body{
        text-align: center;
    }
    /* Turn off scrollbar when body element has the loading class */
    body.loading{
        overflow: hidden;   
    }
    /* Make spinner image visible when body element has the loading class */
    body.loading .overlay{
        display: block;
    }
</style>    
</head>
<body>
    <div class="form-body">
        <div class="website-logo">
            <a href="index.html">
                <div class="logo1">
                    <!-- <img class="" src="<?php echo $this->webroot; ?>img/logo-png.png" alt="" width="100%"> -->
                </div>
            </a>
        </div>
        <div class="row">

            <div class="img-holder" style='text-align:center;background-image: url(<?php echo $this->webroot; ?>registration/images/graphic2.png); background-size: cover;'>
                <div class="bg"></div>
                <div class="info-holder">
                    <!-- <img src="<?php echo $this->webroot; ?>registration/images/graphic2.png" alt=""> -->
                </div>
            </div>

            <div class="form-holder" id="form-holder1" >
                <div class="form-content">
                  <div class="overlay"></div>
                    <div class="form-items">
                        <h3>Three Simple Steps.</h3>
                        <p><span style="color: #52c034;">Register |</span> <span style="color: #e0e729;">Find your College |</span><span style="color: #e77929;"> Apply </span></p>
                        <div class="page-links">
                             <a href="##" class="active register" >Register</a> <a href="##" class="login" >Login</a>
                        </div>

                        <div class="alert alert-danger alert-dismissable" bis_skin_checked="1" style="display: none;" id="errorMsg1"><button type="button" class="close" data-dismiss="alert" aria-hidden="true" ></button>Incorrect email or password, try again.</div>

                        <?php echo $this->Session->flash(); ?>
                        
                        <?php echo $this->Form->create('User', array('class' => 'login-form','id'=>'regForm','role' => 'form', 'url' => array('controller' => 'users', 'action' => 'admin_registration'))); ?>

                            <input class="form-control" type="text" name="data[User][first_name]" placeholder="Full Name" id="name" required>
                            <input class="form-control" type="email" name="data[User][email]" placeholder="Email" id="regemail" required>
                            <input class="form-control" type="tel"  name="data[User][phone]" placeholder="Phone (eg: 8907556150)" pattern="[1-9]{1}[0-9]{9}" id="phone" maxlength="10" required>
                            <input class="form-control" type="text" name="data[User][school]" placeholder="District" id="school" required>
                            <input class="form-control" type="password" name="data[User][password]" placeholder="Password" id="newpassword" required>
                            <input class="form-control" type="text" name="data[User][referral_code]" placeholder="Referral Code" id="referral_code" required>
                            <div class="form-button">
                                <button id="submitReg" type="button" class="ibtn">Register to Find College</button>
                            </div>

                        <?php echo $this->Form->end(); ?>


                        


                        <div class="other-links">
                            <!-- <span>Or register with</span><a href="#">Facebook</a><a href="#">Google</a><a href="#">Linkedin</a> -->
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-holder" id="form-holder2" style="display: none;">
                <div class="form-content">
                  <div class="overlay"></div>
                    <div class="form-items">
                        <h3>Three Simple Steps.</h3>
                        <p><span style="color: #52c034;">Login |</span> <span style="color: #e0e729;">Find your College |</span><span style="color: #e77929;"> Apply </span></p>
                        <div class="page-links">
                            <a href="##" class="active register" >Register</a> <a href="##" class="login" >Login</a>
                        </div>
                        
                        <div class="alert alert-danger alert-dismissable" bis_skin_checked="1" style="display: none;" id="errorMsg"><button type="button" class="close" data-dismiss="alert" aria-hidden="true" ></button>Incorrect email or password, try again.</div>

                        <?php echo $this->Form->create('User', array('class' => 'login-form','id'=>'LoginForm','role' => 'form', 'url' => array('controller' => 'users', 'action' => 'admin_login'))); ?>

                             <input class="form-control" type="email" name="data[User][email]" placeholder="Email" id="email" required>
                            <input class="form-control" type="password" name="data[User][password]" placeholder="Password" id="password" required>
                            <div class="form-button">
                                <!-- <button id="submitLogin" type="button" class="ibtn">Login</button> -->
                            <button  id="submitLogin" type="button" class="ibtn" data-loading-text="Loading..." >
                    Login </button>
                            </div>
                        </form>


                        


                        <div class="other-links">
                            <!-- <span>Or register with</span><a href="#">Facebook</a><a href="#">Google</a><a href="#">Linkedin</a> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script src="<?php echo $this->webroot; ?>registration/js/jquery.min.js"></script>
<script src="<?php echo $this->webroot; ?>registration/js/popper.min.js"></script>
<script src="<?php echo $this->webroot; ?>registration/js/bootstrap.min.js"></script>
<script src="<?php echo $this->webroot; ?>registration/js/main.js"></script>

<script type="text/javascript">

        $(document).on({
            ajaxStart: function(){
                $("body").addClass("loading"); 
            },
            ajaxStop: function(){ 
                $("body").removeClass("loading"); 
            }    
        });

        $(document).ready(function(){
            $("#submitReg").click(function(){
               var validate = validateForm();
               if(validate){
                   var crntName =  $("#regemail").val();     
                    $.ajax({
                         url: "<?php echo $this->webroot;?>users/checkEmail/"+crntName,            
                         cache: false,
                          success: function(html){//alert(html);
                            if(html == 1){      
                                alert('Email address already registered.');
                                //$("#UserEmail").val('');
                            }

                            if(html == 0){
                                $("#regForm").submit();
                            }
                        }
                    });
                }
            });

            $("#submitLogin").click(function(){ 
                $("#errorMsg").css("display", "none");
                var retValidate = validateLoginForm();
                if(retValidate){
                    var email = $("#email").val();
                    var password = $("#password").val();
                    $.ajax({
                        url: '<?php echo $this->webroot.'admin/users/loginnew'; ?>',
                        type: 'POST',
                        data: {"data[User][email]": email  , "data[User][password]": password},
                        success: function (r) {
                          
                          if(r== 0){
                           $("#errorMsg").removeAttr("style");
                          } else{

                             window.location.href = "admin/dashboard/index";
                          }
                            
                        },
                        cache: false
                    });
                }

            });

            $("#form-holder1").show();
            $("#form-holder2").hide();


            $(".register").click(function(){ //alert('register');
                $("#form-holder1").show();
                $("#form-holder2").hide();
                $(".register").addClass('active');
                $(".login").removeClass('active');
            });

            $(".login").click(function(){ //alert('login');
                $("#form-holder2").show();
                $("#form-holder1").hide();

                $(".register").removeClass('active');
                $(".login").addClass('active');
            });


            var input = document.getElementById("password");
            input.addEventListener("keyup", function(event) {
              // Number 13 is the "Enter" key on the keyboard
              if (event.keyCode === 13) {
                // Cancel the default action, if needed
                event.preventDefault();
                // Trigger the button element with a click
                document.getElementById("submitLogin").click();
              }
            });


            var input = document.getElementById("newpassword");
            input.addEventListener("keyup", function(event) {
              // Number 13 is the "Enter" key on the keyboard
              if (event.keyCode === 13) {
                // Cancel the default action, if needed
                event.preventDefault();
                // Trigger the button element with a click
                document.getElementById("submitReg").click();
              }
            });

             

          

        });

        function validateForm() { 
          var name = $("#name").val(); 
          var email = $("#regemail").val();
          var phone = $("#phone").val();
          var school = $("#school").val();
          var password = $("#newpassword").val();


          if (name == "") {
             $("#errorMsg1").html("Name required.");
             $("#errorMsg1").removeAttr("style");
            return false;
          }else if (!validateEmail(email)) {
             $("#errorMsg1").html("Email empty/ Invalid.");
             $("#errorMsg1").removeAttr("style");
            return false;
          }else if (!validatePhone(phone)) {
             $("#errorMsg1").html("Phone empty/ Invalid.");
             $("#errorMsg1").removeAttr("style");
            return false;
          }else if (school == "") {
             $("#errorMsg1").html("District required");
             $("#errorMsg1").removeAttr("style");
            return false;
          }else if (password == "") {
             $("#errorMsg1").html("Password  required");
             $("#errorMsg1").removeAttr("style");
            return false;
          }
          else{
            $("#errorMsg1").css("display", "none");
            return true;
          }


        }

        function validateLoginForm() { 
          var password = $("#password").val(); 
          var email = $("#email").val();
          if (password == "") {
             $("#errorMsg").html("Password required.");
             $("#errorMsg").removeAttr("style");
            return false;
          }else if (!validateEmail(email)) {
             $("#errorMsg").html("Email empty/ Invalid.");
             $("#errorMsg").removeAttr("style");
            return false;
          }
          else{
            return true;
          }

        }

        function validateEmail(email) {
           const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return (re.test(email));
        }

        function validatePhone(phone) { //Validates the phone number
            var phoneRegex = /^\d{10}$/g; // Change this regex based on requirement
            return phoneRegex.test(phone);
        }

        

    </script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-77R7PFHTE1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-77R7PFHTE1');
</script>

<!-- Modal -->
 <div class="modal fade" id="getCodeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-x: hidden; overflow-y: hidden;">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
       <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         <h4 class="modal-title" id="myModalLabel"></h4>
       </div>
       <div class="modal-body" id="getCode" style="text-align:center; background-image: url('<?php echo $this->webroot; ?>img/congrats.png'); background-size: cover;  height: 100%; widows: 100%; min-height: 500px; margin:0 auto; overflow-x: hidden; overflow-y: hidden;" >
        <!-- <img src="<?php echo $this->webroot; ?>img/congrats.png"/> -->
        
       </div>
    </div>
   </div>
 </div>

 
</body>
</html>