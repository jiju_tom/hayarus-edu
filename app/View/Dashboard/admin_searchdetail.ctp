<div style='padding-bottom:10px;'><?php// echo $this->Session->flash(); ?></div>
<br><br>
<?php $user = $this->Session->read('Auth.User');
$groupId = $this->Session->read('Auth.Group.id');
  //pr($scholorships);//exit; ?>
<style type="text/css">
.feeds .col2 .date{padding: 0px !important;}
a:hover, a:focus {
    text-decoration: none!important;
}
</style>

<script type="text/javascript">

	$(document).ready(function(){
		$('.apply_now').click(function() {
		    var course_id = $(this).attr("data-course-id");
		    var college_id = $(this).attr("data-college-id");
		    var user_id = $(this).attr("data-user-id");
		    var collegecourse_id = $(this).attr("data-collegecourse-id");

		    bootbox.confirm("Are you sure want to apply to this course?", function(result){ 
			    if(result){
			    	$.ajax({
                        url: '<?php echo $this->webroot.'admin/scholorships/add'; ?>',
                        type: 'POST',
                        data: {"data[Scholorship][course_id]": course_id  , "data[Scholorship][college_id]": college_id, "data[Scholorship][user_id]" : user_id, "data[Scholorship][collegecourse_id]" : collegecourse_id },
                        success: function (r) {
                          if(r== 1){
                           $("#link_apply_now"+course_id).addClass("disabled");

                           	location.reload();
                          }else if(r== 2){
                          	 bootbox.confirm("Your application submitted successfully!. Check your application status in 'My Admission Wallet'. Redirecting to My Admission Wallet", function(result){ 
                          	 	if(result){
                          	 	 	window.location.href = "<?php echo $this->webroot; ?>admin/users/wallet";
                          	 	}else{
                          	 		location.reload();
                          	 	}	

                          	 });
                          }
                            
                        },
                        cache: false
                    });
			    }
			});


		});
	});
	
</script>

<?php if($groupId == 5){  ?>
	<!--Start of Tawk.to Script-->
		<script type="text/javascript">
		var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
		(function(){
		var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
		s1.async=true;
		s1.src='https://embed.tawk.to/6083d35062662a09efc1a904/1f41f2sv8';
		s1.charset='UTF-8';
		s1.setAttribute('crossorigin','*');
		s0.parentNode.insertBefore(s1,s0);
		})();
		</script>
		<!--End of Tawk.to Script-->
<?php } ?>
		
	<!-- BEGIN CONTENT -->

			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12 blog-page">
					<div class="row">
						<?php if($groupId == 5 && $scholorshipCount == 1){  ?>
							<div class="alert alert-danger">
							<strong>   You've already applied. Check your application status in 'My Admission Wallet'.</strong>
							</div>
						<?php  }else if($groupId == 5 && $scholorshipCount < 1){ ?>
							<div class="alert alert-warning">
								<strong> You can only apply in one college. once applied, can't redo.</strong>
							</div>
						<?php  } ?>
						<div class="col-md-9 article-block">
							
							<div class="blog-tag-data">
								
								
								<div class="col-md-12" >
									<h2><?php echo $this->request->data['College']['name']; ?> </h2>
									<div class="tabbable tabbable-custom boxless">
										<div class="row mix-grid" id="imgPortfolio" >
											<?php  foreach ($this->request->data['Collegeimage'] as $key => $images) { ?>
											<div class="col-md-3 col-sm-4 mix category_1">
												<div class="mix-inner" style="height: 150px;">
													<img class="img-responsive" src="<?php echo $this->webroot.$ImagePath.$images['name']; ?>" alt="" >
													<div class="mix-details">
														<span class="img_id" style="display: none"><?php echo $images['id']; ?></span>
														<h4></h4>
														
														<a class="mix-preview fancybox-button" href="<?php echo $this->webroot.$ImagePath.$images['name']; ?>" title="<?php echo $this->request->data['College']['name']; ?>" data-rel="fancybox-button" style="left: 35%!important;"><i class="fa fa-search"></i></a>
													</div>
												</div>
											</div>
										<?php } ?>
										</div>
									</div>
								</div>
								
								<div class="row">
									<div class="col-md-6">
										<ul class="list-inline blog-tags">
											<li>
												<i class="fa fa-tags"></i>
												<a href="#"><?php echo h($this->request->data['College']['location_name']); ?>&nbsp;, <?php echo h($cities[$this->request->data['College']['city_id']]); ?>, <?php echo h($states[$this->request->data['College']['state_id']]); ?>, <?php echo $countries[$this->request->data['College']['country_id']]; ?></a>
												
											</li>
										</ul>
									</div>
									
								</div>
							</div>
							<!--end news-tag-data-->
							<div>
								<p>
									<?php echo $this->request->data['College']['description']; ?>
								</p>
								
							</div>
							<hr>
							<div class="media">
								<h3>Location Details</h3>
								<a href="#" class="pull-left">
								<img alt="" src="assets/img/blog/9.jpg" class="media-object">
								</a>
								<div class="media-body">
									<h4 class="media-heading"><?php echo h($this->request->data['College']['location_name']); ?>&nbsp;, <?php echo h($cities[$this->request->data['College']['city_id']]); ?>, <?php echo h($states[$this->request->data['College']['state_id']]); ?>, <?php echo $countries[$this->request->data['College']['country_id']]; ?>
									
									</h4>
									<p>
										<?php echo $this->request->data['College']['location_description']; ?>
									</p>
									<hr>
								</div>
							</div>
							<!--end media-->
							
							<div class="media">
								
								<div class="media-body">
									
									<!-- BEGIN SAMPLE TABLE PORTLET-->
								<div class="portlet box purple">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-cogs"></i>Courses Offered
										</div>
										<div class="tools">
											<a href="javascript:;" class="collapse"></a>
										</div>
									</div>
									<div class="portlet-body">
										<div class="table-scrollable">
											<table class="table table-striped table-bordered table-hover" id="courses">
											<thead>
											<tr>
												<th scope="col" style="width:450px !important">Course Name </th>

												<th scope="col">Duration</th>
												<th scope="col">Seats Left</th>
												
												<?php //if($groupId != 5){ ?>
												<th scope="col">College Type</th>
												<!-- <th scope="col">Admission Fee</th>
												<th scope="col">Fee Year 1</th>
												<th scope="col">Fee Year 2</th>
												<th scope="col">Fee Year 3</th>
												<th scope="col">Fee Year 4</th>
												<th scope="col">Fee Year 5</th>
												<th scope="col">Fee Year 6</th>
												<th scope="col">Internship</th> -->
												<th scope="col">Total Fee</th>
												<?php if($groupId != 4 && $groupId == 1){ ?>
												<th scope="col">Service Charge</th>
												<th scope="col">Service Charge Agent</th>
												<?php } ?>
												<?php //} ?>
												
											</tr>
											</thead>
											<tbody>
												<?php if (empty($this->request->data['Collegecourse'])) {   ?>

													<tr>
														<td colspan="12" align="center">No Course details found.</td>
													</tr>
												<?php } ?>

												<?php foreach ($this->request->data['Collegecourse'] as $key => $college_course) { //pr($college_course);?>
													
												<tr>

													<td class="course_name">
														<!--hiding popup for group5-->
														<?php //if($groupId != 5){ ?>
														<a data-toggle="modal" href="#responsive<?php echo $key; ?>">
														<?php //} ?>	

															<?php echo $college_course['Course']['name'] ?> 

															<?php if($groupId == 5){ ?><span class="label label-primary">View Details</span><?php } ?>
																
														<?php //if($groupId != 5){ ?>
														</a>
														<?php //} ?>

														<!--apply button-->
														<?php if($groupId == 5){ 
															
															$sizeofScholorshipArr = sizeof($scholorships);
															$disabled = ($sizeofScholorshipArr >= 3) ? 'disabled' : '';


															//pr($scholorships);
															if( !empty($scholorships)) {
															 	foreach ($scholorships as $skey => $value){
															 		if(isset($value) && $college_course['id'] == $value['Scholorship']['collegecourse_id'])
																	$disabled = 'disabled';
															 	}
																 
															}

															?>

															<?php if($groupId == 5 && $scholorshipCount < 1){  ?>
																&nbsp; <a class="btn default btn-xs red apply_now" data-college-id="<?php echo $college_course['college_id'] ?>" data-course-id="<?php echo $college_course['course_id'] ?>" data-user-id = "<?php echo $user['id']; ?>" data-collegecourse-id="<?php echo $college_course['id'] ?>" id ="link_apply_now<?php echo $college_course['course_id'] ?>" <?php echo $disabled; ?> > Apply Now !</a>
															<?php  } ?>	

															<!--end of apply button-->

														<?php } ?>
														</td>
													<td class="duration"><?php echo $college_course['Course']['duration'] ?></td>
													<td class="seat_left"><?php echo $college_course['seat_left'] ?></td>
													
													<?php //if($groupId != 5){ ?>

													<td><?php echo $college_type_dropdown[$this->request->data['Courseexpense'][$key]['gender']]; ?></td>

													<!-- <td class="admission_fee"><?php echo $this->request->data['Courseexpense'][$key]['admission_fee'] ?></td>
													<td class="year1" data-toggle="tooltip" data-placement="top" data-theme="dark" title="<?php echo $college_course['notes']; ?>" style= "cursor: pointer;"><?php echo $this->request->data['Courseexpense'][$key]['year1'] ?></td>
													<td class="year2"><?php echo $this->request->data['Courseexpense'][$key]['year2'] ?></td>
													<td class="year3"><?php echo $this->request->data['Courseexpense'][$key]['year3'] ?></td>
													<td class="year4"><?php echo $this->request->data['Courseexpense'][$key]['year4'] ?></td>
													<td class="year5"><?php echo $this->request->data['Courseexpense'][$key]['year5'] ?></td>
													<td class="year6"><?php echo $this->request->data['Courseexpense'][$key]['year6'] ?></td>
													<td class="internship"><?php echo $this->request->data['Courseexpense'][$key]['internship'] ?></td> -->
													<td class="total"><?php echo $this->request->data['Courseexpense'][$key]['total'] ?></td>
													<?php if($groupId != 4 && $groupId == 1){ ?>
														<td class="sc"><?php echo $college_course['college_sc'] ?></td>
														<td class="sc_agent"><?php echo $college_course['sc_to_agent'] ?></td>
													<?php } ?>
													<?php //} ?>
													
												</tr>

												<!-- /.modal -->
												<div id="responsive<?php echo $key; ?>" class="modal fade" tabindex="-1" aria-hidden="true">
													<div class="modal-dialog">
														<div class="modal-content" style="background-image: url('<?php echo $this->webroot; ?>img/bg-watermark.png'); height: fit-content; ">
															<div class="modal-header">
																<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
																<h4 class="modal-title"><?php echo $college_course['Course']['name'] ?></h4>
															</div>
															<div class="modal-body">
																<div class="scroller" style="height:300px" data-always-visible="1" data-rail-visible1="1">
																	<div class="row">
																		<div class="col-md-6">
																			<!-- <h4>Some Input</h4> -->
																			<p>
																				Duration: <a class="btn default btn-xs green"> <?php echo $college_course['Course']['duration'] ?></a>
																			</p>
																			<p>
																				Seat Left: <a class="btn default btn-xs green"><?php echo $college_course['seat_left'] ?></a>
																			</p>
																			<p>
																				College Type: <a class="btn default btn-xs green"><?php echo $college_type_dropdown[$this->request->data['Courseexpense'][$key]['gender']]; ?> </a>
																			</p>
																			<p>
																				Admission Fee: <a class="btn default btn-xs green"><?php echo $this->request->data['Courseexpense'][$key]['admission_fee'] ?></a>
																			</p>
																			<p>
																				Fee Year 1: <a class="btn default btn-xs green"><?php echo $this->request->data['Courseexpense'][$key]['year1'] ?></a>
																			</p>
																			<p>
																				Fee Year 2: <a class="btn default btn-xs green"><?php echo $this->request->data['Courseexpense'][$key]['year2'] ?></a>
																			</p>
																			<p>
																				Fee Year 3:<a class="btn default btn-xs green"><?php echo $this->request->data['Courseexpense'][$key]['year3'] ?></a>
																			</p>

																			<p>
																				Fee Year 4: <a class="btn default btn-xs green"><?php echo $this->request->data['Courseexpense'][$key]['year4'] ?></a>
																			</p>
																			<p>
																				Fee Year 5: <a class="btn default btn-xs green"><?php echo $this->request->data['Courseexpense'][$key]['year5'] ?></a>
																			</p>
																			<p>
																				Fee Year 6:<a class="btn default btn-xs green"><?php echo $this->request->data['Courseexpense'][$key]['year6'] ?></a>
																			</p>
																		</div>
																		<div class="col-md-6">
																			<!-- <h4>Some More Input</h4> -->
																			
																			<p>
																				Internship: <a class="btn default btn-xs green"><?php echo $this->request->data['Courseexpense'][$key]['internship'] ?></a>
																			</p>
																			<p>
																				Total Fee: <a class="btn default btn-xs green"><?php echo $this->request->data['Courseexpense'][$key]['total'] ?></a>
																			</p>
																			<?php if($groupId != 4 && $groupId == 1){ ?>
																			<p>
																				Service Charge: <a class="btn default btn-xs green"><?php echo $college_course['college_sc'] ?></a>
																			</p>
																			<p>
																				Service Charge Agent: <a class="btn default btn-xs green"><?php echo $college_course['sc_to_agent'] ?></a>
																			</p>
																			<?php } ?>
																			<?php if($groupId != 5){ ?>
																			<p>
																				Description: <div style="background-color:#eaf3d0 "><?php echo $college_course['notes']; ?></div>
																			</p>
																			<?php } ?>
																		</div>
																	</div>
																</div>
															</div>
															<div class="modal-footer">
																<button type="button" data-dismiss="modal" class="btn default">Close</button>
																
															</div>
														</div>
													</div>
												</div>
												<?php } ?>
											</tbody>
											</table>
										</div>
										** All fees are subject to change.
									</div>
								
								</div>
							<!-- END SAMPLE TABLE PORTLET-->
								</div>
							</div>
							<!--end media-->
							
							<!-- <?php if($groupId == 5){ ?> </br><a href="#" class="btn green btn-block">Apply Now</a> <?php } ?> -->
							<hr>
							<?php if($groupId != 5 && $groupId != 4 ){ ?>
							<div class="media">
								<h3>Students Reference</h3>
								<a href="#" class="pull-left">
								<img alt="" src="assets/img/blog/9.jpg" class="media-object">
								</a>
								<div class="media-body">
									<div class="portlet-body">
										<div class="table-scrollable">
											<table class="table table-striped table-bordered table-hover" id="reference">
											<thead>
											<tr>
												<th scope="col" style="width:450px !important">Student Name</th>
												<th scope="col">Course</th>
												<th scope="col">Phone</th>
												<th scope="col">Location</th>
												<th scope="col">Notes</th>
												
											</tr>
											</thead>
											<tbody>
												<?php  if (empty($this->request->data['Studentreference'])) { ?>

													<tr>
														<td colspan="11" align="center">No Reference details found.</td>
													</tr>
												<?php } ?>

												<?php foreach ($this->request->data['Studentreference'] as $key => $reference) { ?>
												<tr>
													
													<td class="student_name"><?php echo $reference['name'] ?></td>
													<td class="course"><?php echo $courses[$reference['course_id']] ?>
														
														<span class="refcourse_id" style="display: none;"><?php echo $reference['course_id'] ?></span>
													<span class="refcollege_id" style="display: none;"><?php echo $reference['college_id']; ?></span>
													<span class="ref_id" style="display: none;"><?php echo $reference['id']; ?></span>

													</td>
													<td class="phone"><?php echo $reference['phone'] ?></td>

													<td class="location"><?php echo $reference['location'] ?></td>

													<td class="notes"><?php echo $reference['notes'] ?></td>

													
												</tr>
												<?php } ?>
											</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
							<!--end media-->
							<?php } ?>
							
						</div>
						<!--end col-md-9-->
						<div class="col-md-3 blog-sidebar">
							
							<h3>Similar Colleges</h3>
							
							<?php if(!empty($collegesByCourseIds)){
								 foreach ($collegesByCourseIds as $key => $value) { //pr($value);?>
								<div class="top-news">
									<a href="<?php echo $this->webroot; ?>admin/dashboard/searchdetail/<?php echo $value['College']['id'] ?>" class="btn blue">
									<span style="font-size: 12px; font-style: normal;">
										<?php echo $value['College']['name']; ?>
									</span>
									<em style="font-size: 9px;"><?php echo h($cities[$value['College']['city_id']]); ?>, <?php echo h($states[$value['College']['state_id']]); ?></em>
									<em>
									</a>
								</div>	
							<?php }
								}else{ ?>
								<div class="top-news">
									<a href="##" class="btn red">
									<span style="font-size: 15px;">
										No Similar colleges to show.
									</span>
									
									</a>
								</div>
							<?php } ?>

							
							
						</div>
						<!--end col-md-3-->
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		

	<!-- END CONTENT -->

	<?php if($groupId == 5){  ?>
				<!--Start of Tawk.to Script-->
					<script type="text/javascript">
					var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
					(function(){
					var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
					s1.async=true;
					s1.src='https://embed.tawk.to/6083d35062662a09efc1a904/1f41f2sv8';
					s1.charset='UTF-8';
					s1.setAttribute('crossorigin','*');
					s0.parentNode.insertBefore(s1,s0);
					})();
					</script>
					<!--End of Tawk.to Script-->
			<?php } ?>

<script>
$(".slider-container").ikSlider({
  speed: 500
});
$(".slider-container").on('changeSlide.ikSlider', function (evt) { console.log(evt.currentSlide); });

</script>				
				