<?php $this->Html->addCrumb('Courseexpense', '/admin/courseexpenses'); ?>
<?php $this->Html->addCrumb('Add', ''); ?>
<div style='padding-bottom:10px;'><?php echo $this->Session->flash(); ?></div>
<script type="text/javascript">
$(document).ready(function(){
	var error1 = $('.alert-danger');
	$('#CourseexpenseAdminAddForm').validate({
		errorElement: 'span', //default input error message container
		errorClass: 'help-block', // default input error message class
		focusInvalid: false, // do not focus the last invalid input
		ignore: "",
		rules:{
		"data[Courseexpense][college_id]" : {required : true},
"data[Courseexpense][course_id]" : {required : true},
"data[Courseexpense][year1]" : {required : true},
"data[Courseexpense][year2]" : {required : true},
"data[Courseexpense][year3]" : {required : true},
"data[Courseexpense][year4]" : {required : true},
"data[Courseexpense][year5]" : {required : true},
"data[Courseexpense][year6]" : {required : true},
"data[Courseexpense][total]" : {required : true},
"data[Courseexpense][notes]" : {required : true},
		},
		messages:{
		"data[Courseexpense][college_id]" : {required :"Please enter college_id."},
"data[Courseexpense][course_id]" : {required :"Please enter course_id."},
"data[Courseexpense][year1]" : {required :"Please enter year1."},
"data[Courseexpense][year2]" : {required :"Please enter year2."},
"data[Courseexpense][year3]" : {required :"Please enter year3."},
"data[Courseexpense][year4]" : {required :"Please enter year4."},
"data[Courseexpense][year5]" : {required :"Please enter year5."},
"data[Courseexpense][year6]" : {required :"Please enter year6."},
"data[Courseexpense][total]" : {required :"Please enter total."},
"data[Courseexpense][notes]" : {required :"Please enter notes."},
		},

		invalidHandler: function (event, validator) { //display error alert on form submit              
			//success1.hide();
			error1.show();
			//App.scrollTo(error1, -200);
		},

		highlight: function (element) { // hightlight error inputs
			$(element)
				.closest('.form-group').addClass('has-error'); // set error class to the control group
		},

		unhighlight: function (element) { // revert the change done by hightlight
			$(element)
				.closest('.form-group').removeClass('has-error'); // set error class to the control group
		},

		success: function (label) {
			label
				.closest('.form-group').removeClass('has-error'); // set success class to the control group
			label
				.closest('.form-group').removeClass('error');
		},
	});
});

</script>

<div class="row">
    <div class="col-md-12">
        <div class="tab-content">
            <div class="tab-pane active" id="tab_0">
                 <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                        	<i class="fa fa-reorder"></i><?php echo __('Add Courseexpense'); ?>                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form courseexpenses">
                    <div class="alert alert-danger display-hide">
                        <button data-close="alert" class="close"></button>
                        You have some form errors. Please check below.
                    </div>
                    <!-- BEGIN FORM-->
                    <?php echo $this->Form->create('Courseexpense', array('class' => 'form-horizontal')); ?>
                    <div class="form-body">  
                    	<div class="form-group">
	                        	<label class="col-md-3 control-label" for="">&nbsp;</label>
	                            <div class="col-md-4">
	                                <div class="input text">
										<span class="required" style="color:#F00"> *</span>= Required
									</div>
	                            </div>
	                        </div>                                
                        <div class="form-group">
							 <?php echo $this->Form->label('college_id<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('college_id', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>
<div class="form-group">
							 <?php echo $this->Form->label('course_id<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('course_id', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>
<div class="form-group">
							 <?php echo $this->Form->label('year1<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('year1', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>
<div class="form-group">
							 <?php echo $this->Form->label('year2<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('year2', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>
<div class="form-group">
							 <?php echo $this->Form->label('year3<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('year3', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>
<div class="form-group">
							 <?php echo $this->Form->label('year4<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('year4', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>
<div class="form-group">
							 <?php echo $this->Form->label('year5<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('year5', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>
<div class="form-group">
							 <?php echo $this->Form->label('year6<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('year6', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>
<div class="form-group">
							 <?php echo $this->Form->label('total<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('total', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>
<div class="form-group">
							 <?php echo $this->Form->label('notes<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('notes', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>
                            <div class="form-actions fluid">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn blue">Submit</button>
                                    <button type="button" class="btn default" onclick="window.location='<?php echo $this->webroot.'admin/courseexpenses'; ?>'">Cancel</button>
                                </div>
                            </div>
                        </div>
                        <?php echo $this->Form->end(); ?>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>