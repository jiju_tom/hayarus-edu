<?php $this->Html->addCrumb('Courseexpense', '/admin/courseexpenses'); ?>
<?php $this->Html->addCrumb('View', ''); ?>
<div style='padding-bottom:10px;'><?php echo $this->Session->flash(); ?></div>

<div class="row">
    <div class="col-md-12">
        <div class="tab-content">
            <div class="tab-pane active" id="tab_0">
                 <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                        	<i class="fa fa-reorder"></i><?php echo __('View Courseexpense'); ?>                        				</div>
	                        <div class="tools">
	                            <a href="javascript:;" class="collapse"></a>
	                        </div>
	                    </div>
                    	<div class="portlet-body form courseexpenses">
		                    <div class="alert alert-danger display-hide">
		                        <button data-close="alert" class="close"></button>
		                        You have some form errors. Please check below.
		                    </div>
		                    <!-- BEGIN FORM-->
		                    <?php echo $this->Form->create('Courseexpense', array('class' => 'form-horizontal')); ?>
		                    <div class="form-body">                      
		                        						<div class='form-group'>
							 <?php echo $this->Form->label('college_id:' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<p class="form-control-static"><?php echo h($courseexpense['Courseexpense']['college_id']);?></p>
							</div>
						</div>
						<div class='form-group'>
							 <?php echo $this->Form->label('course_id:' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<p class="form-control-static"><?php echo h($courseexpense['Courseexpense']['course_id']);?></p>
							</div>
						</div>
						<div class='form-group'>
							 <?php echo $this->Form->label('year1:' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<p class="form-control-static"><?php echo h($courseexpense['Courseexpense']['year1']);?></p>
							</div>
						</div>
						<div class='form-group'>
							 <?php echo $this->Form->label('year2:' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<p class="form-control-static"><?php echo h($courseexpense['Courseexpense']['year2']);?></p>
							</div>
						</div>
						<div class='form-group'>
							 <?php echo $this->Form->label('year3:' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<p class="form-control-static"><?php echo h($courseexpense['Courseexpense']['year3']);?></p>
							</div>
						</div>
						<div class='form-group'>
							 <?php echo $this->Form->label('year4:' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<p class="form-control-static"><?php echo h($courseexpense['Courseexpense']['year4']);?></p>
							</div>
						</div>
						<div class='form-group'>
							 <?php echo $this->Form->label('year5:' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<p class="form-control-static"><?php echo h($courseexpense['Courseexpense']['year5']);?></p>
							</div>
						</div>
						<div class='form-group'>
							 <?php echo $this->Form->label('year6:' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<p class="form-control-static"><?php echo h($courseexpense['Courseexpense']['year6']);?></p>
							</div>
						</div>
						<div class='form-group'>
							 <?php echo $this->Form->label('total:' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<p class="form-control-static"><?php echo h($courseexpense['Courseexpense']['total']);?></p>
							</div>
						</div>
						<div class='form-group'>
							 <?php echo $this->Form->label('notes:' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<p class="form-control-static"><?php echo h($courseexpense['Courseexpense']['notes']);?></p>
							</div>
						</div>
		                            <div class="form-actions fluid">
		                                <div class="col-md-offset-3 col-md-9">
		                                    <button type="button" class="btn blue" onclick="window.location='<?php echo $this->webroot.'admin/courseexpenses'; ?>'">Go Back</button>
		                                </div>
		                            </div>
		                        </div>
		                        <?php echo $this->Form->end(); ?>
		                        <!-- END FORM-->
		                    </div>
                	</div>
            </div>
        </div>
    </div>
</div>