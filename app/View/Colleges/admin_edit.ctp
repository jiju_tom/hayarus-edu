<?php $this->Html->addCrumb('College', '/admin/colleges'); ?>
<?php $this->Html->addCrumb('Edit', ''); ?>
<div style='padding-bottom:10px;'><?php echo $this->Session->flash(); ?></div>
<script src="<?php echo $this->webroot; ?>ckeditor/ckeditor.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	var error1 = $('.alert-danger');
	$('#CollegeAdminEditForm').validate({
		errorElement: 'span', //default input error message container
		errorClass: 'help-block', // default input error message class
		focusInvalid: false, // do not focus the last invalid input
		ignore: "",
		rules:{
		"data[College][id]" : {required : true},
		"data[College][name]" : {required : true},
		"data[College][description]" : {required : false},
		"data[College][location_name]" : {required : true},
		"data[College][course_id]" : {required : true},
		},
		messages:{
		"data[College][id]" : {required :"Please enter id."},
		"data[College][name]" : {required :"Please enter name."},
		"data[College][description]" : {required :"Please enter description."},
		"data[College][location_name]" : {required :"Please enter location."},
		"data[College][course_id]" : {required :"Please enter course_id."},
		},

		invalidHandler: function (event, validator) { //display error alert on form submit              
			//success1.hide();
			error1.show();
			//App.scrollTo(error1, -200);
		},

		highlight: function (element) { // hightlight error inputs
			$(element)
				.closest('.form-group').addClass('has-error'); // set error class to the control group
		},

		unhighlight: function (element) { // revert the change done by hightlight
			$(element)
				.closest('.form-group').removeClass('has-error'); // set error class to the control group
		},

		success: function (label) {
			label
				.closest('.form-group').removeClass('has-error'); // set success class to the control group
			label
				.closest('.form-group').removeClass('error');
		},
	});

	var error1 = $('.alert-danger1');
	$('#CourseAdminEditForm').validate({
		errorElement: 'span', //default input error message container
		errorClass: 'help-block', // default input error message class
		focusInvalid: false, // do not focus the last invalid input
		ignore: "",
		rules:{
		"data[Course][id]" : {required : true},
		"data[Course][seat_left]" : {required : true},
		"data[Course][college_sc]" : {required : true},
		"data[Course][year1]" : {required : true},
		},
		messages:{
		"data[Course][id]" : {required :"Please enter id."},
		"data[Course][seat_left]" : {required :"Please enter seat left."},
		"data[Course][college_sc]" : {required :"Please enter sc."},
		"data[Course][year1]" : {required :"Please enter fee."}
		},

		invalidHandler: function (event, validator) { //display error alert on form submit              
			//success1.hide();
			error1.show();
			//App.scrollTo(error1, -200);
		},

		highlight: function (element) { // hightlight error inputs
			$(element)
				.closest('.form-group').addClass('has-error'); // set error class to the control group
		},

		unhighlight: function (element) { // revert the change done by hightlight
			$(element)
				.closest('.form-group').removeClass('has-error'); // set error class to the control group
		},

		success: function (label) {
			label
				.closest('.form-group').removeClass('has-error'); // set success class to the control group
			label
				.closest('.form-group').removeClass('error');
		},
	});


	var error1 = $('.alert-danger2');
	$('#ReferenceAdminEditForm').validate({
		errorElement: 'span', //default input error message container
		errorClass: 'help-block', // default input error message class
		focusInvalid: false, // do not focus the last invalid input
		ignore: "",
		rules:{
		"data[student_name]" : {required : true},
		"data[phone]" : {required : true},
		"data[student_location]" : {required : true},
		"data[notes]" : {required : false},
		},
		messages:{
		"data[student_name]" : {required :"Please enter student name."},
		"data[phone]" : {required :"Please enter phone number."},
		"data[student_location]" : {required :"Please enter location."},
		"data[notes]" : {required :"Please enter notes."}
		},

		invalidHandler: function (event, validator) { //display error alert on form submit              
			//success1.hide();
			error1.show();
			//App.scrollTo(error1, -200);
		},

		highlight: function (element) { // hightlight error inputs
			$(element)
				.closest('.form-group').addClass('has-error'); // set error class to the control group
		},

		unhighlight: function (element) { // revert the change done by hightlight
			$(element)
				.closest('.form-group').removeClass('has-error'); // set error class to the control group
		},

		success: function (label) {
			label
				.closest('.form-group').removeClass('has-error'); // set success class to the control group
			label
				.closest('.form-group').removeClass('error');
		},
	});

			
	$(".btn_course_edit").click(function() {
	    var row = $(this).closest("tr");    // Find the row
	    var course_id = row.find(".course_id").text(); 
	    var courseexpense_id = row.find(".courseexpense_id").text(); 
	    var collegecourse_id = row.find(".collegecourse_id").text();
	    var seat_left = row.find(".seat_left").text();
	    var admission_fee = row.find(".admission_fee").text();
	    var college_type = row.find(".college_type").text();
	    var year1 = row.find(".year1").text();
	    var year2 = row.find(".year2").text();
	    var year3 = row.find(".year3").text();
	    var year4 = row.find(".year4").text();
	    var year5 = row.find(".year5").text();
	    var year6 = row.find(".year6").text();
	    var internship = row.find(".internship").text();
	    var sc = row.find(".sc").text();
		var sc_agent = row.find(".sc_agent").text();
		var course_description = row.find(".course_description").html();

	    
	    // Let's paste it to form
	    $('#courseexpense_id').val(courseexpense_id);
	    $('#collegecourse_id').val(collegecourse_id);
	    $('#CourseCourseId').val(course_id).trigger('change');
	    $('#CourseSeatLeft').val(seat_left);
	    $('#CourseCollegeSc').val(sc);
	    $('#CourseScToAgent').val(sc_agent);
	    $('#CourseAdmissionFee').val(admission_fee);
	    $('#CourseGender').val(college_type).trigger('change');
	    $('#CourseYear1').val(year1);
	    $('#CourseYear2').val(year2);
	    $('#CourseYear3').val(year3);
	    $('#CourseYear4').val(year4);
	    $('#CourseYear5').val(year5);
	    $('#CourseYear6').val(year6);
	    $('#CourseInternship').val(internship);
	    CKEDITOR.instances['CourseNotes'].setData(course_description);


	});


	$(".btn_course_delete").click(function() {

		var row = $(this).closest("tr");    // Find the row
	    var course_id = row.find(".course_id").text(); 
	    var courseexpense_id = row.find(".courseexpense_id").text(); 
	    var collegecourse_id = row.find(".collegecourse_id").text();
	    //var formdata = ;
		var r = confirm("Are you sure you want to delete this?");
		if (r == true) {
			$.ajax({
                url: '<?php echo $this->webroot.'admin/Colleges/collegecoursedelete'; ?>',
                type: 'POST',
                data: {'courseexpense_id': courseexpense_id  , 'collegecourse_id': collegecourse_id},
                success: function (r) {
                  
                  if(r==1){
                  	location.reload();
                  }  
                    
                },
                cache: false
            });
		   
		} else {
		  	//nothing to do
		  	event.returnValue = false; 
		  	return false;
		}

		
	});


	$(".btn_ref_edit").click(function() {
	    var row = $(this).closest("tr");// Find the row
	    var name = row.find(".student_name").text();
	    var course_id = row.find(".refcourse_id").text(); 
	    var college_id = row.find(".refcollege_id").text(); 
	    var phone = row.find(".phone").text();
	    var location = row.find(".location").text();
	    var notes = row.find(".notes").text();
	    var id = row.find(".ref_id").text();	   
	    
	    // Let's paste it to form
	    $('#student_name').val(name);
	    $('#refcourse_id').val(course_id).trigger('change');
	    $('#phone').val(phone);
	    $('#student_location').val(location);
	    $('#notes').val(notes);
	    $('#ref_id').val(id);
	});
	
	$(".btn_ref_delete").click(function() {

		var row = $(this).closest("tr");    // Find the row
	    var ref_id = row.find(".ref_id").text(); 
	    
	    //var formdata = ;
		var r = confirm("Are you sure you want to delete this?");
		if (r == true) {
			$.ajax({
                url: '<?php echo $this->webroot.'admin/Studentreferences/delete'; ?>',
                type: 'POST',
                data: {'id': ref_id },
                success: function (r) {
                  
                  if(r==1){
                  	location.reload();
                  }  
                    
                },
                cache: false
            });
		   
		} else {
		  	//nothing to do
		  	event.returnValue = false; 
		  	return false;
		}

		
	});

	Dropzone.autoDiscover = false;
    var counter = 0; 
    var myDropzone = new Dropzone(".dropzone", { 
       autoProcessQueue: false,
       parallelUploads: 20,    
	   maxFilesize: 2, 
	   maxFiles: 20,
	   addRemoveLinks: true,
       acceptedFiles: ".jpeg,.jpg,.png,.gif",
       init: function () {
		    this.on("complete", function (file) {
		    	counter = counter+1;
            var count= myDropzone.files.length;
            var path = "<?php echo $this->webroot;?>assets/plugins/dropzone/uploads/";
           	var college_id = $('#college_id').val();
            if(counter == count){
                	$.ajax({
	                    url: '<?php echo $this->webroot.'admin/Collegeimages/getAllImages'; ?>',
	                    type: 'POST',
	                    data: {'id': college_id },
	                    success: function (r) {
	                      //console.log(r);
	                      if(r.length > 0){
	                      		$('#imgPortfolio').empty();
	                      	  	var obj = jQuery.parseJSON(r);
		  						for (var i = 0; i < obj.length; i++) {
		  							
		  							var img_id = $.trim(obj[i].Collegeimage.id);
		  						 	var portfolio_div = $('<div class="col-md-3 col-sm-4 mix category_1 mix_all" style="display: block;  opacity: 1;"><div class="mix-inner" style="height: 150px;"><img class="img-responsive" src="'+path+obj[i].Collegeimage.name+'" alt=""><div class="mix-details"><span class="img_id" style="display: none">1</span><h4></h4><a class="mix-link btn_delete_img" onclick="deleteImg('+img_id+')"><i class="fa fa-trash-o"></i></a><a class="mix-preview fancybox-button" href="'+path+obj[i].Collegeimage.name+'" title="" data-rel="fancybox-button"><i class="fa fa-search"></i></a></div></div></div>');
		  						 	Portfolio.init();
		  						 	$('#imgPortfolio').append(portfolio_div);
		  						}

	                		}else{

	                		}		

	            		},
                      	//location.reload();
                  });  
                    
              }
                cache: false
		    });
        }
    });
  
    $('#button').click(function(){
       myDropzone.processQueue();
    });

});

function deleteImg(id = null){
		
		var path = "<?php echo $this->webroot;?>assets/plugins/dropzone/uploads/";
		var college_id = $('#college_id').val();
		var r = confirm("Are you sure you want to delete this?");
		if (r == true) {
			$.ajax({
                url: '<?php echo $this->webroot.'admin/Collegeimages/delete'; ?>',
                type: 'POST',
                data: {'id': id },
                success: function (r) {
                  
                  if(r==1){
                  	$.ajax({
	                    url: '<?php echo $this->webroot.'admin/Collegeimages/getAllImages'; ?>',
	                    type: 'POST',
	                    data: {'id': college_id },
	                    success: function (r) {
	                      //console.log(r);
	                      if(r.length > 0){
	                      		$('#imgPortfolio').empty();
	                      	  	var obj = jQuery.parseJSON(r);
	                      	  	
		  						for (var i = 0; i < obj.length; i++) {
		  							Portfolio.init();
		  							var img_id = $.trim(obj[i].Collegeimage.id);
		  						 	var portfolio_div = $('<div class="col-md-3 col-sm-4 mix category_1 mix_all" style="display: block;  opacity: 1;"><div class="mix-inner" style="height: 150px;"><img class="img-responsive" src="'+path+obj[i].Collegeimage.name+'" alt=""><div class="mix-details"><span class="img_id" style="display: none">1</span><h4></h4><a class="mix-link btn_delete_img" onclick="deleteImg('+img_id+')"><i class="fa fa-trash-o"></i></a><a class="mix-preview fancybox-button" href="'+path+obj[i].Collegeimage.name+'" title="" data-rel="fancybox-button"><i class="fa fa-search"></i></a></div></div></div>');

		  						 	$('#imgPortfolio').append(portfolio_div);
		  						}
	                		}		
	            		},
                      	//location.reload();
                  }); 
                  }  
                    
                },
                cache: false
            });
		   
		} else {
		  	//nothing to do
		  	event.returnValue = false; 
		  	return false;
		}
}

</script>

<div class="row">
    <div class="col-md-12">
        <div class="tab-content">
            <div class="tab-pane active" id="tab_0">
                 <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                        	<i class="fa fa-reorder"></i><?php echo __('Edit College'); ?>                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form colleges">
                    <div class="alert alert-danger display-hide">
                        <button data-close="alert" class="close"></button>
                        You have some form errors. Please check below.
                    </div>
                    <!-- BEGIN FORM-->
                    <?php echo $this->Form->create('College', array('class' => 'form-horizontal')); ?>
                    <div class="form-body">  
                    	<div class="form-group">
	                        	<label class="col-md-3 control-label" for="">&nbsp;</label>
	                            <div class="col-md-4">
	                                <div class="input text">
										<span class="required" style="color:#F00"> *</span>= Required
									</div>
	                            </div>
	                        </div>                                
                        <div class="form-group">
							 <?php //echo $this->Form->label('id<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('id', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>
						<div class="form-group">
							 <?php echo $this->Form->label('name<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('name', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>


						<div class="form-group">
							 <?php echo $this->Form->label('description' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php //echo $this->Form->input('description', array('class' => 'form-control', 'label' => false, 'required' => false));?>
								<?php echo $this->Form->textarea('description',array('class'=>'form-control ckeditor', 'label' => false, 'required' => false))?>
							</div>
						</div>

						<div class="form-group">
							 <?php echo $this->Form->label('location<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('location_name', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>

						<div class="form-group">
							<?php echo $this->Form->label('Country<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label ')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('country_id', array('class' => 'form-control select2me', 'label' => false, 'required' => false, 'default' => 101));?>
							</div>
						</div>

						<div class="form-group">
							<?php echo $this->Form->label('State<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('state_id', array('class' => 'form-control select2me', 'label' => false, 'required' => false));?>
							</div>
						</div>

						<div class="form-group">
							<?php echo $this->Form->label('City<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label ')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('city_id', array('class' => 'form-control select2me', 'label' => false, 'required' => false));?>
							</div>
						</div>

						<div class="form-group">
							 <?php echo $this->Form->label('location description' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('location_description', array('class' => 'form-control', 'label' => false, 'required' => false, 'type'=> 'Textarea'));?>
							</div>
						</div>

						<div class="form-group">
							 <?php echo $this->Form->label('quick contact<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('quick_contact', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>

						<div class="form-group">
							 <?php echo $this->Form->label('Rating' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('rating', array('class' => 'form-control', 'label' => false, 'required' => false, 'options' => $rating));?>
							</div>
						</div>

                            <div class="form-actions fluid">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn blue">Submit</button>
                                    <button type="button" class="btn default" onclick="window.location='<?php echo $this->webroot.'admin/colleges'; ?>'">Cancel</button>
                                </div>
                            </div>
                        </div>
                        <?php echo $this->Form->end(); ?>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</br>
<div class="row">
	<div class="col-md-12">
		<p>
			<span class="label label-danger">
				UPLOAD IMAGES HERE			</span>
			&nbsp; For better UI experience, upload good quality pictures
		</p>
		<form action="<?php echo $this->webroot;?>assets/plugins/dropzone/upload.php" class="dropzone" id="my-dropzone">
			<?php echo $this->Form->input('college_id', array('class' => 'form-control', 'label' => false, 'required' => false, 'type' => 'hidden', 'value' => $this->request->params['pass'][0]));?>

		</form>
		</br>	
		<button type="submit" id="button" class="btn btn-primary">Start Upload Files</button>
		</br></br>
	</div>
	
	<div class="col-md-12" >
		<div class="tabbable tabbable-custom boxless">
			<div class="row mix-grid" id="imgPortfolio">
				<?php  foreach ($this->request->data['Collegeimage'] as $key => $images) { ?>
				<div class="col-md-3 col-sm-4 mix category_1">
					<div class="mix-inner" style="height: 150px;">
						<img class="img-responsive" src="<?php echo $this->webroot.$ImagePath.$images['name']; ?>" alt="" >
						<div class="mix-details">
							<span class="img_id" style="display: none"><?php echo $images['id']; ?></span>
							<h4></h4>
							<a class="mix-link btn_delete_img" onclick="deleteImg('<?php echo $images['id']; ?>')"><i class="fa fa-trash-o"></i></a>
							<a class="mix-preview fancybox-button" href="<?php echo $this->webroot.$ImagePath.$images['name']; ?>" title="Project Name" data-rel="fancybox-button"><i class="fa fa-search"></i></a>
						</div>
					</div>
				</div>
			<?php } ?>
			</div>
		</div>
	</div>
</div>
</br>
<div class="row">
    <div class="col-md-12">
        <div class="tab-content">
            <div class="tab-pane active" id="tab_0">
                 <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                        	<i class="fa fa-reorder"></i><?php echo __('Courses'); ?>                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form courses">
                    <div class="alert alert-danger display-hide">
                        <button data-close="alert" class="close"></button>
                        You have some form errors. Please check below.
                    </div>

                    <div class="portlet box purple">
									
									<div class="portlet-body">
										<div class="table-scrollable">
											<table class="table table-striped table-bordered table-hover" id="courses">
											<thead>
											<tr>
												<th scope="col" style="width:450px !important">Course Name</th>
												<th scope="col">Edit</th>
												<th scope="col">Delete</th>
												<th scope="col">Duration</th>
												<th scope="col">Seats Left</th>
												<th scope="col">Admission Fee</th>
												<th scope="col">College Type</th>
												<th scope="col">Fee Year 1</th>
												<th scope="col">Fee Year 2</th>
												<th scope="col">Fee Year 3</th>
												<th scope="col">Fee Year 4</th>
												<th scope="col">Fee Year 5</th>
												<th scope="col">Fee Year 6</th>
												<th scope="col">Internship</th>
												<th scope="col">Total Fee</th>
												<th scope="col">Service Charge</th>
												<th scope="col">Service Charge Agent</th>
												
											</tr>
											</thead>
											<tbody>
												<?php if (empty($this->request->data['Collegecourse'])) { ?>

													<tr>
														<td colspan="12" align="center">No Course details found.</td>
													</tr>
												<?php } ?>

												<?php foreach ($this->request->data['Collegecourse'] as $key => $college_course) { ?>
												<tr>
													
													

													<td class="course_name"><?php echo $college_course['Course']['name'] ?>
													<span class="course_id" style="display: none;"><?php echo $college_course['course_id'] ?></span>
													<span class="courseexpense_id" style="display: none;"><?php echo $this->request->data['Courseexpense'][$key]['id'] ?></span>
													<span class="collegecourse_id" style="display: none;"><?php echo $college_course['id'] ?></span>
													<span class="course_description" style="display: none;"><?php echo $college_course['notes'] ?></span>

													<span class="college_type" style="display: none;"><?php echo $this->request->data['Courseexpense'][$key]['gender'] ?></span>


													</td>
													<td>
														<a href="#formspan" class="btn default btn-xs purple btn_course_edit" id=""><i class="fa fa-edit"></i> Edit</a>
													</td>
													<td>
														<a href="#" class="btn red btn-xs black btn_course_delete" return false;"><i class="fa fa-trash-o"></i> Delete</a>
														
													</td>
													<td class="duration"><?php echo $college_course['Course']['duration'] ?></td>
													<td class="seat_left"><?php echo $college_course['seat_left'] ?></td>
													<td class="admission_fee"><?php echo $this->request->data['Courseexpense'][$key]['admission_fee'] ?></td>

													<td><?php echo $college_type_dropdown[$this->request->data['Courseexpense'][$key]['gender']]; ?></td>

													<td class="year1" data-toggle="tooltip" data-placement="top" data-theme="dark" title="<?php echo $college_course['notes']; ?>" style= "cursor: pointer;"><?php echo $this->request->data['Courseexpense'][$key]['year1'] ?></td>
													<td class="year2"><?php echo $this->request->data['Courseexpense'][$key]['year2'] ?></td>
													<td class="year3"><?php echo $this->request->data['Courseexpense'][$key]['year3'] ?></td>
													<td class="year4"><?php echo $this->request->data['Courseexpense'][$key]['year4'] ?></td>
													<td class="year5"><?php echo $this->request->data['Courseexpense'][$key]['year5'] ?></td>
													<td class="year6"><?php echo $this->request->data['Courseexpense'][$key]['year6'] ?></td>
													<td class="internship"><?php echo $this->request->data['Courseexpense'][$key]['internship'] ?></td>
													<td class="total"><?php echo $this->request->data['Courseexpense'][$key]['total'] ?></td>
													<td class="sc"><?php echo $college_course['college_sc'] ?></td>
													<td class="sc_agent"><?php echo $college_course['sc_to_agent'] ?></td>
													
												</tr>
												<?php } ?>
											</tbody>
											</table>
										</div>
									</div>
								</div>

					<span id="formspan"></span>			
                    <!-- BEGIN FORM-->
                    <?php echo $this->Form->create('Course', array('class' => 'form-horizontal')); ?>
                    <div class="form-body">  

                    	<div class="form-group">
                        	<label class="col-md-3 control-label" for="">&nbsp;</label>
                            <div class="col-md-4">
                                <div class="input text">
									<span class="required" style="color:#F00"> *</span>= Required
								</div>
                            </div>
                        </div> 

						<input type="hidden" name="data[Course][courseexpense_id]" class="form-control" id="courseexpense_id" value="">
						<input name="data[Course][collegecourse_id]" class="form-control" type="hidden" id="collegecourse_id" value="">

	                    <div class="form-group">
							<?php echo $this->Form->label('Course<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label ')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('course_id', array('class' => 'form-control select2me', 'label' => false, 'required' => false, 'default' => ''));?>
							</div>
						</div>

                        <div class="form-group">
							 <?php echo $this->Form->label('seat left<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('seat_left', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>

						<div class="form-group">
							 <?php echo $this->Form->label('service charge from college<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('college_sc', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>

						

						<div class="form-group">
							 <?php echo $this->Form->label('agent service charge' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('sc_to_agent', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>

						<div class="form-group">
							 <?php echo $this->Form->label('admission_fee' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('admission_fee', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>

						<div class="form-group">
							 <?php echo $this->Form->label('college_type' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('gender', array('class' => 'form-control', 'label' => false, 'required' => false, 'default' => 2, 'options' => $college_type_dropdown));?>
							</div>
						</div>
						
						<div class="form-group">
							 <?php echo $this->Form->label('Year 1 Fee<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('year1', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>

						<div class="form-group">
							 <?php echo $this->Form->label('Year 2 Fee' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('year2', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>

						<div class="form-group">
							 <?php echo $this->Form->label('Year 3 Fee' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('year3', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>

						<div class="form-group">
							 <?php echo $this->Form->label('Year 4 Fee' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('year4', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>

						<div class="form-group">
							 <?php echo $this->Form->label('Year 5 Fee' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('year5', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>

						<div class="form-group">
							 <?php echo $this->Form->label('Year 6 Fee' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('year6', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>

						<div class="form-group">
							 <?php echo $this->Form->label('Internship' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('internship', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>

						<div class="form-group">
							 <?php echo $this->Form->label('course description' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php //echo $this->Form->input('notes', array('class' => 'form-control', 'label' => false, 'required' => false, 'type'=> 'Textarea'));?>
								<?php echo $this->Form->textarea('notes',array('class'=>'form-control ckeditor', 'label' => false, 'required' => false))?>
							</div>
						</div>

					
                        <div class="form-actions fluid">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn blue">Submit</button>
                                <button type="button" class="btn default" onclick="window.location='<?php echo $this->webroot.'admin/colleges'; ?>'">Cancel</button>
                            </div>
                        </div>
                        </div>
                        <?php echo $this->Form->end(); ?>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</br>
<div class="row">
    <div class="col-md-12">
        <div class="tab-content">
            <div class="tab-pane active" id="tab_0">
                 <div class="portlet box yellow">
                    <div class="portlet-title">
                        <div class="caption">
                        	<i class="fa fa-reorder"></i><?php echo __('Students Reference'); ?>                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form courses">
                    <div class="alert alert-danger display-hide">
                        <button data-close="alert" class="close"></button>
                        You have some form errors. Please check below.
                    </div>

                    <div class="portlet box purple">
									
									<div class="portlet-body">
										<div class="table-scrollable">
											<table class="table table-striped table-bordered table-hover" id="reference">
											<thead>
											<tr>
												<th scope="col" style="width:450px !important">Student Name</th>
												<th scope="col">Course</th>
												<th scope="col">Phone</th>
												<th scope="col">Location</th>
												<th scope="col">Notes</th>
												<th scope="col">Edit</th>
												<th scope="col">Delete</th>
											</tr>
											</thead>
											<tbody>
												<?php  if (empty($this->request->data['Studentreference'])) { ?>

													<tr>
														<td colspan="11" align="center">No Reference details found.</td>
													</tr>
												<?php } ?>

												<?php foreach ($this->request->data['Studentreference'] as $key => $reference) { ?>
												<tr>
													
													<td class="student_name"><?php echo $reference['name'] ?></td>
													<td class="course"><?php echo $courses[$reference['course_id']] ?>
														
														<span class="refcourse_id" style="display: none;"><?php echo $reference['course_id'] ?></span>
													<span class="refcollege_id" style="display: none;"><?php echo $reference['college_id']; ?></span>
													<span class="ref_id" style="display: none;"><?php echo $reference['id']; ?></span>

													</td>
													<td class="phone"><?php echo $reference['phone'] ?></td>

													<td class="location"><?php echo $reference['location'] ?></td>

													<td class="notes"><?php echo $reference['notes'] ?></td>

													<td>
														<a href="#formspan1" class="btn default btn-xs purple btn_ref_edit" id=""><i class="fa fa-edit"></i> Edit</a>
													</td>
													<td>
														<a href="#" class="btn red btn-xs black btn_ref_delete" return false;"><i class="fa fa-trash-o"></i> Delete</a>
														
													</td>
												</tr>
												<?php } ?>
											</tbody>
											</table>
										</div>
									</div>
								</div>

					<span id="formspan1"></span>		

                    <!-- BEGIN FORM-->
                    <form action="<?php echo $this->webroot; ?>admin/colleges/studentreference/" class="form-horizontal" id="ReferenceAdminEditForm" method="post" accept-charset="utf-8" novalidate="novalidate" >
                    <div class="form-body">  
                    	<div class="form-group">
	                        	<label class="col-md-3 control-label" for="">&nbsp;</label>
	                            <div class="col-md-4">
	                                <div class="input text">
										<span class="required" style="color:#F00"> *</span>= Required
									</div>
	                            </div>
	                        </div> 

	                    <div class="form-group">

	                    	<?php echo $this->Form->input('refcollege_id', array('class' => 'form-control', 'label' => false, 'required' => false, 'type' => 'hidden', 'value' => $this->request->params['pass'][0]));?>
	                    	<?php echo $this->Form->input('ref_id', array('class' => 'form-control', 'label' => false, 'required' => false, 'type' => 'hidden', 'value' => ''));?>

							<?php echo $this->Form->label('Student Name<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label ')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('student_name', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>

						<div class="form-group">
							<?php echo $this->Form->label('Course<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label ')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('refcourse_id', array('class' => 'form-control select2me', 'label' => false, 'required' => false, 'default' => '', 'options' => $courses ));?>
							</div>
						</div>

                        <div class="form-group">
							 <?php echo $this->Form->label('phone<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('phone', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>

						<div class="form-group">
							 <?php echo $this->Form->label('location<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('student_location', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>

						<div class="form-group">
							 <?php echo $this->Form->label('notes' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('notes', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>

                        <div class="form-actions fluid">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn blue">Submit</button>
                                <button type="button" class="btn default" onclick="window.location='<?php echo $this->webroot.'admin/colleges'; ?>'">Cancel</button>
                            </div>
                        </div>

                        </div>
                        <?php echo $this->Form->end(); ?>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


