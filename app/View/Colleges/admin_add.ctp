<?php $this->Html->addCrumb('College', '/admin/colleges'); ?>
<?php $this->Html->addCrumb('Add', ''); ?>
<div style='padding-bottom:10px;'><?php echo $this->Session->flash(); ?></div>
<script src="<?php echo $this->webroot; ?>ckeditor/ckeditor.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	var error1 = $('.alert-danger');
	$('#CollegeAdminAddForm').validate({
		errorElement: 'span', //default input error message container
		errorClass: 'help-block', // default input error message class
		focusInvalid: false, // do not focus the last invalid input
		ignore: "",
		rules:{
		"data[College][name]" : {required : true},
		"data[College][description]" : {required : false},
		"data[College][location_name]" : {required : true},
		"data[College][course_id]" : {required : true},
		"data[College][quick_contact]" : {required : true, number: true},
		},
		messages:{
		"data[College][name]" : {required :"Please enter name."},
		"data[College][description]" : {required :"Please enter description."},
		"data[College][locatilocation_nameon_id]" : {required :"Please enter location."},
		"data[College][course_id]" : {required :"Please enter course_id."},
		"data[College][quick_contact]" : {required :"Please enter contact person.", number: "enter a phone number"},
		},

		invalidHandler: function (event, validator) { //display error alert on form submit              
			//success1.hide();
			error1.show();
			//App.scrollTo(error1, -200);
		},

		highlight: function (element) { // hightlight error inputs
			$(element)
				.closest('.form-group').addClass('has-error'); // set error class to the control group
		},

		unhighlight: function (element) { // revert the change done by hightlight
			$(element)
				.closest('.form-group').removeClass('has-error'); // set error class to the control group
		},

		success: function (label) {
			label
				.closest('.form-group').removeClass('has-error'); // set success class to the control group
			label
				.closest('.form-group').removeClass('error');
		},
	});
});

</script>

<div class="row">
    <div class="col-md-12">
        <div class="tab-content">
            <div class="tab-pane active" id="tab_0">
                 <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                        	<i class="fa fa-reorder"></i><?php echo __('Add College'); ?>                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form colleges">
                    <div class="alert alert-danger display-hide">
                        <button data-close="alert" class="close"></button>
                        You have some form errors. Please check below.
                    </div>
                    <!-- BEGIN FORM-->
                    <?php echo $this->Form->create('College', array('class' => 'form-horizontal')); ?>
                    <div class="form-body">  
                    	<div class="form-group">
	                        	<label class="col-md-3 control-label" for="">&nbsp;</label>
	                            <div class="col-md-4">
	                                <div class="input text">
										<span class="required" style="color:#F00"> *</span>= Required
									</div>
	                            </div>
	                        </div>                                
                        <div class="form-group">
							 <?php echo $this->Form->label('name<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('name', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>
						<div class="form-group">
							 <?php echo $this->Form->label('college description<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->textarea('description',array('class'=>'form-control ckeditor', 'label' => false, 'required' => false))?>
							</div>
						</div>
						<div class="form-group">
							 <?php echo $this->Form->label('location<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('location_name', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>

						<div class="form-group">
							<?php echo $this->Form->label('Country<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label ')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('country_id', array('class' => 'form-control select2me', 'label' => false, 'required' => false, 'default' => 101));?>
							</div>
						</div>

						<div class="form-group">
							<?php echo $this->Form->label('State<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('state_id', array('class' => 'form-control select2me', 'label' => false, 'required' => false));?>
							</div>
						</div>

						<div class="form-group">
							<?php echo $this->Form->label('City<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label ')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('city_id', array('class' => 'form-control select2me', 'label' => false, 'required' => false));?>
							</div>
						</div>

						<div class="form-group">
							 <?php echo $this->Form->label('location description<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('location_description', array('class' => 'form-control', 'label' => false, 'required' => false, 'type'=> 'Textarea'));?>
							</div>
						</div>

						<div class="form-group">
							 <?php echo $this->Form->label('quick contact<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('quick_contact', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>

						<div class="form-group">
							 <?php echo $this->Form->label('Rating' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('rating', array('class' => 'form-control', 'label' => false, 'required' => false, 'options' => $rating));?>
							</div>
						</div>


						
                            <div class="form-actions fluid">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn blue">Submit</button>
                                    <button type="button" class="btn default" onclick="window.location='<?php echo $this->webroot.'admin/colleges'; ?>'">Cancel</button>
                                </div>
                            </div>
                        </div>
                        <?php echo $this->Form->end(); ?>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
