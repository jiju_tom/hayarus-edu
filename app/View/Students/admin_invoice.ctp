<?php //$this->Html->addCrumb('Admission', '/admin/students'); ?>
<?php //$this->Html->addCrumb('Invoice', ''); ?>

<div style='padding-bottom:10px;'><?php echo $this->Session->flash(); ?></div>
<script type="text/javascript">
$(document).ready(function(){
	
	$('#company_name').on('change', function(){ 
		var company_name = $('#company_name').val();
		if(company_name == 1){
			$("#companyImage").attr("src","<?php echo $this->webroot; ?>img/logo-hayarus.png");
			$("#addressHayarusSolutions").show();
			$("#addressHayarusTrust").hide();
		}else{
			$("#companyImage").attr("src","<?php echo $this->webroot; ?>img/trust-logo.png");
			$("#addressHayarusSolutions").hide();
			$("#addressHayarusTrust").show();
		}
		
	});

	$("i.fa").click(function() {
	    $(this).closest("tr").remove();
	});

	$( "#print_now" ).on( "click", function() { 
    	event.preventDefault();  
   		
    	var particulars1 = null;
    	var particulars2 = null;
    	var particulars3 = null;
    	var particulars4 = null;
    	var amount1 = null;
    	var amount2 = null;
    	var amount3 = null;
    	var amount4 = null;

    	var branch = $('#company_name').val();
    	var invoice_no = $('#invoice_no').val();
    	var about = $('#about').val();
    	var issued_to = $('#issued_to').val();

    	if ($("#particulars1").length){  
   			particulars1 = $('#particulars1').val();
   		}
   		if ($("#particulars2").length){  
   			particulars2 = $('#particulars2').val();
   		}
   		
   		if ($("#particulars3").length){  
   			particulars3 = $('#particulars3').val();
   		}

   		if ($("#particulars4").length){  
   			particulars4 = $('#particulars4').val();
   		}


   		if ($("#amount1").length){  
   			amount1 = $('#amount1').val();
   		}
   		if ($("#amount2").length){  
   			amount2 = $('#amount2').val();
   		}
   		
   		if ($("#amount3").length){  
   			amount3 = $('#amount3').val();
   		}

   		if ($("#amount4").length){  
   			amount4 = $('#amount4').val();
   		}

   		if(particulars1 != "" || particulars2 != "" || particulars3 != "" ){ 
   			$.ajax({
	            url: '<?php echo $this->webroot.'admin/invoices/add'; ?>',
	            type: 'POST',
	            data: {"data[Invoice][branch]": branch  , "data[Invoice][invoice_no]": invoice_no, "data[Invoice][about]": about , "data[Invoice][issued_to]": issued_to, "data[Invoice][particular][0]": particulars1, "data[Invoice][particular][1]": particulars2, "data[Invoice][particular][2]": particulars3, "data[Invoice][particular][3]": particulars4, "data[Invoice][amount][0]": amount1, "data[Invoice][amount][1]": amount2, "data[Invoice][amount][2]": amount3, "data[Invoice][amount][3]": amount4},
	            success: function (r) {
	              if(r== 0){
	               alert('Something went wrong. try again.');
	              } else{
	                //$('#company_name').hide();
			    	//window.print(); // print the page
			    	//$('#company_name').show();
	              }
	                
	            },
	            cache: false
	        });	
   		}

   		

   		
	});

	$('input.amount').change(function () {
   		//alert(this.value);
   		var amountItem1 = 0;
   		var amountItem2 = 0; 
   		var amountItem3 = 0;
   		var amountItem4 = 0;
   		var total=  0;

   		if ($("#amount1").length){  
   			amountItem1 = $('#amount1').val();
   		}
   		if ($("#amount2").length){  
   			amountItem2 = $('#amount2').val();
   		}
   		
   		if ($("#amount3").length){  
   			amountItem3 = $('#amount3').val();
   		}
   		
   		amountItem4 = $('#amount4').val();
		var total = +amountItem1 + +amountItem2 + +amountItem3 - +amountItem4;
		$('#total').html(total);

	});
	
});

</script>
<style type="text/css">
	.page-breadcrumb{
		display: none;
	}

	@media print {
	  button {
	    display: none !important;
	  }
	  input,
	  textarea {
	    border: none !important;
	    box-shadow: none !important;
	    outline: none !important;
	    resize: none;
	  }
	  i{
	  	display: none !important;
	  }
	}
</style>



<div class="row">
	<div class="col-md-4" style="margin-bottom: 20px;">
		<select class="form-control input" data-placeholder="Select College" name="college_id" id="company_name">
			<option value="0">Hayarus Education Trust</option>
			<option value="1">Hayarus Solutions</option>
		</select>
	</div>

    <div class="col-md-12">
        <div class="invoice">
				<div class="row invoice-logo">
					<div class="col-xs-6 invoice-logo-space">
						<img src="<?php echo $this->webroot; ?>img/trust-logo.png" alt="" id="companyImage" width ="200" />
					</div>
					<div class="col-xs-6">
						<p>
							#<?php echo $rand = rand ( 100000 , 999999 ); ?> | <?php echo date("d/m/Y"); ?>
							<!-- <span class="muted">
								Consectetuer adipiscing elit
							</span> -->
						</p>
					</div>
				</div>
				<hr/>
				<div class="row"> <?php //pr($student); ?>
					<div class="col-xs-4">
						<h4 style="margin-left: 9px;">To:</h4>
						<ul class="list-unstyled">
							<li>
								<?php echo $this->Form->input('invoice_no', array('class' => 'form-control input-medium', 'label' => false, 'required' => false, 'value' => $rand, 'type' => 'hidden'));?>

								<textarea rows="5" cols="50" name="data['Invoice'][name]" class="form-control " value="" id="issued_to"><?php echo $student['Student']['name']; ?></textarea>
								
							</li>
							
						</ul>
					</div>
					<div class="col-xs-6">
						<h4 style="margin-left: 9px;">Course Details:</h4>
						<ul class="list-unstyled">
							<li>
								<textarea rows="5" cols="50" name="data['Invoice'][about]" class="form-control " value="" id="about"><?php  if(!empty($student['Student']['course_id'])) { echo $courses[$student['Student']['course_id']]; }else { echo ""; } ?> in <?php  if(!empty($student['Student']['college_id'])) { echo $colleges[$student['Student']['college_id']]; }else { echo ""; } ?></textarea>
							</li>
							
						</ul>
					</div>
					<!-- <div class="col-xs-4 invoice-payment">
						<h4>Payment Details:</h4>
						<ul class="list-unstyled">
							<li>
								<strong>V.A.T Reg #:</strong> 542554(DEMO)78
							</li>
							<li>
								<strong>Account Name:</strong> FoodMaster Ltd
							</li>
							<li>
								<strong>SWIFT code:</strong> 45454DEMO545DEMO
							</li>
							<li>
								<strong>V.A.T Reg #:</strong> 542554(DEMO)78
							</li>
							<li>
								<strong>Account Name:</strong> FoodMaster Ltd
							</li>
							<li>
								<strong>SWIFT code:</strong> 45454DEMO545DEMO
							</li>
						</ul>
					</div> -->
				</div>
				<div class="row">
					<div class="col-xs-12">
						<table class="table table-striped table-hover">
						<thead>
						<tr>
							<th>#</th>
							<th>Particulars</th>
							<th></th>
							<th>Amount</th>
							<th></th>
						</tr>
						</thead>
						<tbody>
						<tr id="row1">
							<td>1</td>
							<td width="70%"><input type="text" name="data[Invoice][particulars1]" aria-controls="sample_editable_1" class="form-control" value="" id="particulars1"></td>
							<td></td>
							<td><input type="text" name="data[Invoice][amount1]" aria-controls="sample_editable_1" class="form-control amount" value="" id="amount1"></td>
							<td><i class="fa fa-times-circle" style="font-size: 18px; color: #35aa47; padding: 8px; cursor: pointer;"></i></td>
						</tr>
						<tr id="row2">
							<td>2</td>
							<td width="70%"><input type="text" name="data[Invoice][particulars2]" aria-controls="sample_editable_1" class="form-control" value="" id="particulars2"></td>
							<td></td>
							<td><input type="text" name="data[Invoice][amount2]" aria-controls="sample_editable_1" class="form-control amount" value="" id="amount2"></td>
							<td><i class="fa fa-times-circle" style="font-size: 18px; color: #35aa47; padding: 8px; cursor: pointer;"></i></td>
						</tr>
						<tr id="row3">
							<td>3</td>
							<td width="70%"><input type="text" name="data[Invoice][particulars3]" aria-controls="sample_editable_1" class="form-control" value="" id="particulars3"></td>
							<td></td>
							<td><input type="text" name="data[Invoice][amount3]" aria-controls="sample_editable_1" class="form-control amount" value="" id="amount3"></td>
							<td><i class="fa fa-times-circle" style="font-size: 18px; color: #35aa47; padding: 8px; cursor: pointer;"></i></td>
						</tr>

						<tr id="row4">
							<td></td>
							<td width="70%"><input type="text" name="data[Invoice][particulars4]" aria-controls="sample_editable_1" class="form-control" value="Scholarship"  id="particulars4"></td>
							<td></td>
							<td><input type="text" name="data[Invoice][amount4]" aria-controls="sample_editable_1" class="form-control amount" value="" id="amount4"></td>
							<td></td>
						</tr>


						</tbody>
						</table>
					</div>
				</div>
				</br>
				<div class="row">
					<div class="col-xs-4">
						<div class="well" id="addressHayarusTrust">
							<address>
							<strong>Hayarus Education Trust</strong><br/>
							Level 2, Nellimoottil Building<br/>
							Near KSRTC Bus Stand, Pathanamthitta<br/>
							Phone: 9048 55 1110, 9048 56 1110 </address>
							<address>
							
							</address>
						</div>

						<div class="well" id="addressHayarusSolutions" style="display: none;">
							<address>
							<strong>Hayarus Solutions</strong><br/>
							Level 1, Chakkalayil Complex<br/>
							Aruthooti P.O, Kottayam<br/>
							Phone:  9048 57 1110</address>
							<address>
							
							</address>
						</div>

					</div>
					<div class="col-xs-8 invoice-block">

						<ul class="list-unstyled amounts">
							
							<li>
								<strong>Grand Total:</strong> <i class="fa fa-inr" aria-hidden="true" style="font-size: 12px;"></i><span id="total">0</span>
							</li>
						</ul>
						<br/>
						<a class="btn btn-lg blue hidden-print" id="print_now">Print <i class="fa fa-print"></i></a>
						<!-- <a class="btn btn-lg green hidden-print">Submit Your Invoice <i class="fa fa-check"></i></a> -->
					</div>
				</div>
			</div>
    </div>
</div>