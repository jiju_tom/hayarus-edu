<?php $this->Html->addCrumb('Service Enquiries', '/admin/businesses'); $paginationVariables = $this->Paginator->params();?>

<?php 
 $name = isset($name) ? $name : "";
 $phone = isset($phone) ? $phone : "";
 $status_id = isset($status_id) ? $status_id : "";
?>

<div style='padding-bottom:10px;'><?php echo $this->Session->flash(); ?></div>
<div class="row">
    <div class="col-md-12">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
	                <i class="fa fa-list-alt"></i><?php echo __('Service Enquiries'); ?>                </div>
                <!-- <div class="tools">
    		        <a href="javascript:;" class="collapse"></a>
            	</div> -->
            </div>
            <div class="portlet-body"><br />
                <div class="table-toolbar">
                    <?php if($groupId != 1){ ?>
                    <div class="btn-group"> 
                        <?php echo $this->Form->create('', array('type' => 'get','role' => 'form', 'url' => array('controller' => strtolower('Businesses'), 'action' => 'index', 'admin' => true))); ?>
                            <div class="row">
                            <!-- /.col-md-6 -->
                                <div class="col-md-6">
                                    <div class="input-group input-medium" >
                                        <input type="text" class="form-control" placeholder="Search here" name="search">
                                        <span class="input-group-btn">
	                                        <button class="btn green" type="button" onclick="this.form.submit()">Search <i class="fa fa-search"></i></button>
                                        </span>
                                    </div>
                                <!-- /input-group -->
                                </div>
                            <!-- /.col-md-6 -->
                            </div>
                        <!-- /.row -->
                        </form>
                    </div>
                    <?php } ?>
                    <div class="btn-group pull-right">
                        <div class="btn-group" style="padding-right:15px;">
	                        <?php echo $this->Html->link(__('New Service Enquiry <i class="fa fa-plus"></i>'), array('action' => 'add'), array('class' => 'btn green','escape' => FALSE)); ?> 
                        </div>
                        <!-- <button class="btn default"  onclick="window.location.href='<?php echo $this->webroot.'admin/Businesses/export'; ?>'">Export to CSV <i class="fa fa-download"></i> </button> -->
                    </div>
                </div>

                <div class="row" style="margin-left: 13px; margin-top: 50px;">
                   <?php echo $this->Form->create('', array('type' => 'get','role' => 'form', 'url' => array('controller' => strtolower('Businesses'), 'action' => 'index', 'admin' => true))); ?>
                   <?php if($groupId == 1){ ?>
                    <div class="table-toolbar">
                        <div class="col-md-3 col-sm-12">
                          <div class="dataTables_filter" id="sample_editable_1_filter">
                            <label>Name: <input type="text" name="name" aria-controls="sample_editable_1" class="form-control input-medium" value="<?php echo $name; ?>"></label>
                          </div>
                        </div>

                        <div class="col-md-3 col-sm-12">
                          <div class="dataTables_filter" id="sample_editable_1_filter">
                            <label>Phone: <input type="text" name="phone" aria-controls="sample_editable_1" class="form-control input-medium" value="<?php echo $phone; ?>"></label>
                          </div>
                        </div>

                        <!-- <div class="col-md-3 col-sm-12">
                          <div class="dataTables_filter" id="sample_editable_1_filter">
                            <label>Added By: <?php echo $this->Form->input('user_id', array('class' => 'form-control input-medium', 'label' => false, 'required' => false, 'empty'=>'Select', 'selected' => $user_id));?></label>
                          </div>
                        </div> -->

                        <div class="col-md-3 col-sm-12">
                          <div class="dataTables_filter" id="sample_editable_1_filter">
                            <label>Status: <?php echo $this->Form->input('servicestatuses', array('class' => 'form-control input-medium', 'label' => false, 'required' => false, 'empty'=>'Select', 'selected' => $status_id));?></label>
                          </div>
                        </div>

                    <div class="col-md-3 col-sm-12">
                      <div class="dataTables_filter" id="sample_editable_1_filter">
                      <label >Added Date:
                      <input class="form-control form-control-inline input-medium date-picker" size="16" name="created" type="text" value="<?php echo  isset($created) ? $created : ""; ?>" placeholder="month/date/year"/></label>
                      </div>
                    </div>

                    <div class="col-md-3 col-sm-12">
                      <div class="dataTables_filter" id="sample_editable_1_filter">
                      <label >Updated Date:
                      <input class="form-control form-control-inline input-medium date-picker" size="16" name="modified" type="text" value="<?php echo  isset($modified) ? $modified : ""; ?>" placeholder="month/date/year" /></label>
                      </div>
                    </div>

                    

                  <!--   <div class="col-md-3 col-sm-12">
                          <div class="dataTables_filter" id="sample_editable_1_filter">
                            <label>Updated By: <?php echo $this->Form->input('updated_user', array('class' => 'form-control input-medium', 'label' => false, 'required' => false, 'options' => $users, 'empty'=>'Select', 'selected' => $updated_user));?></label>
                          </div>
                        </div> -->

                        <div class="col-md-3 col-sm-12">
                          <button class="btn green" type="submit" onclick="this.form.submit()" style="    margin-top: 30px;float: left;">Search <i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    <?php } ?>
                  </form>
                </div>

                <br />
                <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover " id="sample_1">
                  <thead>
                      <tr>
                        <th>#</th>  
                        <th><?php echo $this->Paginator->sort('name'); ?></th>
                        <th><?php echo $this->Paginator->sort('phone'); ?></th>
                        
                        <th><?php echo $this->Paginator->sort('businesstype', 'business type'); ?></th>
                        <th><?php echo $this->Paginator->sort('service', 'Service Required'); ?></th>
                        
                        <th><?php echo $this->Paginator->sort('status'); ?></th>
                        <th>invoice</th>
                        <th>Edit</th>
                        <th>Delete</th>
                      </tr>
                  </thead>
                  <?php  if(isset($businesses) && sizeof($businesses)>0) {?>
                  <tbody>
                  <?php $slno=0; foreach ($businesses as $business): $slno++?>
                <tr>
                    <td><?php echo $slno+$paginationVariables['limit']*($paginationVariables['page']-1); ?></td>
        			<td><?php echo h($business['Business']['name']); ?>&nbsp;</td>
            		<td><?php echo h($business['Business']['phone']); ?>&nbsp;</td>
            		
                    <td><?php echo h($business['Business']['businesstype']); ?>
		            </td>
		            <td><?php echo h($business['Business']['service']); ?>&nbsp;</td>
                    
		            <td><?php echo $servicestatuses[$business['Business']['servicestatus_id']]; ?>&nbsp;</td>

                    <td><?php if(!empty($business['Business']['invoice_id'])) { echo $this->Html->link(__('<i class="fa fa-edit"></i> Invoice'), array('controller' => 'invoices','action' => 'serviceview', $business['Business']['invoice_id']), array('class' => 'btn default btn-xs yellow','escape' => FALSE)); } else { Echo 'NA';} ?>&nbsp;</td>

					<td>
							<?php echo $this->Html->link(__('<i class="fa fa-edit"></i> Edit'), array('action' => 'edit', $business['Business']['id']), array('class' => 'btn default btn-xs purple','escape' => FALSE)); ?>
					</td>
					<td>
							<?php echo $this->Form->postLink(__('<i class="fa fa-trash-o"></i> Delete'), array('action' => 'delete', $business['Business']['id']), array('confirm' => 'Are you sure you want to delete this?','class' => 'btn red btn-xs black','escape' => FALSE)); ?>
					</td>
				</tr>
									<?php endforeach; ?>
                   <?php } else {?>
                      <tr><td colspan='8' align='center'>No Records Found.</td></tr>
                  <?php }?>
                  </tbody>
                </table>
              </div>
                <div class="row">
    	            <div class="col-md-5 col-sm-12" style="margin-top:10px;">
                      <div id="sample_1_length" class="dataTables_length">
                        <?php echo $this->Form->create('', array( 'url' => array('controller' => strtolower('Businesses'), 'action' => 'index'))); ?>
                            <label>Show 
                          			<?php echo $this->Form->input('limit', array('name' => 'data[Business][limit]','type' => 'select', 'class' => 'form-control input-xsmall', 'label' => false,  'required' => false,'hiddenField' => false, 'style' => 'display:inline-block !important; padding-right:0px', 'onchange' => 'this.form.submit()', 'options' =>$default_limit_dropdown, 'default' => $limit, 'div' => false));?>
                            records
                            </label>
                            <?php echo $this->Form->end(); ?>
                					</div>
                				</div>
                        <div class="col-md-7 col-sm-12">
                          <div class="dataTables_paginate paging_bootstrap">
                            <?php $totalItem = $this->Paginator->counter('{:count}')?><?php $currentItem = $this->Paginator->counter('{:current}')?><?php if($totalItem>$currentItem) {?>
                            <ul class="pagination" style="visibility: visible;">
                                <li class="prev disabled">
                                			<?php echo $this->Paginator->prev('< ' . __(''), array(), null, array('class' => 'prev disabled'));?>
	
                                </li>
                                <li>
                                			<?php		echo $this->Paginator->numbers(array('separator' => ''));	?>
                                </li>
                                <li class="next disabled">
                                			<?php		echo $this->Paginator->next(__('') . ' >', array(), null, array('class' => 'next'));	?>
                                </li>
                            </ul>
                      <?php }?>
    						        </div>
                      </div>
    				    </div>
    			</div>
    	</div>
    <!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
