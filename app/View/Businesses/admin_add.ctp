<?php $this->Html->addCrumb('Business', '/admin/businesses'); ?>
<?php $this->Html->addCrumb('Add', ''); ?>
<div style='padding-bottom:10px;'><?php echo $this->Session->flash(); ?></div>
<script type="text/javascript">
$(document).ready(function(){
	var error1 = $('.alert-danger');
	$('#BusinessAdminAddForm').validate({
		errorElement: 'span', //default input error message container
		errorClass: 'help-block', // default input error message class
		focusInvalid: false, // do not focus the last invalid input
		ignore: "",
		rules:{
		"data[Business][name]" : {required : true},
		"data[Business][phone]" : {required : true},
		"data[Business][address]" : {required : false},
		"data[Business][businesstype_id]" : {required : false},
		"data[Business][service]" : {required : true},
		"data[Business][invoice_id]" : {required : false},
		"data[Business][status]" : {required : false},
		},
		messages:{
		"data[Business][name]" : {required :"Please enter name."},
		"data[Business][phone]" : {required :"Please enter phone."},
		"data[Business][address]" : {required :"Please enter address."},
		"data[Business][businesstype_id]" : {required :"Please enter businesstype_id."},
		"data[Business][service]" : {required :"Please enter service."},
		"data[Business][invoice_id]" : {required :"Please enter invoice_id."},
		"data[Business][status]" : {required :"Please enter status."},
		},

		invalidHandler: function (event, validator) { //display error alert on form submit              
			//success1.hide();
			error1.show();
			//App.scrollTo(error1, -200);
		},

		highlight: function (element) { // hightlight error inputs
			$(element)
				.closest('.form-group').addClass('has-error'); // set error class to the control group
		},

		unhighlight: function (element) { // revert the change done by hightlight
			$(element)
				.closest('.form-group').removeClass('has-error'); // set error class to the control group
		},

		success: function (label) {
			label
				.closest('.form-group').removeClass('has-error'); // set success class to the control group
			label
				.closest('.form-group').removeClass('error');
		},
	});
});

</script>

<div class="row">
    <div class="col-md-12">
        <div class="tab-content">
            <div class="tab-pane active" id="tab_0">
                 <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                        	<i class="fa fa-reorder"></i><?php echo __('Add Business'); ?>                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form businesses">
                    <div class="alert alert-danger display-hide">
                        <button data-close="alert" class="close"></button>
                        You have some form errors. Please check below.
                    </div>
                    <!-- BEGIN FORM-->
                    <?php echo $this->Form->create('Business', array('class' => 'form-horizontal')); ?>
                    <div class="form-body">  
                    	<div class="form-group">
	                        	<label class="col-md-3 control-label" for="">&nbsp;</label>
	                            <div class="col-md-4">
	                                <div class="input text">
										<span class="required" style="color:#F00"> *</span>= Required
									</div>
	                            </div>
	                        </div>                                
                        <div class="form-group">
							 <?php echo $this->Form->label('name<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('name', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>
<div class="form-group">
							 <?php echo $this->Form->label('phone<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('phone', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>
						<div class="form-group">
							 <?php echo $this->Form->label('address' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('address', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>
<div class="form-group">
							 <?php echo $this->Form->label('business_type' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('businesstype', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>
						<div class="form-group">
							 <?php echo $this->Form->label('service_Required<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('service', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>

						<div class="form-group">
							 <?php echo $this->Form->label('notes' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('notes', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>

						<div class="form-group">
							 <?php echo $this->Form->label('status' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('servicestatus_id', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>
                            <div class="form-actions fluid">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn blue">Submit</button>
                                    <button type="button" class="btn default" onclick="window.location='<?php echo $this->webroot.'admin/businesses'; ?>'">Cancel</button>
                                </div>
                            </div>
                        </div>
                        <?php echo $this->Form->end(); ?>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>