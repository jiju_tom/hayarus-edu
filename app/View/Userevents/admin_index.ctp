<?php $this->Html->addCrumb('User events', '/admin/userevents'); $paginationVariables = $this->Paginator->params();?>
<div style='padding-bottom:10px;'><?php echo $this->Session->flash(); ?></div>
<div class="row">
    <div class="col-md-12">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
	                <i class="fa fa-list-alt"></i><?php echo __('User events'); ?>                </div>
                <!-- <div class="tools">
    		        <a href="javascript:;" class="collapse"></a>
            	</div> -->
            </div>
            <div class="portlet-body"><br />
                <div class="table-toolbar">
                    <div class="btn-group"> 
                       
		                
		                    <?php echo $this->Form->create('', array('type' => 'get','role' => 'form', 'url' => array('controller' => strtolower('Userevents'), 'action' => 'index', 'admin' => true))); ?>
		                   
		                    <div class="table-toolbar" style="margin-left: 0%;">
		                      

			                    <div class="col-md-5 col-sm-12">
			                      <div class="dataTables_filter" id="sample_editable_1_filter">
			                      <label >Log Date:
			                      <input class="form-control form-control-inline input-medium date-picker" size="16" name="created" type="text" value="<?php echo  isset($created) ? $created : ""; ?>" placeholder="month/date/year" /></label>
			                      </div>
			                    </div>

		                    

			                    <div class="col-md-4 col-sm-12">
			                          <div class="dataTables_filter" id="sample_editable_1_filter">
			                            <label>User: <?php echo $this->Form->input('updated_user', array('class' => 'form-control input-medium', 'label' => false, 'required' => false, 'options' => $users, 'empty'=>'Select', 'selected' => $updated_user));?></label>
			                          </div>
			                    </div>

			                    <div class="col-md-3 col-sm-12">
			                      <button class="btn green" type="submit" onclick="this.form.submit()" style="    margin-top: 30px;float: left;">Search <i class="fa fa-search"></i></button>
			                    </div>

			                </div>
		                    
		                  </form>
		               
                
                        <!-- /.row -->
                        
                    </div>
                    <!-- <div class="btn-group pull-right">
                        <div class="btn-group" style="padding-right:15px;">
	                        <?php echo $this->Html->link(__('New Userevent <i class="fa fa-plus"></i>'), array('action' => 'add'), array('class' => 'btn green','escape' => FALSE)); ?> 
                        </div>
                        <button class="btn default"  onclick="window.location.href='<?php echo $this->webroot.'admin/Userevents/export'; ?>'">Export to CSV <i class="fa fa-download"></i> </button>
                    </div> -->
                </div>
                <br />
                <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover " id="sample_1">
                  <thead>
                      <tr>
                        <th>Date</th>  
                        <th><?php echo $this->Paginator->sort('user_id'); ?></th>
                        <th><?php echo $this->Paginator->sort('action'); ?></th>
                        <th><?php echo $this->Paginator->sort('controller'); ?></th>
                        <!-- <th>Edit</th>
                        <th>Delete</th> -->
                      </tr>
                  </thead>
                  <?php  if(isset($userevents) && sizeof($userevents)>0) {?>
                  <tbody>
                  <?php $slno=0; foreach ($userevents as $userevent): $slno++?>
                  <tr>
                    <td><?php echo  date("d-m-Y H:i A", strtotime($userevent['Userevent']['created'])) ?></td>
					<td><?php echo $users[$userevent['Userevent']['user_id']]; ?></td>
					<td><?php echo h($userevent['Userevent']['action']); ?>&nbsp;</td>
					<td><?php echo h($userevent['Userevent']['controller']); ?>&nbsp;</td>
					<!-- <td><?php echo $this->Html->link(__('<i class="fa fa-edit"></i> Edit'), array('action' => 'edit', $userevent['Userevent']['id']), array('class' => 'btn default btn-xs purple','escape' => FALSE)); ?>
					</td>
					<td><?php echo $this->Form->postLink(__('<i class="fa fa-trash-o"></i> Delete'), array('action' => 'delete', $userevent['Userevent']['id']), array('confirm' => 'Are you sure you want to delete this?','class' => 'btn red btn-xs black','escape' => FALSE)); ?>
					</td> -->
					</tr>
									<?php endforeach; ?>
                   <?php } else {?>
                      <tr><td colspan='7' align='center'>No Records Found.</td></tr>
                  <?php }?>
                  </tbody>
                </table>
              </div>

                <div class="row">
    	            <div class="col-md-5 col-sm-12" style="margin-top:10px;">
                      <div id="sample_1_length" class="dataTables_length">
                        <?php echo $this->Form->create('', array( 'url' => array('controller' => strtolower('Userevents'), 'action' => 'index'))); ?>
                            <label>Show 
                          			<?php echo $this->Form->input('limit', array('name' => 'data[Userevent][limit]','type' => 'select', 'class' => 'form-control input-xsmall', 'label' => false,  'required' => false,'hiddenField' => false, 'style' => 'display:inline-block !important; padding-right:0px', 'onchange' => 'this.form.submit()', 'options' =>$default_limit_dropdown, 'default' => $limit, 'div' => false));?>
                            records
                            </label>
                            <?php echo $this->Form->end(); ?>
    					</div>
    				</div>
                        <div class="col-md-7 col-sm-12">
                          <div class="dataTables_paginate paging_bootstrap">
                            <?php $totalItem = $this->Paginator->counter('{:count}')?><?php $currentItem = $this->Paginator->counter('{:current}')?><?php if($totalItem>$currentItem) {?>
                            <ul class="pagination" style="visibility: visible;">
                                <li class="prev disabled">
                                			<?php echo $this->Paginator->prev('< ' . __(''), array(), null, array('class' => 'prev disabled'));?>
	
                                </li>
                                <li>
                                			<?php		echo $this->Paginator->numbers(array('separator' => ''));	?>
                                </li>
                                <li class="next disabled">
                                			<?php		echo $this->Paginator->next(__('') . ' >', array(), null, array('class' => 'next'));	?>
                                </li>
                            </ul>
                      <?php }?>
    						        </div>
                      </div>
    				    </div>
    			</div>
    	</div>
    <!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
