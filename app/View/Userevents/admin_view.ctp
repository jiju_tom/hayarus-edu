<?php $this->Html->addCrumb('Userevent', '/admin/userevents'); ?>
<?php $this->Html->addCrumb('View', ''); ?>
<div style='padding-bottom:10px;'><?php echo $this->Session->flash(); ?></div>

<div class="row">
    <div class="col-md-12">
        <div class="tab-content">
            <div class="tab-pane active" id="tab_0">
                 <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                        	<i class="fa fa-reorder"></i><?php echo __('View Userevent'); ?>                        				</div>
	                        <div class="tools">
	                            <a href="javascript:;" class="collapse"></a>
	                        </div>
	                    </div>
                    	<div class="portlet-body form userevents">
		                    <div class="alert alert-danger display-hide">
		                        <button data-close="alert" class="close"></button>
		                        You have some form errors. Please check below.
		                    </div>
		                    <!-- BEGIN FORM-->
		                    <?php echo $this->Form->create('Userevent', array('class' => 'form-horizontal')); ?>
		                    <div class="form-body">                      
		                        						<div class='form-group'>
							 <?php echo $this->Form->label('user_id:' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<p class="form-control-static"><?php echo h($userevent['Userevent']['user_id']);?></p>
							</div>
						</div>
						<div class='form-group'>
							 <?php echo $this->Form->label('action:' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<p class="form-control-static"><?php echo h($userevent['Userevent']['action']);?></p>
							</div>
						</div>
						<div class='form-group'>
							 <?php echo $this->Form->label('controller:' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<p class="form-control-static"><?php echo h($userevent['Userevent']['controller']);?></p>
							</div>
						</div>
		                            <div class="form-actions fluid">
		                                <div class="col-md-offset-3 col-md-9">
		                                    <button type="button" class="btn blue" onclick="window.location='<?php echo $this->webroot.'admin/userevents'; ?>'">Go Back</button>
		                                </div>
		                            </div>
		                        </div>
		                        <?php echo $this->Form->end(); ?>
		                        <!-- END FORM-->
		                    </div>
                	</div>
            </div>
        </div>
    </div>
</div>