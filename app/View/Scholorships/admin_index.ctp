<?php $this->Html->addCrumb('Scholarship', '/admin/scholorships'); 
$paginationVariables = $this->Paginator->params();
$groupId = $this->Session->read('Auth.Group.id');
 
$name = isset($name) ? $name : "";
$code = isset($code) ? $code : "";

?>
<div style='padding-bottom:10px;'><?php echo $this->Session->flash(); ?></div>
<div class="row">
    <div class="col-md-12">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
	                <i class="fa fa-list-alt"></i><?php echo __('Scholorships'); ?>                </div>
                <!-- <div class="tools">
    		        <a href="javascript:;" class="collapse"></a>
            	</div> -->
            </div>
            <div class="portlet-body"><br />
              <div class="row" style="margin-left: 13px;">
                <div class="table-toolbar">
                  <?php echo $this->Form->create('', array('type' => 'get','role' => 'form', 'url' => array('controller' => strtolower('Scholorships'), 'action' => 'index', 'admin' => true))); ?>
                  <div class="table-toolbar">
                        <div class="col-md-3 col-sm-12">
                          <div class="dataTables_filter" id="sample_editable_1_filter">
                            <label>Name: <input type="text" name="name" aria-controls="sample_editable_1" class="form-control input-medium" value="<?php echo $name; ?>"></label>
                          </div>
                        </div>

                        <div class="col-md-3 col-sm-12">
                          <div class="dataTables_filter" id="sample_editable_1_filter">
                            <label>Code: <input type="text" name="code" aria-controls="sample_editable_1" class="form-control input-medium" value="<?php echo $code; ?>"></label>
                          </div>
                        </div>

                        

                        

                        <div class="col-md-3 col-sm-12">
                          <button class="btn green" type="submit" onclick="this.form.submit()" style="    margin-top: 30px;float: left;">Search <i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    </form>

                    
                    <div class="btn-group pull-right">
                        <!-- <div class="btn-group" style="padding-right:15px;">
	                        <?php echo $this->Html->link(__('New Scholorship <i class="fa fa-plus"></i>'), array('action' => 'add'), array('class' => 'btn green','escape' => FALSE)); ?> 
                        </div> -->
                        <!-- <button class="btn default"  onclick="window.location.href='<?php echo $this->webroot.'admin/Scholorships/export'; ?>'">Export to CSV <i class="fa fa-download"></i> </button> -->
                    </div>
                </div>
              </div>
                <br />
                <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover " id="sample_1">
                  <thead>
                      <tr>
                        <th>#</th>  
                        <th>Name</th>
                        <th>Email</th>
                        <th>School</th>
                        <th>Status</th>
                        <th>Edit</th>
                        <?php if($groupId == 1){ ?>
                          <th>Delete</th>
                        <?php } ?>
                      </tr>
                  </thead>
                  <?php  if(isset($scholorships) && sizeof($scholorships)>0) {?>
                  <tbody>
                  <?php $slno=0; foreach ($scholorships as $scholorship): $slno++?>
                  <tr>
                     <td><?php echo $slno+$paginationVariables['limit']*($paginationVariables['page']-1); ?></td>
        					<td>
											<?php echo $users[$scholorship['User']['id']]; ?>
		              </td>
                  <td>
											<?php echo $scholorship['User']['email']; ?>
		              </td>
                  <td>
											<?php echo $scholorship['User']['school']; ?>
		              </td>
                  <td>
                    <a href="##" class="btn default btn-xs green"><i class="fa fa-gear"></i> <?php echo $scholorship_status[$scholorship['Scholorship']['status']]; ?></a>
                      
                  </td>
											<td>
													<?php echo $this->Html->link(__('<i class="fa fa-edit"></i> View'), array('action' => 'view', $scholorship['Scholorship']['user_id']), array('class' => 'btn default btn-xs purple','escape' => FALSE)); ?>

											</td>
                      <?php if($groupId == 1){ ?>
											<td>
													<?php echo $this->Form->postLink(__('<i class="fa fa-trash-o"></i> Delete'), array('action' => 'delete', $scholorship['Scholorship']['id']), array('confirm' => 'Are you sure you want to delete this?','class' => 'btn red btn-xs black','escape' => FALSE)); ?>
											</td>
                      <?php } ?>
										</tr>
									<?php endforeach; ?>
                   <?php } else {?>
                      <tr><td colspan='6' align='center'>No Records Found.</td></tr>
                  <?php }?>
                  </tbody>
                </table>
              </div>
                <div class="row">
    	            <div class="col-md-5 col-sm-12" style="margin-top:10px;">
                      <div id="sample_1_length" class="dataTables_length">
                        <?php echo $this->Form->create('', array( 'url' => array('controller' => strtolower('Scholorships'), 'action' => 'index'))); ?>
                            <label>Show 
                          			<?php echo $this->Form->input('limit', array('name' => 'data[Scholorship][limit]','type' => 'select', 'class' => 'form-control input-xsmall', 'label' => false,  'required' => false,'hiddenField' => false, 'style' => 'display:inline-block !important; padding-right:0px', 'onchange' => 'this.form.submit()', 'options' =>$default_limit_dropdown, 'default' => $limit, 'div' => false));?>
                            records
                            </label>
                            <?php echo $this->Form->end(); ?>
                					</div>
                				</div>
                        <div class="col-md-7 col-sm-12">
                          <div class="dataTables_paginate paging_bootstrap">
                            <?php $totalItem = $this->Paginator->counter('{:count}')?><?php $currentItem = $this->Paginator->counter('{:current}')?><?php if($totalItem>$currentItem) {?>
                            <ul class="pagination" style="visibility: visible;">
                                <li class="prev disabled">
                                			<?php echo $this->Paginator->prev('< ' . __(''), array(), null, array('class' => 'prev disabled'));?>
	
                                </li>
                                <li>
                                			<?php		echo $this->Paginator->numbers(array('separator' => ''));	?>
                                </li>
                                <li class="next disabled">
                                			<?php		echo $this->Paginator->next(__('') . ' >', array(), null, array('class' => 'next'));	?>
                                </li>
                            </ul>
                      <?php }?>
    						        </div>
                      </div>
    				    </div>
    			</div>
    	</div>
    <!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
