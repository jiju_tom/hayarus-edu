<?php
App::uses('AppController', 'Controller');
/**
 * Businesstypes Controller
 *
 * @property Businesstype $Businesstype
 * @property PaginatorComponent $Paginator
 */
class BusinesstypesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

public function beforeFilter() {
		parent::beforeFilter();
		$this->layout='admin_default';
	}
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		if (!empty($this->data)){
			if(isset($this->data['Businesstype']['limit'])){
            	$limit = $this->data['Businesstype']['limit'];
				$this->Session->write('default_limit', $limit);
			}
		}else{
			if($this->Session->check('default_limit'))
				$limit = $this->Session->read('default_limit');
			else
				$limit = $this->default_limit;
		}
		$search_conditions = array();
		$conditions = array();
		$this->set("search_string", "");
		if(isset($this->params->query['search'])){
			$this->set("search_string", $this->params->query['search']);
			$conditions = array('OR' => array(
			'Businesstype.name LIKE "%'.trim(addslashes($this->params->query['search'])).'%"')); 
		}
		$this->paginate  = array(
				'limit' => $limit, 
				'order' => 'Businesstype.id DESC',
				'conditions' => array(array_merge($conditions,$search_conditions))
			);
		$this->Businesstype->recursive = 0;
		$this->set('businesstypes', $this->Paginator->paginate());
		$this->set('limit', $limit);
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Businesstype->exists($id)) {
			throw new NotFoundException(__('Invalid businesstype'));
		}
		$options = array('conditions' => array('Businesstype.' . $this->Businesstype->primaryKey => $id));
		$this->set('businesstype', $this->Businesstype->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Businesstype->create();
			if ($this->Businesstype->save($this->request->data)) {
				$this->Session->setFlash('The businesstype has been added successfully.','flash_success');
				return $this->redirect(array('action' => 'edit', $this->Businesstype->id));
			} else {
				$this->Session->setFlash('The businesstype could not be added. Please, try again.','flash_failure');
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Businesstype->exists($id)) {
			throw new NotFoundException(__('Invalid businesstype'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Businesstype->save($this->request->data)) {
				$this->Session->setFlash('The businesstype has been updated successfully.','flash_success');
				return $this->redirect(array('action' => 'edit', $this->Businesstype->id));
			} else {
				$this->Session->setFlash('The businesstype could not be updated. Please, try again.','flash_failure');
			}
		} else {
			$options = array('conditions' => array('Businesstype.' . $this->Businesstype->primaryKey => $id));
			$this->request->data = $this->Businesstype->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Businesstype->id = $id;
		if (!$this->Businesstype->exists()) {
			throw new NotFoundException(__('Invalid businesstype'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Businesstype->delete()) {
			$this->Session->setFlash('The businesstype has been deleted successfully.','flash_success');
		} else {
			$this->Session->setFlash('The businesstype could not be deleted. Please, try again.','flash_failure');
		}
		return $this->redirect(array('action' => 'index'));
	}

	/**
 * admin_export method
 *
 * @throws NotFoundException
 * @return csv
 */ 
 
	 public function admin_export() {
	    $results = $this->Businesstype->find('all');
	    $this->response->download('Crowdfunding-Export-'.'businesstypes-'.date('d-m-Y').'.csv');
	    $_serialize = 'results';
	    $_header = array('Businesstype ID', 'Status', 'Created');
	    $_extract = array('Businesstype.id', 'Businesstype.status', 'Businesstype.created');
		$this->viewClass = 'CsvView.Csv';
	    $this->set(compact('results', '_serialize', '_header', '_extract'));
	 }
}
