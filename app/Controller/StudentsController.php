<?php
App::uses('AppController', 'Controller');
/**
 * Students Controller
 *
 * @property Student $Student
 * @property PaginatorComponent $Paginator
 */
class StudentsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

public function beforeFilter() {
		parent::beforeFilter();
		$this->layout='admin_default';
	}

	public function beforeRender(){

		$this->loadModel('Userevent');

		if($this->request->params['controller'] != 'userevents'){

			//Logging
				$loguser = $this->Session->read('Auth.User');
				$this->Userevent->data['user_id'] = $loguser['id'];
				$this->Userevent->data['controller'] = $this->request->params['controller']; 
				$this->Userevent->data['action'] = $this->request->params['action']; 
				//pr($this->Userevent->data);exit;
				$this->Userevent->save($this->Userevent->data);
			//end logging
		}
	}
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$groupId = $this->Session->read('Auth.Group.id');
		if (!empty($this->data)){
			if(isset($this->data['Student']['limit'])){
            	$limit = $this->data['Student']['limit'];
				$this->Session->write('default_limit', $limit);
			}
		}else{
			if($this->Session->check('default_limit'))
				$limit = $this->Session->read('default_limit');
			else
				$limit = $this->default_limit;
		}
		$search_conditions = array();
		$conditions = array();
		$this->set("search_string", "");
		if(isset($this->params->query['name']) && $this->params->query['name']!= null){
			$this->set("name", $this->params->query['name']);
			array_push($search_conditions, array('OR' => array(
			'Student.name LIKE "%'.trim(addslashes($this->params->query['name'])).'%"'))); 
		}

		if(isset($this->params->query['phone']) && $this->params->query['phone']!= null){
			$this->set("phone", $this->params->query['phone']);
			array_push($search_conditions, array('OR' => array(
			'Student.phone LIKE "%'.trim($this->params->query['phone']).'%"'))); 
		}

		if(isset($this->params->query['school']) && $this->params->query['school']!= null){
			$this->set("school", $this->params->query['school']);
			array_push($search_conditions, array('OR' => array(
			'Student.school LIKE "%'.trim($this->params->query['school']).'%"'))); 
		}

		if(isset($this->params->query['district']) && $this->params->query['district']!= null){
			$this->set("district", $this->params->query['district']);
			array_push($search_conditions, array('OR' => array(
			'Student.district LIKE "%'.trim($this->params->query['district']).'%"'))); 
		}else{
			$this->set("district", "");
		}

		if(isset($this->params->query['status_id']) && $this->params->query['status_id']!= null){
			$this->set("status_id", $this->params->query['status_id']);
			array_push($search_conditions, array('OR' => array(
			'Student.status_id = "'.$this->params->query['status_id'].'"'))); 
		}

		if(isset($this->params->query['status_id']) && $this->params->query['status_id']!= null){
			$this->set("status_id", $this->params->query['status_id']);
			array_push($search_conditions, array('OR' => array(
			'Student.status_id = "'.$this->params->query['status_id'].'"'))); 
		}

		if(isset($this->params->query['created']) && $this->params->query['created']!= null){
			$this->set("created", $this->params->query['created']);
			$date = date("Y-m-d", strtotime($this->params->query['created']));
			array_push($search_conditions, array('OR' => array(
			'Student.created LIKE "%'.$date.'%"'))); 
		}

		if(isset($this->params->query['next_followup_date']) && $this->params->query['next_followup_date']!= null){
			$this->set("next_followup_date", $this->params->query['next_followup_date']);
			$date = date("Y-m-d", strtotime($this->params->query['next_followup_date']));
			array_push($search_conditions, array('OR' => array(
			'Student.next_followup_date LIKE "%'.$date.'%"'))); 
		}

		if(isset($this->params->query['modified']) && $this->params->query['modified']!= null){
			$this->set("modified", $this->params->query['modified']);
			$date = date("Y-m-d", strtotime($this->params->query['modified']));
			array_push($search_conditions, array('OR' => array(
			'Student.modified LIKE "%'.$date.'%"'))); 
		}

		if(isset($this->params->query['user_id']) && $this->params->query['user_id']!= null){
			//pr($this->params->query);exit;
			$this->set("user_id", $this->params->query['user_id']);
			array_push($search_conditions, array('OR' => array(
			'Student.user_id = "'.$this->params->query['user_id'].'"'))); 
		}else{
			$this->set("user_id", "");
		}

		if(isset($this->params->query['updated_user']) && $this->params->query['updated_user']!= null){
			//pr($this->params->query);exit;
			$this->set("updated_user", $this->params->query['updated_user']);
			array_push($search_conditions, array('OR' => array(
			'Student.updated_user = "'.$this->params->query['updated_user'].'"'))); 
		}else{
			$this->set("updated_user", "");
		}

		//array_push($search_conditions, array('OR' => array('Student.status_id != ' => 8 )));
		//array_push($search_conditions, array('OR' => array('Student.type' => 0 )));

		//pr($search_conditions);
		$this->paginate  = array(
				'limit' => $limit, 
				'order' => 'Student.id DESC',
				'conditions' => array(array_merge($conditions,$search_conditions))
			);

		$this->Student->recursive = 0;
		$this->LoadModel('User');
		$options = array('conditions' => array('User.type'=> 0), 'fields' => 'first_name');
		$users = $this->User->find('list', $options); 
		$statuses = $this->Student->Status->find('list');

		$options = array('conditions' => array('Student.type'=> 0));
		$studentCount =  $this->Student->find('count', $options); //pr($studentCount);
		$this->set('statuses', $statuses);
		$this->set('students', $this->Paginator->paginate());
		$this->set('limit', $limit);
		$this->set(compact('statuses', 'users', 'groupId', 'studentCount'));
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Student->exists($id)) {
			throw new NotFoundException(__('Invalid student'));
		}
		$options = array('conditions' => array('Student.' . $this->Student->primaryKey => $id));
		$this->set('student', $this->Student->find('first', $options));
	}


	public function admin_invoice($id = null) {
		
		$options = array('conditions' => array('Student.' . $this->Student->primaryKey => $id));
		$this->set('student', $this->Student->find('first', $options));

		$this->LoadModel('Course');
		$courses = $this->Course->find('list');

		$this->LoadModel('College');
		$colleges = $this->College->find('list');

		$this->set(compact('courses','colleges'));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		$groupId = $this->Session->read('Auth.Group.id');
		$user = $this->Session->read('Auth.User');

		if ($this->request->is('post')) {

			$this->Student->create();
			$this->request->data['Student']['user_id'] = $user['id'];
			if($groupId == 4){
				$this->request->data['Student']['type'] = 1;
			}

			if ($this->Student->save($this->request->data)) {

				$this->Session->setFlash('The student has been added successfully.','flash_success');

				return $this->redirect($this->Session->read('referUrlAdd'));
			} else {
				$this->Session->setFlash('The student could not be added. Please, try again.','flash_failure');
			}
		}
		$refer_url = $this->request->referer(); //pr($refer_url);
		$this->Session->write('referUrlAdd', $refer_url);
		//$cities = $this->Student->City->find('list');
		$statuses = $this->Student->Status->find('list');
		//$users = $this->Student->User->find('list');
		//$colleges = $this->Student->College->find('list');
		//$courses = $this->Student->Course->find('list');
		$this->set(compact('statuses'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Student->exists($id)) {
			throw new NotFoundException(__('Invalid student'));
		}
		if ($this->request->is(array('post', 'put'))) { //pr($this->request->data['Student']['status_id']);exit;
			
			$this->request->data['Student']['view_status'] = 1;
			$this->request->data['Student']['next_followup_date'] = date("Y-m-d", strtotime($this->request->data['Student']['next_followup_date']));
			if ($this->Student->save($this->request->data)) {
				$this->Session->setFlash('The student has been updated successfully.','flash_success');
				return $this->redirect($this->Session->read('referUrlEdit'));
			} else {
				$this->Session->setFlash('The student could not be updated. Please, try again.','flash_failure');
			}
		} else {
			$options = array('conditions' => array('Student.id' => $id));
			$this->request->data = $this->Student->find('first', $options);
			$this->set('next_followup_date', date("d-m-Y", strtotime($this->request->data['Student']['next_followup_date'])));
		}

		$refer_url = $this->request->referer(); //pr($refer_url);
		$this->Session->write('referUrlEdit', $refer_url);
		//$cities = $this->Student->City->find('list');
		$statuses = $this->Student->Status->find('list');
		//$users = $this->Student->User->find('list');
		$this->LoadModel('College');
		$colleges = $this->College->find('list');
		$this->LoadModel('Course');
		$courses = $this->Course->find('list');
		$prev_url = $this->request->referer();
		$this->set(compact('statuses', 'courses', 'colleges', 'prev_url'));
	}


/**
 * admin_attend method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_attend($id = null) {
		
		
		$userId = $this->Auth->user('id');
		$groupId = $this->Session->read('Auth.Group.id');
		$this->LoadModel('Course');

		if (!$this->Student->exists($id)) {
			throw new NotFoundException(__('Invalid student'));
		}
		if ($this->request->is(array('post', 'put'))) { //pr($this->request->data);exit;
			
			$this->request->data['Student']['updated_user'] = $userId;
			$this->request->data['Student']['next_followup_date'] = date("Y-m-d", strtotime($this->request->data['Student']['next_followup_date']));
			//pr(date("Y-m-d", strtotime($this->request->data['Student']['next_followup_date'])));exit;

			if($this->request->data['Student']['status_id'] == 6 && $this->request->data['Student']['invoice_id'] == 0){

					$this->LoadModel('College');
					$this->College->recursive = -1;
					$options = array('conditions' => array('College.id' => $this->request->data['Student']['college_id'], ), 'fields' => array('College.name'));
					$collegeName = $this->College->find('first', $options);

					$this->LoadModel('Course');
					$this->Course->recursive = -1;
					$options = array('conditions' => array('Course.id' => $this->request->data['Student']['course_id'], ), 'fields' => array('Course.name'));
					$courseName = $this->Course->find('first', $options);
					//pr($collegeName);exit;

					$this->loadModel('Invoice');
					$this->request->data['Invoice']['invoice_no'] = rand ( 100000 , 999999 );
					$this->request->data['Invoice']['user_id'] = $userId;
					$this->request->data['Invoice']['issued_to'] = $this->request->data['Student']['name'];
					$this->request->data['Invoice']['about'] = $courseName['Course']['name'] . " in ". $collegeName['College']['name'];
					//pr($this->request->data['Invoice']);exit;
					$options = array('conditions' => array('Invoice.invoice_no' => $this->request->data['Invoice']['invoice_no']));
					$existingInvoice  = $this->Invoice->find('first', $options);
					if(empty($existingInvoice)){
						$this->Invoice->save($this->request->data['Invoice']);

						$this->request->data['Student']['invoice_id'] =  $this->Invoice->id;
					}	
			}

			if ($this->Student->save($this->request->data)) {
				
				$this->Session->setFlash('The student has been updated successfully.','flash_success');
				return $this->redirect($this->Session->read('referUrl'));

			} else {
				$this->Session->setFlash('The student could not be updated. Please, try again.','flash_failure');
			}
		} else {
			$options = array('conditions' => array('Student.id' => $id));
			$this->request->data = $this->Student->find('first', $options);
			$this->set('next_followup_date', date("d-m-Y", strtotime($this->request->data['Student']['next_followup_date'])));
		}
		$refer_url = $this->request->referer(); //pr($refer_url);
		$this->Session->write('referUrl', $refer_url);
		//$cities = $this->Student->City->find('list');
		$statuses = $this->Student->Status->find('list');
		//$users = $this->Student->User->find('list');
		$this->LoadModel('College');
		$colleges = $this->College->find('list');

		$prev_url = $this->request->referer();

		$courses = $this->Course->find('list');
		$this->set(compact('statuses', 'groupId','prev_url', 'courses', 'colleges'));
	}



	/**
 * admin_admission method
 *
 * @return void
 */
	public function admin_admission() {
		$userId = $this->Auth->user('id');
		$groupId = $this->Session->read('Auth.Group.id');

		if (!empty($this->data)){
			if(isset($this->data['Student']['limit'])){
            	$limit = $this->data['Student']['limit'];
				$this->Session->write('default_limit', $limit);
			}
		}else{
			if($this->Session->check('default_limit'))
				$limit = $this->Session->read('default_limit');
			else
				$limit = $this->default_limit;
		}
		$search_conditions = array();
		$conditions = array();
		$this->set("search_string", "");
		if(isset($this->params->query['name']) && $this->params->query['name']!= null){
			$this->set("name", $this->params->query['name']);
			array_push($search_conditions, array('OR' => array(
			'Student.name LIKE "%'.trim(addslashes($this->params->query['name'])).'%"'))); 
		}

		if(isset($this->params->query['phone']) && $this->params->query['phone']!= null){
			$this->set("phone", $this->params->query['phone']);
			array_push($search_conditions, array('OR' => array(
			'Student.phone LIKE "%'.trim($this->params->query['phone']).'%"'))); 
		}
		if(isset($this->params->query['status_id']) && $this->params->query['status_id']!= null){
			$this->set("status_id", $this->params->query['status_id']);
			array_push($search_conditions, array('OR' => array(
			'Student.status_id = "'.$this->params->query['status_id'].'"'))); 
		}

		//array_push($search_conditions, array('OR' => array('Student.status_id != ' => 8 )));
		//array_push($search_conditions, array('OR' => array('Student.updated_user ' => $userId )));

		array_push($search_conditions, array('OR' => array('Student.user_id' => $userId, 'Student.updated_user ' => $userId )));

		if($groupId == 4){
			array_push($search_conditions, array('OR' => array('Student.type' => 1 )));
		}

		//pr($search_conditions);
		$this->paginate  = array(
				'limit' => $limit, 
				'order' => 'Student.id DESC',
				'conditions' => array(array_merge($conditions,$search_conditions))
			);
		$this->Student->recursive = 0;
		$this->LoadModel('User');
		$users = $this->User->find('list', array('fields' => 'first_name')); 
		$statuses = $this->Student->Status->find('list');
		$this->set('statuses', $statuses);
		$this->set('students', $this->Paginator->paginate());
		$this->set('limit', $limit);
		$this->set('users', $users);
		$this->set(compact('userId'));
	}



/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Student->id = $id;
		if (!$this->Student->exists()) {
			throw new NotFoundException(__('Invalid student'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Student->delete()) {
			$this->Session->setFlash('The student has been deleted successfully.','flash_success');
		} else {
			$this->Session->setFlash('The student could not be deleted. Please, try again.','flash_failure');
		}
		return $this->redirect(array('action' => 'index'));
	}

	/**
 * admin_export method
 *
 * @throws NotFoundException
 * @return csv
 */ 
 
	 public function admin_import() {
	 	$this->autoRender = false;
	 	$userId = $this->Auth->user('id');
	 	$groupId = $this->Session->read('Auth.Group.id');
	 	$type = 0;
	 	if($groupId == 4){
	 		$type = 1;
	 	}

	    if(isset($this->request->data) && $this->request->data['Student'] != null){

	    	//live db Connection
	    	$dbHost = 'localhost';
			$dbUsername = 'hayarus_portal-edu';
			$dbPassword = 'portal-edu@2021';
			$dbName = 'hayarus_portal-edu';


            /*$dbHost = 'localhost';
			$dbUsername = 'root';
			$dbPassword = '';
			$dbName = 'hayarus_edu';*/
			//connect with the database
			$conn = new mysqli($dbHost, $dbUsername, $dbPassword, $dbName);
			if($conn->connect_errno){
			    echo "Failed to connect to MySQL: (" . $conn->connect_errno . ") " . $conn->connect_error;
			}
		         
			$file = $this->request->data['Student']['tmp_name'];
			$handle = fopen($file, "r");
			$c = 0;
			$existing = 0;
			while(($filesop = fgetcsv($handle, 5000, ",")) !== false)
			{

				$name = ($filesop[0]!= null) ? $filesop[0] : "";
				$phone = ($filesop[1]!= null) ? $filesop[1] : "";
				$school = ($filesop[2]!= null) ? $filesop[2] : "";
				$division = ($filesop[3]!= null) ? $filesop[3] : "";
				$district = ($filesop[4]!= null) ? $filesop[4] : "";
				$date = date("Y-m-d H:i:s");

				$studentArr = $conn->query("SELECT * FROM he_students WHERE `he_students`.`phone` = '".$phone."' ");
				//print_r($studentArr->num_rows);exit;
				if($studentArr->num_rows == 0){
					$conn->query("INSERT INTO he_students (name, phone, school, division, district, user_id, type, created) VALUES('".$name."','".$phone."','".$school."','".$division."','".$district."','".$userId."','".$type."', '".$date."' )");
					$c = $c + 1;
				}else{
					$existing = $existing + 1;
				}
							        

				
			}
			//echo $c;exit;
			if($c > 0){
				$this->Session->setFlash(''.$c.' Records have been added successfully.','flash_success');
			}
			
			if($existing > 0){
				$this->Session->setFlash(''.$existing.' Records already exists.','flash_failure');
			}
			
			
			$this->redirect( Router::url( $this->referer(), true ) );


	 	}
 	}

 	/**
 * admin_checkDuplicatePhone method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_checkDuplicatePhone() {
		$this->autoRender = false;
		$phone = $this->request->data['phone'];
		if($phone != null){

			$options = array('conditions' => array('Student.phone' => $phone));
			$student = $this->Student->find('first', $options);
			//pr($student);exit;

			if(empty($student)){
				echo 0;
			}else{
				echo 1;
			}

		}
	}
}
