<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class DashboardController extends AppController {

/**
 * This controller does not use a model
 *
 * @var array
 */
		//public $uses = array('Project','Award');
	public $components = array('Paginator');
	
/**
 * Displays a view
 *
 * @param mixed What page to display
 * @return void
 * @throws NotFoundException When the view file could not be found
 *	or MissingViewException in debug mode.
 */
 
 
 	public function beforeFilter() {
		parent::beforeFilter();
		//$this->Auth->allow('admin_index','admin_searchdetail');
	}

	////shows the count of Traveled places and advertisements
	public function admin_index() {

		$this->loadModel('College');
		$this->layout='admin_default';
		//pr($this->data);exit;
		if (!empty($this->data['limit'])){ 
			if(isset($this->data['College']['limit'])){
            	$limit = $this->data['College']['limit'];
				$this->Session->write('default_limit', $limit);
			}
		}else{
			if($this->Session->check('default_limit'))
				$limit = $this->Session->read('default_limit');
			else
				$limit = $this->default_limit;
		}
		$search_conditions = array();
		$conditions = array();
		$this->set("search_string", "");
		
		if(isset($this->params->query['college_id']) && $this->params->query['college_id']!= null){ 
			$this->set("college_id", $this->params->query['college_id']);
			$conditions1 = array('OR' => array(
			'College.id = '.$this->params->query['college_id'].'')); 
			array_push($search_conditions, $conditions1);
		}
		if(isset($this->params->query['city_id']) && $this->params->query['city_id']!= null){
			$this->set("city_id", $this->params->query['city_id']);
			array_push($search_conditions, array('OR' => array(
			'College.city_id = '.$this->params->query['city_id'].'')));
		}

		if(isset($this->params->query['state_id']) && $this->params->query['state_id']!= null){
			$this->set("state_id", $this->params->query['state_id']);
			array_push($search_conditions, array('OR' => array(
			'College.state_id = '.$this->params->query['state_id'].'')));
		}

		if(isset($this->params->query['country_id']) && $this->params->query['country_id']!= null){
			$this->set("country_id", $this->params->query['country_id']);
			array_push($search_conditions, array('OR' => array(
			'College.country_id = '.$this->params->query['country_id'].'')));
		}
		
		if(isset($this->params->query['rating']) && $this->params->query['rating']!= null){
			$this->set("ratedVal", $this->params->query['rating']);
			array_push($search_conditions, array('OR' => array(
			'College.rating = '.$this->params->query['rating'].'')));
		}

		$this->loadModel('Collegecourse');
		if(isset($this->params->query['course_id']) && $this->params->query['course_id']!= null){
			$this->set("course_id", $this->params->query['course_id']);

			$collegecourses = $this->Collegecourse->find('list', array('fields' => array('college_id'), 'conditions' =>array('Collegecourse.course_id = '.$this->params->query['course_id'].'') ));
			$collegeIds = array_values($collegecourses);
			array_push($search_conditions, array('OR' => array(
			'College.id' => $collegeIds)));	
		
		
		}

		$this->loadModel('Course');
		if(isset($this->params->query['areaofstudy_id']) && $this->params->query['areaofstudy_id']!= null){
			$this->set("areaofstudy_id", $this->params->query['areaofstudy_id']);

			$coursesByAreaofstudy = $this->Course->find('list', array('fields' => array('id'), 'conditions' =>array('Course.areaofstudy_id = '.$this->params->query['areaofstudy_id'].'') ));
			$courseIdsByAreaofstudy = array_values($coursesByAreaofstudy);



			$collegecoursesByCourse = $this->Collegecourse->find('list', array('fields' => array('college_id'), 'conditions' =>array('Collegecourse.course_id ' => $courseIdsByAreaofstudy) ));

			$collegeIds = array_values($collegecoursesByCourse);

			array_push($search_conditions, array('OR' => array(
			'College.id' => $collegeIds)));	
		
		
		}


		$this->paginate  = array(
				'limit' => $limit, 
				'order' => 'College.id DESC',
				'conditions' => array($search_conditions)
			);
		$this->College->recursive = 1;
		$this->set('collegelists', $this->Paginator->paginate('College'));

		
		$this->set('limit', $limit);

		$this->loadModel('Country');
		$this->loadModel('States');
		$this->loadModel('City');
		$this->loadModel('Course');
		$this->loadModel('Areaofstudy');

		$colleges = $this->College->find('list');
		$courses = $this->Course->find('list');
		$areaofstudies = $this->Areaofstudy->find('list');
		$countries = $this->Country->find('list'); 
		$states = $this->States->find('list'); 
		$cities = $this->City->find('list'); 

		$this->loadModel('Scholorship');
		$user = $this->Session->read('Auth.User');
		$scholorshipCount = $this->Scholorship->find('count', array('conditions' => array('Scholorship.user_id' => $user['id'])));

		$this->loadModel('User');
		$userDetails = $this->User->find('first', array('conditions' => array('User.id' => $user['id'])));
		$this->set(compact('countries', 'states', 'cities', 'colleges', 'courses', 'areaofstudies', 'scholorshipCount', 'userDetails'));
	}

	public function admin_searchdetail($id = null) {

		$groupId = $this->Session->read('Auth.Group.id');
		$user = $this->Session->read('Auth.User');

		$this->layout='admin_default';

		$this->loadModel('User');
		$this->request->data['User']['id'] = $user['id'];
		$this->request->data['User']['first_landing'] = 0;
		//pr($this->User->save($this->request->data));exit;
		$this->User->save($this->request->data);

		$this->loadModel('College');
		$options = array('conditions' => array('College.' . $this->College->primaryKey => $id));
			$this->request->data = $this->College->find('first', $options);
			//pr($this->request->data);


		$this->loadModel('Collegecourse');
		$collegecoursesByCourse = $this->Collegecourse->find('list', array('fields' => array('course_id'), 'conditions' =>array('Collegecourse.college_id ' => $id) ));
		
		$collegecourseIds = array_values($collegecoursesByCourse);

		$collegesByCourseIds = $this->Collegecourse->find('list', array('fields' => array('college_id'), 'conditions' =>array('Collegecourse.course_id ' => $collegecourseIds) ));

		
		$this->College->recursive = -1;
		$collegeIds = array_unique($collegesByCourseIds);
		$collegesByCourseIds = $this->College->find('all', array('fields' => array('id', 'name', 'location_name','city_id', 'state_id'), 'conditions' =>array('College.id ' => $collegeIds, 'College.id !=' =>$id)));

		//pr($collegesByCourseIds);


		$this->loadModel('Country');
		$this->loadModel('States');
		$this->loadModel('City');
		$this->loadModel('Course');
		$this->loadModel('Areaofstudy');
		$this->loadModel('Scholorship');

		$countries = $this->Country->find('list'); 
		$states = $this->States->find('list'); 
		$cities = $this->City->find('list'); 
		$courses = $this->Course->find('list');
		$areaofstudies = $this->Areaofstudy->find('list');

		
		//pr($user);
		$user = $this->Session->read('Auth.User');

		$this->Scholorship->recursive = -1;
		$options = array('conditions' => array('Scholorship.college_id'=> $id, 'Scholorship.user_id'=>$user['id']));
		$scholorships = $this->Scholorship->find('all', $options);

		
		$scholorshipCount = $this->Scholorship->find('count', array('conditions' => array('Scholorship.user_id' => $user['id'])));

		$college_type_dropdown = $this->college_type_dropdown;



		$this->set(compact('countries', 'states', 'cities', 'courses', 'areaofstudies', 'collegesByCourseIds', 'college_type_dropdown', 'groupId', 'scholorships', 'scholorshipCount'));

	}
}
