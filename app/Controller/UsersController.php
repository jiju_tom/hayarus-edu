<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

public function beforeFilter() {
		parent::beforeFilter();
		$this->layout='admin_default';
		$this->Auth->allow('admin_forgotpassword','admin_resetforgottenpassword','admin_login','admin_add','checkEmail','admin_logout', 'admin_registration', 'admin_loginnew');
	}

	public function beforeRender(){

		$this->loadModel('Userevent');

		if($this->request->params['controller'] != 'userevents'){

			//Logging
				$loguser = $this->Session->read('Auth.User');
				$this->Userevent->data['user_id'] = $loguser['id'];
				$this->Userevent->data['controller'] = $this->request->params['controller']; 
				$this->Userevent->data['action'] = $this->request->params['action']; 
				//pr($this->Userevent->data);exit;
				$this->Userevent->save($this->Userevent->data);
			//end logging
		}
	}


/* Function for super admin login*/
	 public function admin_login(){
		$this->layout="admin_login";
		if($this->request->is('post')){//pr($this->request->data);
			$this->request->data['User']['email']=$this->request->data['User']['email'];
			$this->request->data['User']['password']=$this->request->data['User']['password'];
			$this->Session->delete('Auth');
			if($this->Auth->login()){
				//echo "here";exit;
				$this->Session->delete('sessionUserInfo');
				$userInfo = $this->Auth->user();
				$this->Session->write("sessionUserInfo",$userInfo);	
				$this->User->id = $this->Auth->user('id');
				$result = $this->User->find('first', array(
			        'conditions' => array(
			            'User.id' => $this->Auth->user('id')
			        ),
			        'fields' => 'User.group_id' //CHANGE THIS
			    ));
    			$groupId = $result['User']['group_id']; //CHANGE THIS
				$this->Session->write('Auth.Group.id', $groupId);

				$this->loadModel('Userevent');
				if($this->request->params['controller'] != 'userevents'){

					//Logging
						$loguser = $this->Session->read('Auth.User');
						$this->Userevent->data['user_id'] = $loguser['id'];
						$this->Userevent->data['controller'] = $this->request->params['controller']; 
						$this->Userevent->data['action'] = $this->request->params['action']; 
						//pr($this->Userevent->data);exit;
						$this->Userevent->save($this->Userevent->data);
					//end logging
				}

				$this->redirect($this->Auth->redirectUrl());
			}else{
				$this->Session->setFlash('Incorrect email or password, try again.', 'flash_failure');
				return $this->redirect($this->Auth->redirect());
			}
			//pr($this->request->data);exit;
		}	
		
	}

 /* Function for super admin login*/
	 public function admin_loginnew(){
		$this->autoRender = false;
		if($this->request->is('post')){
			$this->request->data['User']['email']=$this->request->data['User']['email'];
			$this->request->data['User']['password']=$this->request->data['User']['password'];
			$this->Session->delete('Auth');
			if($this->Auth->login()){
				
				$this->Session->delete('sessionUserInfo');
				$userInfo = $this->Auth->user();
				$this->Session->write("sessionUserInfo",$userInfo);	
				$this->User->id = $this->Auth->user('id');
				$result = $this->User->find('first', array(
			        'conditions' => array(
			            'User.id' => $this->Auth->user('id')
			        ),
			        'fields' => 'User.group_id' //CHANGE THIS
			    ));
    			$groupId = $result['User']['group_id']; //CHANGE THIS
				$this->Session->write('Auth.Group.id', $groupId);
				echo 1; exit;
				
			}else{
				echo 0; exit;
			}
		}else{
			echo 0;exit;
		}	
	}




	public function admin_logout(){
		$groupId = $this->Session->read('Auth.Group.id');
		$this->loadModel('Userevent');
		if($this->request->params['controller'] != 'userevents'){

			//Logging
				$loguser = $this->Session->read('Auth.User');
				$this->Userevent->data['user_id'] = $loguser['id'];
				$this->Userevent->data['controller'] = $this->request->params['controller']; 
				$this->Userevent->data['action'] = $this->request->params['action']; 
				//pr($this->Userevent->data);exit;
				$this->Userevent->save($this->Userevent->data);
			//end logging
		}
		$this->Session->delete("sessionUserInfo");
		$this->Session->delete("Auth");
		$this->Session->destroy();
		$this->Session->setFlash('You have successfully logged out.', 'flash_success');
		//pr($this->Session->read());exit;

		if($groupId != 5){
			$this->redirect($this->Auth->logout());
		}else{
			return $this->redirect(array('controller'=>'users', 'action' => 'registration', 'admin'=> true));
		}
		
		
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		if (!empty($this->data)){
			if(isset($this->data['User']['limit'])){
            	$limit = $this->data['User']['limit'];
				$this->Session->write('default_limit', $limit);
			}
		}else{
			if($this->Session->check('default_limit'))
				$limit = $this->Session->read('default_limit');
			else
				$limit = $this->default_limit;
		}
		$search_conditions = array('User.type' => 0);
		$conditions = array();
		$this->set("search_string", "");
		if(isset($this->params->query['search'])){
			$this->set("search_string", $this->params->query['search']);
			$conditions = array('OR' => array(
			'User.first_name LIKE "%'.trim(addslashes($this->params->query['search'])).'%"')); 
		}
		$this->paginate  = array(
			'limit' => $limit, 
			'order' => 'User.id DESC',
			'conditions' => array(array_merge($conditions,$search_conditions))
		);
		$this->User->recursive = 0;
		$this->set('users', $this->Paginator->paginate());
		$this->set('limit', $limit);
	}


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_scholarshipregistration() {


		if (!empty($this->data)){
			if(isset($this->data['User']['limit'])){
            	$limit = $this->data['User']['limit'];
				$this->Session->write('default_limit', $limit);
			}
		}else{
			if($this->Session->check('default_limit'))
				$limit = $this->Session->read('default_limit');
			else
				$limit = $this->default_limit;
		}
		$search_conditions = array('User.type' => 1);
		$conditions = array();
		$this->set("search_string", "");
		if(isset($this->params->query['search'])){
			$this->set("search_string", $this->params->query['search']);
			$conditions = array('OR' => array(
			'User.code LIKE "%'.trim(addslashes($this->params->query['search'])).'%"')); 
		}
		$this->paginate  = array(
			'limit' => $limit, 
			'order' => 'User.id DESC',
			'conditions' => array(array_merge($conditions,$search_conditions))
		);
		$this->User->recursive = 0;
		$this->set('users', $this->Paginator->paginate());
		$this->set('limit', $limit);
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}

	/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_srview($id = null) {
		
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$user = $this->User->find('first', $options);
		

		$referralCode = $user['User']['code'];
		$options1 = array('conditions' => array('User.referral_code' => $referralCode));
		$refusers = $this->User->find('all', $options1);

		$this->set(compact('user', 'refusers'));
	}


/**
 * admin_registration method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_registration($id = null) {
		
		$this->layout='';
		if ($this->request->is('post')) {
			$this->User->create();
			$this->request->data['User']['type'] = 1;
			$this->request->data['User']['group_id'] = 5;
			$result = $this->User->query("SELECT Auto_increment FROM information_schema.tables AS NextId  WHERE table_name='he_users'");
    		//return $result[0]['NextId']['Auto_increment'];
			if(!empty($result)){
				$lastId = $result[0]['NextId']['Auto_increment'];
			}else{
				$lastId = "";
			}
			$this->request->data['User']['code'] = "HET".rand ( 10000 , 99999 ).$lastId;
			//pr($this->request->data);exit;
			if ($this->User->save($this->request->data)) {

				$this->request->data['User']['email']=$this->request->data['User']['email'];
				$this->request->data['User']['password']=$this->request->data['User']['password'];
				$this->Session->delete('Auth');
				if($this->Auth->login()){
					//echo "here";exit;
					$this->Session->delete('sessionUserInfo');
					$userInfo = $this->Auth->user();
					$this->Session->write("sessionUserInfo",$userInfo);	
					$this->User->id = $this->Auth->user('id');
					$result = $this->User->find('first', array(
				        'conditions' => array(
				            'User.id' => $this->Auth->user('id')
				        ),
				        'fields' => 'User.group_id' //CHANGE THIS
				    ));
	    			$groupId = $result['User']['group_id']; //CHANGE THIS
					$this->Session->write('Auth.Group.id', $groupId);

					//$this->Session->setFlash('Registration successful.! scholorship has been added to your wallet.','flash_success');
					return $this->redirect(array('controller'=>'dashboard', 'action' => 'index', 'admin'=> true));
				}
		}
	}

		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}


/**
 * admin_wallet method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_wallet($id = null) {
		
		$userAdmin = $this->Session->read('Auth.User'); 

		$user = $this->User->find('first',array('conditions'=>array('User.id'=>$userAdmin['id'])));
		//pr($user);

		$referralCode = $user['User']['code'];
		$options1 = array('conditions' => array('User.referral_code' => $referralCode));
		$refusers = $this->User->find('all', $options1);
		//pr($refusers);

		$this->request->data['User']['id'] = $user['User']['id'];
		$this->request->data['User']['first_landing'] = 0;
		$this->User->save($this->request->data);
		
		$this->loadModel('Course');
		$courses = $this->Course->find('list');
		$this->loadModel('College');
		$colleges = $this->College->find('list');

		$this->loadModel('Scholorship');
		$userid = $this->Session->read('Auth.User');
		$scholorshipCount = $this->Scholorship->find('count', array('conditions' => array('Scholorship.user_id' => $userid['id'])));

		$scholorship_status = $this->scholorship_status;

		$this->set(compact('courses', 'user', 'colleges','scholorshipCount', 'scholorship_status', 'refusers'));

	}



/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash('The user has been added successfully.','flash_success');
				return $this->redirect(array('action' => 'index', $this->User->id));
			} else {
				$this->Session->setFlash('The user could not be added. Please, try again.','flash_failure');
			}
		}
		$groups = $this->User->Group->find('list');
		$this->set(compact('groups'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->User->save($this->request->data)) {

				$this->Session->setFlash('The user has been updated successfully.','flash_success');
				return $this->redirect(array('action' => 'edit', $this->User->id));
			} else {
				$this->Session->setFlash('The user could not be updated. Please, try again.','flash_failure');
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
		$groups = $this->User->Group->find('list');
		$this->set(compact('groups'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->User->delete()) {

			$this->Session->setFlash('The user has been deleted successfully.','flash_success');
		} else {
			$this->Session->setFlash('The user could not be deleted. Please, try again.','flash_failure');
		}
		//return $this->redirect(array('action' => 'index'));
		$this->redirect( Router::url( $this->referer(), true ) );
	}

	/**
 * admin_export method
 *
 * @throws NotFoundException
 * @return csv
 */ 
 
	 public function admin_export() {
	    $results = $this->User->find('all');
	    $this->response->download('Crowdfunding-Export-'.'users-'.date('d-m-Y').'.csv');
	    $_serialize = 'results';
	    $_header = array('User ID', 'Status', 'Created');
	    $_extract = array('User.id', 'User.status', 'User.created');
		$this->viewClass = 'CsvView.Csv';
	    $this->set(compact('results', '_serialize', '_header', '_extract'));
	 }

	 /**
*  for admin profile
*/
	public function admin_profile() {
		//pr($this->Auth->user());
		$this->layout = 'admin_default';
		$userAdmin = $this->Session->read('Auth.User'); 
		$groupId = $this->Session->read('Auth.Group.id');

		$user = $this->User->find('first',array('conditions'=>array('User.id'=>$userAdmin['id'])));
		//pr($user);exit;
		$adminId = $user['User']['id'];
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->User->set($this->data['User']);
			if($this->User->validates()) {
				$this->User->id = $adminId;
				$this->User->save();
				$updatedInfo = $this->User->find('first',array('conditions' => array('User.id' => $adminId)));
				$this->Session->write('sessionAdminInfo',$updatedInfo['User']);				
				$this->Session->setFlash('Profile updated successfully.','flash_success');
				$this->redirect(array('controller' => 'Users', 'action' => 'profile'));
			}
		}
		$this->set(compact('user', 'groupId'));	
	}

	public function checkEmail($name) { //echo $name; exit;
		$this->autoRender= false;
		$arrEmail = $this->User->find('first',array('conditions' => array('User.email' => $name)));	
		//pr($arrEmail);
		if(!empty($arrEmail)){
			echo 1;
		}else{
			echo 0;
		}
	}

	/**
*  for change password
*/
	public function admin_changepassword() {
	$this->layout = 'admin_default';
	$adminDetails = $this->Session->read('sessionUserInfo');
	//pr($adminDetails);exit;
	$adminId = $adminDetails['id'];
	//$user = $this->User->find('first',array('conditions'=>array('User.group_id'=>1)));//pr($user);exit;
	//$adminId = $user['User']['id'];
	if($this->request->is('post') || $this->request->is('put')) {	
			$this->User->id = $adminId;
			$this->User->set($this->data['User']);
			//Password validation		
				if($this->User->validates()) {//pr($this->request->data);exit;
				if(!empty($this->data['User']['oldpassword']) && AuthComponent::password($this->data['User']['oldpassword']) != $this->User->field('password')){
					$this->Session->setFlash('The old password you gave is incorrect. Please try again.','flash_failure');
				}
				else if($this->data['User']['newpassword'] != $this->data['User']['confirmpwd']) {
					$this->Session->setFlash('The new password and confirm new password does not match. Please try again.','flash_failure');
				}
				else {
					$this->request->data['User']['password'] = $this->data['User']['newpassword'];
					if ($this->User->save($this->data)) 
						$this->Session->setFlash('The password has been updated successfully.', 'flash_success');
					else
						$this->Session->setFlash('The password could not be updated. Please try again.', 'flash_failure'); 
				}
				$this->redirect(array('controller' => 'users', 'action' => 'profile'));
				}
				else {
				$this->render('admin_profile');
				
				}
		
		}
		
	}
}
