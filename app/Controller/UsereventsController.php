<?php
App::uses('AppController', 'Controller');
/**
 * Userevents Controller
 *
 * @property Userevent $Userevent
 * @property PaginatorComponent $Paginator
 */
class UsereventsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

public function beforeFilter() {
		parent::beforeFilter();
		$this->layout='admin_default';
	}
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		if (!empty($this->data)){
			if(isset($this->data['Userevent']['limit'])){
            	$limit = $this->data['Userevent']['limit'];
				$this->Session->write('default_limit', $limit);
			}
		}else{
			if($this->Session->check('default_limit'))
				$limit = 50;
			else
				$limit = $this->default_limit;
		}
		$search_conditions = array();
		$conditions = array();
		
		if(isset($this->params->query['created']) && $this->params->query['created']!= null){
			$this->set("created", $this->params->query['created']);
			$date = date("Y-m-d", strtotime($this->params->query['created']));
			array_push($search_conditions, array('OR' => array(
			'Userevent.created LIKE "%'.$date.'%"'))); 
		}

		if(isset($this->params->query['updated_user']) && $this->params->query['updated_user']!= null){
			//pr($this->params->query);exit;
			$this->set("updated_user", $this->params->query['updated_user']);
			array_push($search_conditions, array('OR' => array(
			'Userevent.user_id = "'.$this->params->query['updated_user'].'"'))); 
		}else{
			$this->set("updated_user", "");
		}


		$this->paginate  = array(
				'limit' => $limit, 
				'order' => 'Userevent.id DESC',
				'conditions' => array(array_merge($conditions,$search_conditions))
			);
		$this->Userevent->recursive = 0;
		$this->set('userevents', $this->Paginator->paginate());
		
		$this->loadModel('User');
		//pr($this->User->find('list'));
		$options = array('conditions' => array('User.type'=> 0), 'fields' => 'first_name');
		$users = $this->User->find('list', $options); 

		$this->set('users', $users);

		$this->set('limit', $limit);
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Userevent->exists($id)) {
			throw new NotFoundException(__('Invalid userevent'));
		}
		$options = array('conditions' => array('Userevent.' . $this->Userevent->primaryKey => $id));
		$this->set('userevent', $this->Userevent->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Userevent->create();
			if ($this->Userevent->save($this->request->data)) {
				$this->Session->setFlash('The userevent has been added successfully.','flash_success');
				return $this->redirect(array('action' => 'edit', $this->Userevent->id));
			} else {
				$this->Session->setFlash('The userevent could not be added. Please, try again.','flash_failure');
			}
		}
		$users = $this->Userevent->User->find('list');
		$this->set(compact('users'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Userevent->exists($id)) {
			throw new NotFoundException(__('Invalid userevent'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Userevent->save($this->request->data)) {
				$this->Session->setFlash('The userevent has been updated successfully.','flash_success');
				return $this->redirect(array('action' => 'edit', $this->Userevent->id));
			} else {
				$this->Session->setFlash('The userevent could not be updated. Please, try again.','flash_failure');
			}
		} else {
			$options = array('conditions' => array('Userevent.' . $this->Userevent->primaryKey => $id));
			$this->request->data = $this->Userevent->find('first', $options);
		}
		$users = $this->Userevent->User->find('list');
		$this->set(compact('users'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Userevent->id = $id;
		if (!$this->Userevent->exists()) {
			throw new NotFoundException(__('Invalid userevent'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Userevent->delete()) {
			$this->Session->setFlash('The userevent has been deleted successfully.','flash_success');
		} else {
			$this->Session->setFlash('The userevent could not be deleted. Please, try again.','flash_failure');
		}
		return $this->redirect(array('action' => 'index'));
	}

	/**
 * admin_export method
 *
 * @throws NotFoundException
 * @return csv
 */ 
 
	 public function admin_export() {
	    $results = $this->Userevent->find('all');
	    $this->response->download('Crowdfunding-Export-'.'userevents-'.date('d-m-Y').'.csv');
	    $_serialize = 'results';
	    $_header = array('Userevent ID', 'Status', 'Created');
	    $_extract = array('Userevent.id', 'Userevent.status', 'Userevent.created');
		$this->viewClass = 'CsvView.Csv';
	    $this->set(compact('results', '_serialize', '_header', '_extract'));
	 }
}
