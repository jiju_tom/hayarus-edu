<?php
App::uses('AppController', 'Controller');
/**
 * Invoices Controller
 *
 * @property Invoice $Invoice
 * @property PaginatorComponent $Paginator
 */
class InvoicesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public function beforeFilter() {
		parent::beforeFilter();
		$this->layout='admin_default';
	}

	public function beforeRender(){

		$this->loadModel('Userevent');

		if($this->request->params['controller'] != 'userevents'){

			//Logging
				$loguser = $this->Session->read('Auth.User');
				$this->Userevent->data['user_id'] = $loguser['id'];
				$this->Userevent->data['controller'] = $this->request->params['controller']; 
				$this->Userevent->data['action'] = $this->request->params['action']; 
				//pr($this->Userevent->data);exit;
				$this->Userevent->save($this->Userevent->data);
			//end logging
		}
	}
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		if (!empty($this->data)){
			if(isset($this->data['Invoice']['limit'])){
            	$limit = $this->data['Invoice']['limit'];
				$this->Session->write('default_limit', $limit);
			}
		}else{
			if($this->Session->check('default_limit'))
				$limit = $this->Session->read('default_limit');
			else
				$limit = $this->default_limit;
		}
		$search_conditions = array();
		$conditions = array('Invoice.type' => 0);
		$this->set("search_string", "");
		if(isset($this->params->query['search'])){
			$this->set("search_string", $this->params->query['search']);
			$conditions = array('OR' => array(
			'User.first_name LIKE "%'.trim(addslashes($this->params->query['search'])).'%"')); 
		}
		$this->paginate  = array(
				'limit' => $limit, 
				'order' => 'Invoice.id DESC',
				'conditions' => array(array_merge($conditions,$search_conditions))
			);
		$this->Invoice->recursive = 0;
		$this->set('invoices', $this->Paginator->paginate());
		$this->set('limit', $limit);
	}



	public function admin_serviceindex() {
		if (!empty($this->data)){
			if(isset($this->data['Invoice']['limit'])){
            	$limit = $this->data['Invoice']['limit'];
				$this->Session->write('default_limit', $limit);
			}
		}else{
			if($this->Session->check('default_limit'))
				$limit = $this->Session->read('default_limit');
			else
				$limit = $this->default_limit;
		}
		$search_conditions = array();
		$conditions = array('Invoice.type' => 1);
		$this->set("search_string", "");
		if(isset($this->params->query['search'])){
			$this->set("search_string", $this->params->query['search']);
			$conditions = array('OR' => array(
			'User.first_name LIKE "%'.trim(addslashes($this->params->query['search'])).'%"')); 
		}
		$this->paginate  = array(
				'limit' => $limit, 
				'order' => 'Invoice.id DESC',
				'conditions' => array(array_merge($conditions,$search_conditions))
			);
		$this->Invoice->recursive = 0;
		$this->set('invoices', $this->Paginator->paginate());
		$this->set('limit', $limit);
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Invoice->exists($id)) {
			throw new NotFoundException(__('Invalid invoice'));
		}


		$options = array('conditions' => array('Invoice.id' => $id));
		$Invoice = $this->Invoice->find('first', $options);

		$this->loadModel('Invoiceitem');
		$options = array('conditions' => array('Invoiceitem.invoice_id' => $id));
		$Invoiceitem = $this->Invoiceitem->find('all', $options);

		$this->set(compact('Invoice', 'Invoiceitem'));

	}


	public function admin_serviceview($id = null) {
		if (!$this->Invoice->exists($id)) {
			throw new NotFoundException(__('Invalid invoice'));
		}


		$options = array('conditions' => array('Invoice.id' => $id));
		$Invoice = $this->Invoice->find('first', $options);

		$this->loadModel('Invoiceitem');
		$options = array('conditions' => array('Invoiceitem.invoice_id' => $id));
		$Invoiceitem = $this->Invoiceitem->find('all', $options);

		$this->set(compact('Invoice', 'Invoiceitem'));

	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		$this->autoRender = false;
		$user = $this->Session->read('Auth.User');
		if ($this->request->is('post')) {
			//pr($this->request->data);exit;
			$this->loadModel('Invoice');
			$this->loadModel('Invoiceitem');
			$this->request->data['Invoice']['id'] = $this->request->data['Invoice']['id'];
			$this->request->data['Invoice']['user_id'] = $user['id'];
	
			if ($this->Invoice->save($this->request->data)) {
				
				$options = array('conditions' => array('Invoiceitem.invoice_id' => $this->request->data['Invoice']['id']));
				$existingInvoiceItems = $this->Invoiceitem->find('all', $options);
				if(!empty($existingInvoiceItems)){
					$this->Invoiceitem->deleteAll(array('Invoiceitem.invoice_id' => $this->request->data['Invoice']['id']));
					foreach ($this->request->data['Invoice']['particular'] as $key => $value) {

						$this->Invoiceitem->create();
						$this->request->data['Invoiceitem']['invoice_id'] = $this->request->data['Invoice']['id'];
						$this->request->data['Invoiceitem']['particular'] = $this->request->data['Invoice']['particular'][$key];
						$this->request->data['Invoiceitem']['amount'] = $this->request->data['Invoice']['amount'][$key];
						$this->Invoiceitem->save($this->request->data);	
					}
					echo 1;exit;
				}else{
					foreach ($this->request->data['Invoice']['particular'] as $key => $value) {

						$this->Invoiceitem->create();
						$this->request->data['Invoiceitem']['invoice_id'] = $this->request->data['Invoice']['id'];
						$this->request->data['Invoiceitem']['particular'] = $this->request->data['Invoice']['particular'][$key];
						$this->request->data['Invoiceitem']['amount'] = $this->request->data['Invoice']['amount'][$key];
						$this->Invoiceitem->save($this->request->data);	
					}
					echo 1;exit;	
				}

			} else {
				// $this->Session->setFlash('The invoice could not be added. Please, try again.','flash_failure');
				echo 0;exit;
			}
		}

		$colleges = $this->Invoice->College->find('list');
		$courses = $this->Invoice->Course->find('list');
		$locations = $this->Invoice->Location->find('list');
		$students = $this->Invoice->Student->find('list');
		$users = $this->Invoice->User->find('list');
		$this->set(compact('colleges', 'courses', 'locations', 'students', 'users'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Invoice->exists($id)) {
			throw new NotFoundException(__('Invalid invoice'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Invoice->save($this->request->data)) {
				$this->Session->setFlash('The invoice has been updated successfully.','flash_success');
				return $this->redirect(array('action' => 'edit', $this->Invoice->id));
			} else {
				$this->Session->setFlash('The invoice could not be updated. Please, try again.','flash_failure');
			}
		} else {
			$options = array('conditions' => array('Invoice.' . $this->Invoice->primaryKey => $id));
			$this->request->data = $this->Invoice->find('first', $options);
		}
		$colleges = $this->Invoice->College->find('list');
		$courses = $this->Invoice->Course->find('list');
		$locations = $this->Invoice->Location->find('list');
		$students = $this->Invoice->Student->find('list');
		$users = $this->Invoice->User->find('list');
		$this->set(compact('colleges', 'courses', 'locations', 'students', 'users'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Invoice->id = $id;
		if (!$this->Invoice->exists()) {
			throw new NotFoundException(__('Invalid invoice'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Invoice->delete()) {
			$this->Session->setFlash('The invoice has been deleted successfully.','flash_success');
		} else {
			$this->Session->setFlash('The invoice could not be deleted. Please, try again.','flash_failure');
		}
		return $this->redirect(array('action' => 'index'));
	}

	/**
 * admin_export method
 *
 * @throws NotFoundException
 * @return csv
 */ 
 
	 public function admin_export() {
	    $results = $this->Invoice->find('all');
	    $this->response->download('Crowdfunding-Export-'.'invoices-'.date('d-m-Y').'.csv');
	    $_serialize = 'results';
	    $_header = array('Invoice ID', 'Status', 'Created');
	    $_extract = array('Invoice.id', 'Invoice.status', 'Invoice.created');
		$this->viewClass = 'CsvView.Csv';
	    $this->set(compact('results', '_serialize', '_header', '_extract'));
	 }
}
