<?php
App::uses('AppController', 'Controller');
/**
 * Servicestatuses Controller
 *
 * @property Servicestatus $Servicestatus
 * @property PaginatorComponent $Paginator
 */
class ServicestatusesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

public function beforeFilter() {
		parent::beforeFilter();
		$this->layout='admin_default';
	}
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		if (!empty($this->data)){
			if(isset($this->data['Servicestatus']['limit'])){
            	$limit = $this->data['Servicestatus']['limit'];
				$this->Session->write('default_limit', $limit);
			}
		}else{
			if($this->Session->check('default_limit'))
				$limit = $this->Session->read('default_limit');
			else
				$limit = $this->default_limit;
		}
		$search_conditions = array();
		$conditions = array();
		$this->set("search_string", "");
		if(isset($this->params->query['search'])){
			$this->set("search_string", $this->params->query['search']);
			$conditions = array('OR' => array(
			'Servicestatus.name LIKE "%'.trim(addslashes($this->params->query['search'])).'%"')); 
		}
		$this->paginate  = array(
				'limit' => $limit, 
				'order' => 'Servicestatus.id DESC',
				'conditions' => array(array_merge($conditions,$search_conditions))
			);
		$this->Servicestatus->recursive = 0;
		$this->set('servicestatuses', $this->Paginator->paginate());
		$this->set('limit', $limit);
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Servicestatus->exists($id)) {
			throw new NotFoundException(__('Invalid servicestatus'));
		}
		$options = array('conditions' => array('Servicestatus.' . $this->Servicestatus->primaryKey => $id));
		$this->set('servicestatus', $this->Servicestatus->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Servicestatus->create();
			if ($this->Servicestatus->save($this->request->data)) {
				$this->Session->setFlash('The servicestatus has been added successfully.','flash_success');
				return $this->redirect(array('action' => 'edit', $this->Servicestatus->id));
			} else {
				$this->Session->setFlash('The servicestatus could not be added. Please, try again.','flash_failure');
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Servicestatus->exists($id)) {
			throw new NotFoundException(__('Invalid servicestatus'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Servicestatus->save($this->request->data)) {
				$this->Session->setFlash('The servicestatus has been updated successfully.','flash_success');
				return $this->redirect(array('action' => 'edit', $this->Servicestatus->id));
			} else {
				$this->Session->setFlash('The servicestatus could not be updated. Please, try again.','flash_failure');
			}
		} else {
			$options = array('conditions' => array('Servicestatus.' . $this->Servicestatus->primaryKey => $id));
			$this->request->data = $this->Servicestatus->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Servicestatus->id = $id;
		if (!$this->Servicestatus->exists()) {
			throw new NotFoundException(__('Invalid servicestatus'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Servicestatus->delete()) {
			$this->Session->setFlash('The servicestatus has been deleted successfully.','flash_success');
		} else {
			$this->Session->setFlash('The servicestatus could not be deleted. Please, try again.','flash_failure');
		}
		return $this->redirect(array('action' => 'index'));
	}

	/**
 * admin_export method
 *
 * @throws NotFoundException
 * @return csv
 */ 
 
	 public function admin_export() {
	    $results = $this->Servicestatus->find('all');
	    $this->response->download('Crowdfunding-Export-'.'servicestatuss-'.date('d-m-Y').'.csv');
	    $_serialize = 'results';
	    $_header = array('Servicestatus ID', 'Status', 'Created');
	    $_extract = array('Servicestatus.id', 'Servicestatus.status', 'Servicestatus.created');
		$this->viewClass = 'CsvView.Csv';
	    $this->set(compact('results', '_serialize', '_header', '_extract'));
	 }
}
