<?php
App::uses('AppController', 'Controller');
/**
 * Scholorships Controller
 *
 * @property Scholorship $Scholorship
 * @property PaginatorComponent $Paginator
 */
class ScholorshipsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

public function beforeFilter() {
		parent::beforeFilter();
		$this->layout='admin_default';
	}

public function beforeRender(){

	$this->loadModel('Userevent');

	if($this->request->params['controller'] != 'userevents'){

		//Logging
			$loguser = $this->Session->read('Auth.User');
			$this->Userevent->data['user_id'] = $loguser['id'];
			$this->Userevent->data['controller'] = $this->request->params['controller']; 
			$this->Userevent->data['action'] = $this->request->params['action']; 
			//pr($this->Userevent->data);exit;
			$this->Userevent->save($this->Userevent->data);
		//end logging
	}
}	
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		if (!empty($this->data)){
			if(isset($this->data['Scholorship']['limit'])){
            	$limit = $this->data['Scholorship']['limit'];
				$this->Session->write('default_limit', $limit);
			}
		}else{
			if($this->Session->check('default_limit'))
				$limit = $this->Session->read('default_limit');
			else
				$limit = $this->default_limit;
		}
		$search_conditions = array();
		$conditions = array();
		$this->set("search_string", "");
		

		if(isset($this->params->query['name']) && $this->params->query['name']!= null){
			$this->set("name", $this->params->query['name']);
			array_push($search_conditions, array('OR' => array(
			'User.first_name LIKE "%'.trim(addslashes($this->params->query['name'])).'%"'))); 
		}

		if(isset($this->params->query['code']) && $this->params->query['code']!= null){
			$this->set("code", $this->params->query['code']);
			array_push($search_conditions, array('OR' => array(
			'User.code LIKE "%'.trim($this->params->query['code']).'%"'))); 
		}

		$this->paginate  = array(
				'limit' => $limit, 
				'order' => 'User.id DESC',
				'group' => 'user_id',
				'conditions' => array(array_merge($conditions,$search_conditions))
			);
		//pr($this->Paginator);
		$this->Scholorship->recursive = 0;
		$this->set('scholorships', $this->Paginator->paginate());
		$this->loadModel('User');
		//$options = array('conditions' => array('User.type' => 1, 'fields' => ));
		$users = $this->User->find('list', array( 'fields' => 'first_name' ));

		$scholorship_status = $this->scholorship_status;

		$this->set(compact('limit', 'users', 'scholorship_status'));

	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		
		$options = array('conditions' => array('Scholorship.user_id' => $id));
		$scholorships = $this->Scholorship->find('all', $options);
		//pr($scholorships);

		$this->loadModel('Course');
		$courses = $this->Course->find('list');
		$this->loadModel('College');
		$colleges = $this->College->find('list');
		//pr($colleges);
		$scholorship_status = $this->scholorship_status;

		$this->set(compact('scholorships','scholorship_status', 'courses','colleges'));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		$this->autoRender = false;
		if ($this->request->is('post')) {
			$this->Scholorship->create();
			if ($this->Scholorship->save($this->request->data)) {
				//pr($this->request->data);exit;
				$user = $this->Session->read('Auth.User');
		
				$count = $this->Scholorship->find('count', array('conditions' => array('Scholorship.user_id' => $user['id'])));
				if($count == 1){
					echo 2;exit;
				}else{
					echo 1;exit;
				}
				
			} else {	
				echo 0;exit;
			}
		}
		$users = $this->Scholorship->User->find('list');
		$colleges = $this->Scholorship->College->find('list');
		$courses = $this->Scholorship->Course->find('list');
		$this->set(compact('users', 'colleges', 'courses'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		
		//pr($this->request->data);exit;
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Scholorship->save($this->request->data)) {
				$this->Session->setFlash('data updated successfully.','flash_success');
				return $this->redirect(array('action' => 'view', $this->request->data['Scholorship']['user_id']));
			} else {
				$this->Session->setFlash('The scholorship could not be updated. Please, try again.','flash_failure');
			}
		} else {
			$options = array('conditions' => array('Scholorship.' . $this->Scholorship->primaryKey => $id));
			$this->request->data = $this->Scholorship->find('first', $options);
		}
		$users = $this->Scholorship->User->find('list');
		$colleges = $this->Scholorship->College->find('list');
		$courses = $this->Scholorship->Course->find('list');
		$this->set(compact('users', 'colleges', 'courses'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Scholorship->id = $id;
		if (!$this->Scholorship->exists()) {
			throw new NotFoundException(__('Invalid scholorship'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Scholorship->delete()) {
			$this->Session->setFlash('The scholorship has been deleted successfully.','flash_success');
		} else {
			$this->Session->setFlash('The scholorship could not be deleted. Please, try again.','flash_failure');
		}
		return $this->redirect(array('action' => 'index'));
	}

	/**
 * admin_export method
 *
 * @throws NotFoundException
 * @return csv
 */ 
 
	 public function admin_export() {
	    $results = $this->Scholorship->find('all');
	    $this->response->download('Crowdfunding-Export-'.'scholorships-'.date('d-m-Y').'.csv');
	    $_serialize = 'results';
	    $_header = array('Scholorship ID', 'Status', 'Created');
	    $_extract = array('Scholorship.id', 'Scholorship.status', 'Scholorship.created');
		$this->viewClass = 'CsvView.Csv';
	    $this->set(compact('results', '_serialize', '_header', '_extract'));
	 }
}
