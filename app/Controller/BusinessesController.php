<?php
App::uses('AppController', 'Controller');
/**
 * Businesses Controller
 *
 * @property Business $Business
 * @property PaginatorComponent $Paginator
 */
class BusinessesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

public function beforeFilter() {
		parent::beforeFilter();
		$this->layout='admin_default';
	}
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {

		$groupId = $this->Session->read('Auth.Group.id');
		if (!empty($this->data)){
			if(isset($this->data['Business']['limit'])){
            	$limit = $this->data['Business']['limit'];
				$this->Session->write('default_limit', $limit);
			}
		}else{
			if($this->Session->check('default_limit'))
				$limit = $this->Session->read('default_limit');
			else
				$limit = $this->default_limit;
		}
		$search_conditions = array();
		$conditions = array();
		$this->set("search_string", "");


		if(isset($this->params->query['name']) && $this->params->query['name']!= null){
			$this->set("name", $this->params->query['name']);
			array_push($search_conditions, array('OR' => array(
			'Business.name LIKE "%'.trim(addslashes($this->params->query['name'])).'%"'))); 
		}

		if(isset($this->params->query['phone']) && $this->params->query['phone']!= null){
			$this->set("phone", $this->params->query['phone']);
			array_push($search_conditions, array('OR' => array(
			'Business.phone LIKE "%'.trim($this->params->query['phone']).'%"'))); 
		}

		if(isset($this->params->query['servicestatuses']) && $this->params->query['servicestatuses']!= null){
			$this->set("servicestatuses", $this->params->query['servicestatuses']);
			array_push($search_conditions, array('OR' => array(
			'Business.servicestatus_id = "'.$this->params->query['servicestatuses'].'"'))); 
		}

		if(isset($this->params->query['created']) && $this->params->query['created']!= null){
			$this->set("created", $this->params->query['created']);
			$date = date("Y-m-d", strtotime($this->params->query['created']));
			array_push($search_conditions, array('OR' => array(
			'Business.created LIKE "%'.$date.'%"'))); 
		}

		if(isset($this->params->query['modified']) && $this->params->query['modified']!= null){
			$this->set("modified", $this->params->query['modified']);
			$date = date("Y-m-d", strtotime($this->params->query['modified']));
			array_push($search_conditions, array('OR' => array(
			'Business.modified LIKE "%'.$date.'%"'))); 
		}


		$this->paginate  = array(
				'limit' => $limit, 
				'order' => 'Business.id DESC',
				'conditions' => array(array_merge($conditions,$search_conditions))
			);
		$this->Business->recursive = 0;
		$this->set('businesses', $this->Paginator->paginate());
		$this->loadModel('Servicestatus');
		$servicestatuses = $this->Servicestatus->find('list');

		$this->LoadModel('User');
		$options = array('conditions' => array('User.type'=> 0), 'fields' => 'first_name');
		$users = $this->User->find('list', $options); 
		

		$this->set(compact('servicestatuses', 'users', 'groupId'));
		$this->set('limit', $limit);
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Business->exists($id)) {
			throw new NotFoundException(__('Invalid business'));
		}
		$options = array('conditions' => array('Business.' . $this->Business->primaryKey => $id));
		$this->set('business', $this->Business->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Business->create();
			if ($this->Business->save($this->request->data)) {
				$this->Session->setFlash('The business has been added successfully.','flash_success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The business could not be added. Please, try again.','flash_failure');
			}
		}
		$this->loadModel('Servicestatus');
		$servicestatuses = $this->Servicestatus->find('list');
		$invoices = $this->Business->Invoice->find('list');
		$this->set(compact('invoices', 'servicestatuses'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$userId = $this->Auth->user('id');
		if (!$this->Business->exists($id)) {
			throw new NotFoundException(__('Invalid business'));
		}
		if ($this->request->is(array('post', 'put'))) { //pr($this->request->data);exit;

			if($this->request->data['Business']['servicestatus_id'] == 4 && $this->request->data['Business']['invoice_id'] == 0){

					$this->loadModel('Invoice');
					$this->request->data['Invoice']['invoice_no'] = rand ( 100000 , 999999 );
					$this->request->data['Invoice']['user_id'] = $userId;
					$this->request->data['Invoice']['issued_to'] = $this->request->data['Business']['name'];
					$this->request->data['Invoice']['type'] = 1;
					
					//pr($this->request->data['Invoice']);exit;
					$options = array('conditions' => array('Invoice.invoice_no' => $this->request->data['Invoice']['invoice_no']));
					$existingInvoice  = $this->Invoice->find('first', $options);
					if(empty($existingInvoice)){
						$this->Invoice->save($this->request->data['Invoice']);

						$this->request->data['Business']['invoice_id'] =  $this->Invoice->id;
					}	
			}

			if ($this->Business->save($this->request->data)) {
				$this->Session->setFlash('The business has been updated successfully.','flash_success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The business could not be updated. Please, try again.','flash_failure');
			}
		} else {
			$options = array('conditions' => array('Business.' . $this->Business->primaryKey => $id));
			$this->request->data = $this->Business->find('first', $options);
		}
		$this->loadModel('Servicestatus');
		$servicestatuses = $this->Servicestatus->find('list');
		$invoices = $this->Business->Invoice->find('list');
		$this->set(compact('servicestatuses', 'invoices'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Business->id = $id;
		if (!$this->Business->exists()) {
			throw new NotFoundException(__('Invalid business'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Business->delete()) {
			$this->Session->setFlash('The business has been deleted successfully.','flash_success');
		} else {
			$this->Session->setFlash('The business could not be deleted. Please, try again.','flash_failure');
		}
		return $this->redirect(array('action' => 'index'));
	}

	/**
 * admin_export method
 *
 * @throws NotFoundException
 * @return csv
 */ 
 
	 public function admin_export() {
	    $results = $this->Business->find('all');
	    $this->response->download('Crowdfunding-Export-'.'businesss-'.date('d-m-Y').'.csv');
	    $_serialize = 'results';
	    $_header = array('Business ID', 'Status', 'Created');
	    $_extract = array('Business.id', 'Business.status', 'Business.created');
		$this->viewClass = 'CsvView.Csv';
	    $this->set(compact('results', '_serialize', '_header', '_extract'));
	 }
}
