<?php
App::uses('AppController', 'Controller');
/**
 * Courseexpenses Controller
 *
 * @property Courseexpense $Courseexpense
 * @property PaginatorComponent $Paginator
 */
class CourseexpensesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

public function beforeFilter() {
		parent::beforeFilter();
		$this->layout='admin_default';
	}
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		if (!empty($this->data)){
			if(isset($this->data['Courseexpense']['limit'])){
            	$limit = $this->data['Courseexpense']['limit'];
				$this->Session->write('default_limit', $limit);
			}
		}else{
			if($this->Session->check('default_limit'))
				$limit = $this->Session->read('default_limit');
			else
				$limit = $this->default_limit;
		}
		$search_conditions = array();
		$conditions = array();
		$this->set("search_string", "");
		if(isset($this->params->query['search'])){
			$this->set("search_string", $this->params->query['search']);
			$conditions = array('OR' => array(
			'Courseexpense.name LIKE "%'.trim(addslashes($this->params->query['search'])).'%"')); 
		}
		$this->paginate  = array(
				'limit' => $limit, 
				'order' => 'Courseexpense.id DESC',
				'conditions' => array(array_merge($conditions,$search_conditions))
			);
		$this->Courseexpense->recursive = 0;
		$this->set('courseexpenses', $this->Paginator->paginate());
		$this->set('limit', $limit);
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Courseexpense->exists($id)) {
			throw new NotFoundException(__('Invalid courseexpense'));
		}
		$options = array('conditions' => array('Courseexpense.' . $this->Courseexpense->primaryKey => $id));
		$this->set('courseexpense', $this->Courseexpense->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Courseexpense->create();
			if ($this->Courseexpense->save($this->request->data)) {
				$this->Session->setFlash('The courseexpense has been added successfully.','flash_success');
				return $this->redirect(array('action' => 'edit', $this->Courseexpense->id));
			} else {
				$this->Session->setFlash('The courseexpense could not be added. Please, try again.','flash_failure');
			}
		}
		$courses = $this->Courseexpense->Course->find('list');
		$this->set(compact('courses'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Courseexpense->exists($id)) {
			throw new NotFoundException(__('Invalid courseexpense'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Courseexpense->save($this->request->data)) {
				$this->Session->setFlash('The courseexpense has been updated successfully.','flash_success');
				return $this->redirect(array('action' => 'edit', $this->Courseexpense->id));
			} else {
				$this->Session->setFlash('The courseexpense could not be updated. Please, try again.','flash_failure');
			}
		} else {
			$options = array('conditions' => array('Courseexpense.' . $this->Courseexpense->primaryKey => $id));
			$this->request->data = $this->Courseexpense->find('first', $options);
		}
		$courses = $this->Courseexpense->Course->find('list');
		$this->set(compact('courses'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Courseexpense->id = $id;
		if (!$this->Courseexpense->exists()) {
			throw new NotFoundException(__('Invalid courseexpense'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Courseexpense->delete()) {
			$this->Session->setFlash('The courseexpense has been deleted successfully.','flash_success');
		} else {
			$this->Session->setFlash('The courseexpense could not be deleted. Please, try again.','flash_failure');
		}
		return $this->redirect(array('action' => 'index'));
	}

	/**
 * admin_export method
 *
 * @throws NotFoundException
 * @return csv
 */ 
 
	 public function admin_export() {
	    $results = $this->Courseexpense->find('all');
	    $this->response->download('Crowdfunding-Export-'.'courseexpenses-'.date('d-m-Y').'.csv');
	    $_serialize = 'results';
	    $_header = array('Courseexpense ID', 'Status', 'Created');
	    $_extract = array('Courseexpense.id', 'Courseexpense.status', 'Courseexpense.created');
		$this->viewClass = 'CsvView.Csv';
	    $this->set(compact('results', '_serialize', '_header', '_extract'));
	 }
}
