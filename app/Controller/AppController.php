<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		https://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

	public $helpers = array('Html', 'Form', 'Session');
	
	 public $components = array(
	    'Auth' => array(
	        'loginAction' => array(
	            'controller' => 'users',
	            'action' => 'login'
	        ),
	        'authError' => 'Did you really think you are allowed to see that?',
	        'authenticate' => array(
	            'Form' => array(
	                'fields' => array(
	                  'username' => 'email', //Default is 'username' in the userModel
	                  'password' => 'password'  //Default is 'password' in the userModel
	                )
	            )
	        )
	    ),'Session','RequestHandler',
	);


	public $default_limit = 10;
	public $gallery_limit = 8;
	public $ImagePath = "assets/plugins/dropzone/uploads/";

	public $default_limit_dropdown = array("5" => 5, "10" => 10, "20" => 20, "50" => 50, "1000000" => "All");
	public $toggle_status = array("0" => '<a class="btn btn-xs yellow ts-toggle sts"> <i class="fa fa-clock-o"></i> Pending</a>', "1" => '<a class="btn btn-xs green ts-toggle sts"><i class="fa fa-check-square-o"></i> Approved </a>', "2" => '<a class="btn btn-xs red ts-toggle sts"><i class="fa fa-minus-square"></i> Hold </a>');

	public $college_type_dropdown = array("0"=> "Girls", "1"=> "Boys", "2"=> "Both");
	public $scholorship_status = array("0"=> "Applied", "1"=> "Confirmed", "2"=> "Rejected", "3" => "Admission Closed");
	public $rating = array("0"=> "4 Star", "1"=> "Boys", "1"=> "5 Star");

	 public function beforeFilter(){



	 	//$this->Auth->allow('*');
		$this->Auth->loginAction = array('controller' => 'users', 'action' => 'login','admin'=>'true');
		$this->Auth->logoutRedirect = array('controller' => 'users', 'action' => 'login','admin'=>'true');
		$this->Auth->loginRedirect = array('controller' => 'dashboard', 'action' => 'index','admin'=>'true');

		$this->loadModel('Userevent');


		$this->set('default_limit_dropdown', $this->default_limit_dropdown);
		$this->set('toggle_status', $this->toggle_status);
		$this->set('college_type_dropdown',$this->college_type_dropdown);
		$this->set('ImagePath',$this->ImagePath);
		$this->set('scholorship_status',$this->scholorship_status);
		$this->set('rating',$this->rating);
	}

	
}
