<?php
App::uses('AppModel', 'Model');
/**
 * College Model
 *
 * @property Collegecourse $Collegecourse
 * @property Collegeimage $Collegeimage
 * @property Studentreference $Studentreference
 */
class College extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';
	public $recursive = 2;

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Collegecourse' => array(
			'className' => 'Collegecourse',
			'foreignKey' => 'college_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Courseexpense' => array(
			'className' => 'Courseexpense',
			'foreignKey' => 'college_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Collegeimage' => array(
			'className' => 'Collegeimage',
			'foreignKey' => 'college_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Studentreference' => array(
			'className' => 'Studentreference',
			'foreignKey' => 'college_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
