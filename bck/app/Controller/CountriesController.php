<?php
App::uses('AppController', 'Controller');
/**
 * Countries Controller
 *
 * @property Country $Country
 * @property PaginatorComponent $Paginator
 */
class CountriesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

public function beforeFilter() {
		parent::beforeFilter();
		$this->layout='admin_default';
	}
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		if (!empty($this->data)){
			if(isset($this->data['Country']['limit'])){
            	$limit = $this->data['Country']['limit'];
				$this->Session->write('default_limit', $limit);
			}
		}else{
			if($this->Session->check('default_limit'))
				$limit = $this->Session->read('default_limit');
			else
				$limit = $this->default_limit;
		}
		$search_conditions = array();
		$conditions = array();
		$this->set("search_string", "");
		if(isset($this->params->query['search'])){
			$this->set("search_string", $this->params->query['search']);
			$conditions = array('OR' => array(
			'Country.name LIKE "%'.trim(addslashes($this->params->query['search'])).'%"')); 
		}
		$this->paginate  = array(
				'limit' => $limit, 
				'order' => 'Country.id DESC',
				'conditions' => array(array_merge($conditions,$search_conditions))
			);
		$this->Country->recursive = 0;
		$this->set('countries', $this->Paginator->paginate());
		$this->set('limit', $limit);
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Country->exists($id)) {
			throw new NotFoundException(__('Invalid country'));
		}
		$options = array('conditions' => array('Country.' . $this->Country->primaryKey => $id));
		$this->set('country', $this->Country->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Country->create();
			if ($this->Country->save($this->request->data)) {
				$this->Session->setFlash('The country has been added successfully.','flash_success');
				return $this->redirect(array('action' => 'edit', $this->Country->id));
			} else {
				$this->Session->setFlash('The country could not be added. Please, try again.','flash_failure');
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Country->exists($id)) {
			throw new NotFoundException(__('Invalid country'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Country->save($this->request->data)) {
				$this->Session->setFlash('The country has been updated successfully.','flash_success');
				return $this->redirect(array('action' => 'edit', $this->Country->id));
			} else {
				$this->Session->setFlash('The country could not be updated. Please, try again.','flash_failure');
			}
		} else {
			$options = array('conditions' => array('Country.' . $this->Country->primaryKey => $id));
			$this->request->data = $this->Country->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Country->id = $id;
		if (!$this->Country->exists()) {
			throw new NotFoundException(__('Invalid country'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Country->delete()) {
			$this->Session->setFlash('The country has been deleted successfully.','flash_success');
		} else {
			$this->Session->setFlash('The country could not be deleted. Please, try again.','flash_failure');
		}
		return $this->redirect(array('action' => 'index'));
	}

	/**
 * admin_export method
 *
 * @throws NotFoundException
 * @return csv
 */ 
 
	 public function admin_export() {
	    $results = $this->Country->find('all');
	    $this->response->download('Crowdfunding-Export-'.'countrys-'.date('d-m-Y').'.csv');
	    $_serialize = 'results';
	    $_header = array('Country ID', 'Status', 'Created');
	    $_extract = array('Country.id', 'Country.status', 'Country.created');
		$this->viewClass = 'CsvView.Csv';
	    $this->set(compact('results', '_serialize', '_header', '_extract'));
	 }
}
