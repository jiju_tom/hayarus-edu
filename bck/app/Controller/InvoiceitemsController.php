<?php
App::uses('AppController', 'Controller');
/**
 * Invoiceitems Controller
 *
 * @property Invoiceitem $Invoiceitem
 * @property PaginatorComponent $Paginator
 */
class InvoiceitemsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

public function beforeFilter() {
		parent::beforeFilter();
		$this->layout='admin_default';
	}
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		if (!empty($this->data)){
			if(isset($this->data['Invoiceitem']['limit'])){
            	$limit = $this->data['Invoiceitem']['limit'];
				$this->Session->write('default_limit', $limit);
			}
		}else{
			if($this->Session->check('default_limit'))
				$limit = $this->Session->read('default_limit');
			else
				$limit = $this->default_limit;
		}
		$search_conditions = array();
		$conditions = array();
		$this->set("search_string", "");
		if(isset($this->params->query['search'])){
			$this->set("search_string", $this->params->query['search']);
			$conditions = array('OR' => array(
			'Invoiceitem.name LIKE "%'.trim(addslashes($this->params->query['search'])).'%"')); 
		}
		$this->paginate  = array(
				'limit' => $limit, 
				'order' => 'Invoiceitem.id DESC',
				'conditions' => array(array_merge($conditions,$search_conditions))
			);
		$this->Invoiceitem->recursive = 0;
		$this->set('invoiceitems', $this->Paginator->paginate());
		$this->set('limit', $limit);
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Invoiceitem->exists($id)) {
			throw new NotFoundException(__('Invalid invoiceitem'));
		}
		$options = array('conditions' => array('Invoiceitem.' . $this->Invoiceitem->primaryKey => $id));
		$this->set('invoiceitem', $this->Invoiceitem->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Invoiceitem->create();
			if ($this->Invoiceitem->save($this->request->data)) {
				$this->Session->setFlash('The invoiceitem has been added successfully.','flash_success');
				return $this->redirect(array('action' => 'edit', $this->Invoiceitem->id));
			} else {
				$this->Session->setFlash('The invoiceitem could not be added. Please, try again.','flash_failure');
			}
		}
		$invoices = $this->Invoiceitem->Invoice->find('list');
		$this->set(compact('invoices'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Invoiceitem->exists($id)) {
			throw new NotFoundException(__('Invalid invoiceitem'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Invoiceitem->save($this->request->data)) {
				$this->Session->setFlash('The invoiceitem has been updated successfully.','flash_success');
				return $this->redirect(array('action' => 'edit', $this->Invoiceitem->id));
			} else {
				$this->Session->setFlash('The invoiceitem could not be updated. Please, try again.','flash_failure');
			}
		} else {
			$options = array('conditions' => array('Invoiceitem.' . $this->Invoiceitem->primaryKey => $id));
			$this->request->data = $this->Invoiceitem->find('first', $options);
		}
		$invoices = $this->Invoiceitem->Invoice->find('list');
		$this->set(compact('invoices'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Invoiceitem->id = $id;
		if (!$this->Invoiceitem->exists()) {
			throw new NotFoundException(__('Invalid invoiceitem'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Invoiceitem->delete()) {
			$this->Session->setFlash('The invoiceitem has been deleted successfully.','flash_success');
		} else {
			$this->Session->setFlash('The invoiceitem could not be deleted. Please, try again.','flash_failure');
		}
		return $this->redirect(array('action' => 'index'));
	}

	/**
 * admin_export method
 *
 * @throws NotFoundException
 * @return csv
 */ 
 
	 public function admin_export() {
	    $results = $this->Invoiceitem->find('all');
	    $this->response->download('Crowdfunding-Export-'.'invoiceitems-'.date('d-m-Y').'.csv');
	    $_serialize = 'results';
	    $_header = array('Invoiceitem ID', 'Status', 'Created');
	    $_extract = array('Invoiceitem.id', 'Invoiceitem.status', 'Invoiceitem.created');
		$this->viewClass = 'CsvView.Csv';
	    $this->set(compact('results', '_serialize', '_header', '_extract'));
	 }
}
