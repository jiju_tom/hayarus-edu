<?php
App::uses('AppController', 'Controller');
/**
 * Studentreferences Controller
 *
 * @property Studentreference $Studentreference
 * @property PaginatorComponent $Paginator
 */
class StudentreferencesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

public function beforeFilter() {
		parent::beforeFilter();
		$this->layout='admin_default';
	}
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		if (!empty($this->data)){
			if(isset($this->data['Studentreference']['limit'])){
            	$limit = $this->data['Studentreference']['limit'];
				$this->Session->write('default_limit', $limit);
			}
		}else{
			if($this->Session->check('default_limit'))
				$limit = $this->Session->read('default_limit');
			else
				$limit = $this->default_limit;
		}
		$search_conditions = array();
		$conditions = array();
		$this->set("search_string", "");
		if(isset($this->params->query['search'])){
			$this->set("search_string", $this->params->query['search']);
			$conditions = array('OR' => array(
			'Studentreference.name LIKE "%'.trim(addslashes($this->params->query['search'])).'%"')); 
		}
		$this->paginate  = array(
				'limit' => $limit, 
				'order' => 'Studentreference.id DESC',
				'conditions' => array(array_merge($conditions,$search_conditions))
			);
		$this->Studentreference->recursive = 0;
		$this->set('studentreferences', $this->Paginator->paginate());
		$this->set('limit', $limit);
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Studentreference->exists($id)) {
			throw new NotFoundException(__('Invalid studentreference'));
		}
		$options = array('conditions' => array('Studentreference.' . $this->Studentreference->primaryKey => $id));
		$this->set('studentreference', $this->Studentreference->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Studentreference->create();
			if ($this->Studentreference->save($this->request->data)) {
				$this->Session->setFlash('The studentreference has been added successfully.','flash_success');
				return $this->redirect(array('action' => 'edit', $this->Studentreference->id));
			} else {
				$this->Session->setFlash('The studentreference could not be added. Please, try again.','flash_failure');
			}
		}
		$courses = $this->Studentreference->Course->find('list');
		$this->set(compact('courses'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Studentreference->exists($id)) {
			throw new NotFoundException(__('Invalid studentreference'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Studentreference->save($this->request->data)) {
				$this->Session->setFlash('The studentreference has been updated successfully.','flash_success');
				return $this->redirect(array('action' => 'edit', $this->Studentreference->id));
			} else {
				$this->Session->setFlash('The studentreference could not be updated. Please, try again.','flash_failure');
			}
		} else {
			$options = array('conditions' => array('Studentreference.' . $this->Studentreference->primaryKey => $id));
			$this->request->data = $this->Studentreference->find('first', $options);
		}
		$courses = $this->Studentreference->Course->find('list');
		$this->set(compact('courses'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Studentreference->id = $this->request->data['id'];
		
		$this->request->allowMethod('post', 'delete');
		if ($this->Studentreference->delete()) {
			$this->Session->setFlash('The studentreference has been deleted successfully.','flash_success');
		} else {
			$this->Session->setFlash('The studentreference could not be deleted. Please, try again.','flash_failure');
		}
		echo 1; exit;
	}

	/**
 * admin_export method
 *
 * @throws NotFoundException
 * @return csv
 */ 
 
	 public function admin_export() {
	    $results = $this->Studentreference->find('all');
	    $this->response->download('Crowdfunding-Export-'.'studentreferences-'.date('d-m-Y').'.csv');
	    $_serialize = 'results';
	    $_header = array('Studentreference ID', 'Status', 'Created');
	    $_extract = array('Studentreference.id', 'Studentreference.status', 'Studentreference.created');
		$this->viewClass = 'CsvView.Csv';
	    $this->set(compact('results', '_serialize', '_header', '_extract'));
	 }
}
