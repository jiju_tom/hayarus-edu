<?php
App::uses('AppController', 'Controller');
/**
 * Collegeimages Controller
 *
 * @property Collegeimage $Collegeimage
 * @property PaginatorComponent $Paginator
 */
class CollegeimagesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

public function beforeFilter() {
		parent::beforeFilter();
		$this->layout='admin_default';
	}
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		if (!empty($this->data)){
			if(isset($this->data['Collegeimage']['limit'])){
            	$limit = $this->data['Collegeimage']['limit'];
				$this->Session->write('default_limit', $limit);
			}
		}else{
			if($this->Session->check('default_limit'))
				$limit = $this->Session->read('default_limit');
			else
				$limit = $this->default_limit;
		}
		$search_conditions = array();
		$conditions = array();
		$this->set("search_string", "");
		if(isset($this->params->query['search'])){
			$this->set("search_string", $this->params->query['search']);
			$conditions = array('OR' => array(
			'Collegeimage.name LIKE "%'.trim(addslashes($this->params->query['search'])).'%"')); 
		}
		$this->paginate  = array(
				'limit' => $limit, 
				'order' => 'Collegeimage.id DESC',
				'conditions' => array(array_merge($conditions,$search_conditions))
			);
		$this->Collegeimage->recursive = 0;
		$this->set('collegeimages', $this->Paginator->paginate());
		$this->set('limit', $limit);
	}

	public function admin_getAllImages() {

		$this->render(false);
		//pr($this->request->data);exit;
		$options = array('conditions' => array('Collegeimage.college_id' => $this->request->data['id']));
		$collegeImages =  $this->Collegeimage->find('all', $options);
		//pr($collegeImages);exit;
		echo json_encode($collegeImages);

	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Collegeimage->exists($id)) {
			throw new NotFoundException(__('Invalid collegeimage'));
		}
		$options = array('conditions' => array('Collegeimage.' . $this->Collegeimage->primaryKey => $id));
		$this->set('collegeimage', $this->Collegeimage->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Collegeimage->create();
			if ($this->Collegeimage->save($this->request->data)) {
				$this->Session->setFlash('The collegeimage has been added successfully.','flash_success');
				return $this->redirect(array('action' => 'edit', $this->Collegeimage->id));
			} else {
				$this->Session->setFlash('The collegeimage could not be added. Please, try again.','flash_failure');
			}
		}
		$colleges = $this->Collegeimage->College->find('list');
		$this->set(compact('colleges'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Collegeimage->exists($id)) {
			throw new NotFoundException(__('Invalid collegeimage'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Collegeimage->save($this->request->data)) {
				$this->Session->setFlash('The collegeimage has been updated successfully.','flash_success');
				return $this->redirect(array('action' => 'edit', $this->Collegeimage->id));
			} else {
				$this->Session->setFlash('The collegeimage could not be updated. Please, try again.','flash_failure');
			}
		} else {
			$options = array('conditions' => array('Collegeimage.' . $this->Collegeimage->primaryKey => $id));
			$this->request->data = $this->Collegeimage->find('first', $options);
		}
		$colleges = $this->Collegeimage->College->find('list');
		$this->set(compact('colleges'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {

		$this->render(false);
		$this->request->allowMethod('post', 'delete');
		$this->Collegeimage->id = $this->request->data['id'];
		if ($this->Collegeimage->delete()) {
			//$this->Session->setFlash('The collegeimage has been deleted successfully.','flash_success');
		} else {
			$this->Session->setFlash('The collegeimage could not be deleted. Please, try again.','flash_failure');
		}
		echo 1; exit;
	}

	/**
 * admin_export method
 *
 * @throws NotFoundException
 * @return csv
 */ 
 
	 public function admin_export() {
	    $results = $this->Collegeimage->find('all');
	    $this->response->download('Crowdfunding-Export-'.'collegeimages-'.date('d-m-Y').'.csv');
	    $_serialize = 'results';
	    $_header = array('Collegeimage ID', 'Status', 'Created');
	    $_extract = array('Collegeimage.id', 'Collegeimage.status', 'Collegeimage.created');
		$this->viewClass = 'CsvView.Csv';
	    $this->set(compact('results', '_serialize', '_header', '_extract'));
	 }
}
