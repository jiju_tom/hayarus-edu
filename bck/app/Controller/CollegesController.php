<?php
App::uses('AppController', 'Controller');
/**
 * Colleges Controller
 *
 * @property College $College
 * @property PaginatorComponent $Paginator
 */
class CollegesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

public function beforeFilter() {
		parent::beforeFilter();
		$this->layout='admin_default';
	}
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		if (!empty($this->data)){
			if(isset($this->data['College']['limit'])){
            	$limit = $this->data['College']['limit'];
				$this->Session->write('default_limit', $limit);
			}
		}else{
			if($this->Session->check('default_limit'))
				$limit = $this->Session->read('default_limit');
			else
				$limit = $this->default_limit;
		}
		$search_conditions = array();
		$conditions = array();
		$this->set("search_string", "");
		if(isset($this->params->query['search'])){
			$this->set("search_string", $this->params->query['search']);
			$conditions = array('OR' => array(
			'College.name LIKE "%'.trim(addslashes($this->params->query['search'])).'%"')); 
		}
		$this->paginate  = array(
				'limit' => $limit, 
				'order' => 'College.id DESC',
				'conditions' => array(array_merge($conditions,$search_conditions))
			);
		$this->College->recursive = -1;
		//pr($this->Paginator->paginate());
		$this->set('colleges', $this->Paginator->paginate());
		$this->set('limit', $limit);

		$this->loadModel('Country');
		$this->loadModel('States');
		$this->loadModel('City');

		$countries = $this->Country->find('list'); 
		$states = $this->States->find('list'); 
		$cities = $this->City->find('list'); 

		$this->set(compact( 'countries', 'states', 'cities'));


	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->College->exists($id)) {
			throw new NotFoundException(__('Invalid college'));
		}
		$options = array('conditions' => array('College.' . $this->College->primaryKey => $id));
		$this->set('college', $this->College->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {

		$this->loadModel('Country');
		$this->loadModel('States');
		$this->loadModel('City');
		if ($this->request->is('post')) {
			$this->College->create();
			if ($this->College->save($this->request->data)) {			
				$this->Session->setFlash('The college has been added successfully.','flash_success');
				return $this->redirect(array('action' => 'edit', $this->College->id));
			} else {
				$this->Session->setFlash('The college could not be added. Please, try again.','flash_failure');
			}
		}

		$countries = $this->Country->find('list'); 
		$states = $this->States->find('list'); 
		$cities = $this->City->find('list'); 
		$rating = $this->rating;
		$this->set(compact( 'countries', 'states', 'cities', 'rating'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {

		$this->loadModel('Country');
		$this->loadModel('States');
		$this->loadModel('City');
		$this->loadModel('Course');

		$this->Session->write('collegeId', $id);


		if (!$this->College->exists($id)) {
			throw new NotFoundException(__('Invalid college'));
		}

		if ($this->request->is(array('post', 'put'))) {

			$this->request->data['College']['id'] = $id;
			
			if ($this->College->save($this->request->data)) {

				if(isset($this->request->data['Course'])){
					/*saving college course details*/
					$this->loadModel('Collegecourse');				
					$this->request->data['Collegecourse'] = $this->request->data['Course'];

					if(empty($this->request->data['Course']['collegecourse_id'])){ 
						$this->Collegecourse->create();
					}else{	
						$this->request->data['Collegecourse']['id'] = $this->request->data['Course']['collegecourse_id'];
					}

					$this->request->data['Collegecourse']['college_id'] = $id;
					//$resppo = $this->Collegecourse->save($this->request->data['Collegecourse']);
					//pr($resppo);exit;
					if($this->Collegecourse->save($this->request->data['Collegecourse'])){
						/*saving college expense details*/
						$this->loadModel('Courseexpense');

						if(empty($this->request->data['Course']['courseexpense_id'])){
							$this->Courseexpense->create();
						}else{
							$this->request->data['Courseexpense']['id'] = $this->request->data['Course']['courseexpense_id'];
						}

						$this->request->data['Courseexpense']['course_id'] = $this->request->data['Course']['course_id'];
						$this->request->data['Courseexpense']['college_id'] = $id;
						$this->request->data['Courseexpense']['admission_fee'] = $this->request->data['Course']['admission_fee'];
						$this->request->data['Courseexpense']['gender'] = $this->request->data['Course']['gender'];
						$this->request->data['Courseexpense']['year1'] = $this->request->data['Course']['year1'];
						$this->request->data['Courseexpense']['year2'] = $this->request->data['Course']['year2'];
						$this->request->data['Courseexpense']['year3'] = $this->request->data['Course']['year3'];
						$this->request->data['Courseexpense']['year4'] = $this->request->data['Course']['year4'];
						$this->request->data['Courseexpense']['year5'] = $this->request->data['Course']['year5'];
						$this->request->data['Courseexpense']['year6'] = $this->request->data['Course']['year6'];
						$this->request->data['Courseexpense']['internship'] = $this->request->data['Course']['internship'];
						$total = ((int)$this->request->data['Course']['year1'] + (int)$this->request->data['Course']['year2'] + (int)$this->request->data['Course']['year3'] + (int)$this->request->data['Course']['year4'] +  (int)$this->request->data['Course']['year5'] + (int)$this->request->data['Course']['year6'] + (int)$this->request->data['Course']['internship'] + (int)$this->request->data['Course']['admission_fee']);
						$this->request->data['Courseexpense']['total'] = $total;
						$this->request->data['Courseexpense']['notes'] = $this->request->data['Course']['notes'];
						

						$this->Courseexpense->save($this->request->data['Courseexpense']);

						

					}else{
						$this->Session->setFlash('Course details could not be updated. Please, try again.','flash_failure');
					}
				}

			$this->Session->setFlash('The college has been updated successfully.','flash_success');
			return $this->redirect(array('action' => 'edit', $id));

			} else {
				$this->Session->setFlash('The college could not be updated. Please, try again.','flash_failure');
			}
		} else {
			$options = array('conditions' => array('College.' . $this->College->primaryKey => $id));
			$this->request->data = $this->College->find('first', $options);
			//pr($this->request->data);
		}

		$countries = $this->Country->find('list'); 
		$states = $this->States->find('list'); 
		$cities = $this->City->find('list'); 
		$courses = $this->Course->find('list');

		$ImagePath = $this->ImagePath;
		$college_type_dropdown = $this->college_type_dropdown;
		$rating = $this->rating;

		$this->set(compact( 'countries', 'states', 'cities', 'courses', 'ImagePath', 'college_type_dropdown', 'rating'));
	}


	public function admin_studentreference() {
		if ($this->request->is('post')) {
			//pr($this->request->data);exit;

			$this->loadModel('Studentreference');
			if(empty($this->request->data['ref_id'])){
				$this->Studentreference->create();
			}else{
				$this->request->data['Studentreference']['id'] = $this->request->data['ref_id'];
			}
			

			$this->request->data['Studentreference']['college_id'] = $this->request->data['refcollege_id'];
			$this->request->data['Studentreference']['course_id'] = $this->request->data['refcourse_id'];
			$this->request->data['Studentreference']['name'] = $this->request->data['student_name'];
			$this->request->data['Studentreference']['phone'] = $this->request->data['phone'];
			$this->request->data['Studentreference']['location'] = $this->request->data['student_location'];
			$this->request->data['Studentreference']['notes'] = $this->request->data['notes'];

			if ($this->Studentreference->save($this->request->data['Studentreference'])) {
				$this->Session->setFlash('The student reference has been added successfully.','flash_success');
				return $this->redirect(array('controller'=> 'colleges','action' => 'edit', $this->request->data['refcollege_id']));
			} else {
				$this->Session->setFlash('The studentreference could not be added. Please, try again.','flash_failure');
			}
		}
		$this->loadModel('Course');
		$courses = $this->Course->find('list');
		$this->set(compact('courses'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null ) {
		$this->College->id = $id;
		if (!$this->College->exists()) {
			throw new NotFoundException(__('Invalid college'));
		}

		$ImagePath = $this->ImagePath;

		$this->loadModel('Courseexpense');
		$this->loadModel('Collegecourse');
		$this->loadModel('Collegeimage');

		$this->request->allowMethod('post', 'delete');
		if ($this->College->delete()) {

			$this->Collegecourse->college_id = $id;
			$this->Collegecourse->deleteAll(array('Collegecourse.college_id' => $id));

			$this->Courseexpense->college_id = $id;
			$this->Courseexpense->deleteAll(array('Courseexpense.college_id' => $id));

			$options = array('conditions' => array('Collegeimage.college_id' => $id));
			$collegeImages =  $this->Collegeimage->find('all', $options);

			foreach ($collegeImages as $key => $image) {
				unlink(WWW_ROOT .$ImagePath.$image['Collegeimage']['name']);  
			}

			$this->Collegeimage->college_id = $id;
			$this->Collegeimage->deleteAll(array('Collegeimage.college_id' => $id));
			

			$this->Session->setFlash('The college has been deleted successfully.','flash_success');

		} else {
			$this->Session->setFlash('The college could not be deleted. Please, try again.','flash_failure');
		}
		return $this->redirect(array('action' => 'index'));
	}


	public function admin_collegecoursedelete(){

		$this->render(false);
		$this->loadModel('Courseexpense');
		$this->loadModel('Collegecourse');	

		$deleteFlag = 0;
		if(!empty($this->request->data['collegecourse_id'])){
			$this->Collegecourse->id = $this->request->data['collegecourse_id'];
			$this->Collegecourse->delete();
			$deleteFlag = 1;
		}else{
			$deleteFlag = 0;
		}

		if(!empty($this->request->data['courseexpense_id'])){
			$this->Courseexpense->id = $this->request->data['courseexpense_id'];
			$this->Courseexpense->delete();
			$deleteFlag = 1;
		}else{
			$deleteFlag = 0;
		}


		$this->Session->setFlash('The course has been deleted successfully.','flash_success');
		echo $deleteFlag; exit;
	}

	/**
 * admin_export method
 *
 * @throws NotFoundException
 * @return csv
 */ 
 
	 public function admin_export() {
	    $results = $this->College->find('all');
	    $this->response->download('Crowdfunding-Export-'.'colleges-'.date('d-m-Y').'.csv');
	    $_serialize = 'results';
	    $_header = array('College ID', 'Status', 'Created');
	    $_extract = array('College.id', 'College.status', 'College.created');
		$this->viewClass = 'CsvView.Csv';
	    $this->set(compact('results', '_serialize', '_header', '_extract'));
	 }
}
