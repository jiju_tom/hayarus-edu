<?php $user = $this->Session->read('Auth.User'); 
    $groupId = $this->Session->read('Auth.Group.id'); //pr($groupId);exit;
?>
<ul class="page-sidebar-menu">
<li class="sidebar-toggler-wrapper">
                    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                    <!-- <div class="sidebar-toggler hidden-phone">
                    </div> -->
                    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                </li>
                <br>
                
                <li class=" <?php if($this->request->params['controller']=='dashboard' && ($this->request->params['action']=='admin_index') || ($this->request->params['action']=='admin_searchdetail'))echo 'active';?> " >
                <a class="loadButton" href="<?php echo $this->webroot; ?>admin/dashboard">
                    <i class="fa fa-magic"></i>
                    <span class="title">
                        Search Colleges
                    </span>
                    <span class="selected">
                    </span>
                </a>
                </li> 
                <?php  if($groupId != 4 && $groupId != 5 ){ ?>
                <li class=" <?php if(($this->request->params['controller']=='areaofstudies')||($this->request->params['controller']=='Areaofstudies') && ($this->request->params['action']=='admin_index'))echo 'active';?>"  >
                <a class="loadButton" href="<?php echo $this->webroot; ?>admin/areaofstudies/index">
                    <i class="fa fa-link"></i>
                    <span class="title">
                      Area of studies<span class="badge badge-danger"></span>
                    </span>
                    <span class="selected">
                    </span>
                </a>
                </li>
                
                <li class=" <?php if(($this->request->params['controller']=='courses')||($this->request->params['controller']=='Advertisements') && ($this->request->params['action']=='admin_add' || $this->request->params['action'] == 'admin_edit' || $this->request->params['action'] == 'admin_view' || $this->request->params['action'] == 'admin_index'))echo 'active';?>"  >
                <a class="loadButton" href="<?php echo $this->webroot; ?>admin/courses">
                    <i class="fa fa-eraser"></i>
                    <span class="title">
                     Course Management <span class="badge badge-danger"></span>
                    </span>
                    <span class="selected">
                    </span>
                </a>
                </li>
    
                
                <li class=" <?php if(($this->request->params['controller']=='colleges')||($this->request->params['controller']=='colleges') && ($this->request->params['action']=='admin_add'))echo 'active';?>"  >
                <a class="loadButton" href="<?php echo $this->webroot; ?>admin/colleges/index">
                    <i class="fa fa-picture-o"></i>
                    <span class="title">
                      College Management<span class="badge badge-danger"></span>
                    </span>
                    <span class="selected">
                    </span>
                </a>
                </li>

                <li class=" <?php if(($this->request->params['controller']=='students')&&($this->request->params['controller']=='students') && ($this->request->params['action']=='admin_index' || $this->request->params['action']=='admin_attend'))echo 'active';?>"  >
                <a class="loadButton" href="<?php echo $this->webroot; ?>admin/students/index">
                    <i class="fa fa-list-ol"></i>
                    <span class="title">
                      Student Database<span class="badge badge-danger"></span>
                    </span>
                    <span class="selected">
                    </span>
                </a>
                </li>

                <li class=" <?php if(($this->request->params['controller']=='scholorships')||($this->request->params['controller']=='scholorships') && ($this->request->params['action']=='admin_index'))echo 'active';?>"  >
                <a class="loadButton" href="<?php echo $this->webroot; ?>admin/scholorships/index">
                    <i class="fa fa-clipboard" aria-hidden="true"></i>
                    <span class="title">
                      Scholarship Applicants<span class="badge badge-danger"></span>
                    </span>
                    <span class="selected">
                    </span>
                </a>
                </li>

                <li>
                <a class="loadButton" href="https://dashboard.tawk.to" target="_blank">
                    <i class="fa fa-comments-o"></i>
                    <span class="title">
                      Chat Bot<span class="badge badge-danger"></span>
                    </span>
                    <span class="selected">
                    </span>
                </a>
                </li>

                <li class=" <?php if(($this->request->params['controller']=='invoices')||($this->request->params['controller']=='invoices') && ($this->request->params['action']=='admin_index'))echo 'active';?>"  >
                <a class="loadButton" href="<?php echo $this->webroot; ?>admin/invoices/index">
                    <i class="fa fa-clipboard" aria-hidden="true"></i>
                    <span class="title">
                      Invoices<span class="badge badge-danger"></span>
                    </span>
                    <span class="selected">
                    </span>
                </a>
                </li>

                <?php } ?>

                <?php if($groupId != 5){ ?>
                <li class=" <?php if(($this->request->params['controller']=='students')&&($this->request->params['controller']=='students' && $this->request->params['action']=='admin_admission')) echo 'active';?>"  >
                <a class="loadButton" href="<?php echo $this->webroot; ?>admin/students/admission">
                    <i class="fa fa-tachometer"></i>
                    <span class="title">
                      My Admissions<span class="badge badge-danger"></span>
                    </span>
                    <span class="selected">
                    </span>
                </a>
                </li>
                <?php } ?>

                <?php if($groupId == 5){ ?>
                    <li class=" <?php if(($this->request->params['controller']=='users')&&($this->request->params['controller']=='users' && $this->request->params['action']=='admin_wallet')) echo 'active';?>"  >
                <a class="loadButton" href="<?php echo $this->webroot; ?>admin/users/wallet">
                    <i class="fa fa-tachometer"></i>
                    <span class="title">
                      My Admission Wallet<span class="badge badge-danger"></span>
                    </span>
                    <span class="selected">
                    </span>
                </a>
                </li>
                <?php } ?>

                <?php if(($groupId != 4 ||  $groupId != 5) && $groupId == 1){ ?>

                    
                
                
                <li class=" <?php if(($this->request->params['controller']=='users')||($this->request->params['controller']=='users') && ($this->request->params['action']=='admin_index'))echo 'active';?>"  >
                <a class="loadButton" href="<?php echo $this->webroot; ?>admin/users/index">
                    <i class="fa fa-user"></i>
                    <span class="title">
                      Users<span class="badge badge-danger"></span>
                    </span>
                    <span class="selected">
                    </span>
                </a>
                </li>
                
                


                <!-- <li class=" <?php if(($this->request->params['controller']=='expenses')||($this->request->params['controller']=='expenses') && ($this->request->params['action']=='admin_index') && ($this->request->params['action']=='admin_add') && ($this->request->params['action']=='admin_edit') )echo 'active';?>"  >
                <a class="loadButton" href="<?php echo $this->webroot; ?>admin/expenses/index">
                    <i class="fa fa-money"></i>
                    <span class="title">
                      Expenses<span class="badge badge-danger"></span>
                    </span>
                    <span class="selected">
                    </span>
                </a>
                </li>
                 <li class=" <?php if(($this->request->params['controller']=='invoices')||($this->request->params['controller']=='invoices') && ($this->request->params['action']=='admin_index'))echo 'active';?>"  > <a class="loadButton" href="<?php echo $this->webroot; ?>admin/invoices/index"> <i class="fa fa-clipboard"></i>
                    <span class="title">
                     Invoices<span class="badge badge-danger"></span>
                    </span>
                    <span class="selected">
                    </span>
                </a>
                </li> -->
                
                <?php } ?>

                 <li class=" <?php if(($this->request->params['controller']=='Users') && ($this->request->params['action']=='admin_profile'))echo 'active';?>"  >
                <a class="loadButton" href="<?php echo $this->webroot; ?>admin/users/profile">
                    <i class="fa fa-user"></i>
                    <span class="title">
                      Profile <span class="badge badge-danger"></span>
                    </span>
                    <span class="selected">
                    </span>
                </a>
                </li>
              
             <li class="" >
                <a class="loadButton" href="<?php echo $this->webroot; ?>admin/users/logout">
                    <!--<i class="fa fa-power-off"></i>-->
                    <i class="fa fa-key"></i>
                    <span class="title">
                        Logout
                    </span>
                    <span class="selected">
                    </span>
                </a>
                </li>   <!--  -->
</ul>                