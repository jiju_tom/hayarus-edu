<?php $this->Html->addCrumb('College', '/admin/colleges'); ?>
<?php $this->Html->addCrumb('View', ''); ?>
<div style='padding-bottom:10px;'><?php echo $this->Session->flash(); ?></div>

<div class="row">
    <div class="col-md-12">
        <div class="tab-content">
            <div class="tab-pane active" id="tab_0">
                 <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                        	<i class="fa fa-reorder"></i><?php echo __('View College'); ?>                        				</div>
	                        <div class="tools">
	                            <a href="javascript:;" class="collapse"></a>
	                        </div>
	                    </div>
                    	<div class="portlet-body form colleges">
		                    <div class="alert alert-danger display-hide">
		                        <button data-close="alert" class="close"></button>
		                        You have some form errors. Please check below.
		                    </div>
		                    <!-- BEGIN FORM-->
		                    <?php echo $this->Form->create('College', array('class' => 'form-horizontal')); ?>
		                    <div class="form-body">                      
		                        						<div class='form-group'>
							 <?php echo $this->Form->label('name:' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<p class="form-control-static"><?php echo h($college['College']['name']);?></p>
							</div>
						</div>
						<div class='form-group'>
							 <?php echo $this->Form->label('description:' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<p class="form-control-static"><?php echo h($college['College']['description']);?></p>
							</div>
						</div>
						<div class='form-group'>
							 <?php echo $this->Form->label('location_id:' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<p class="form-control-static"><?php echo h($college['College']['location_id']);?></p>
							</div>
						</div>
						<div class='form-group'>
							 <?php echo $this->Form->label('course_id:' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<p class="form-control-static"><?php echo h($college['College']['course_id']);?></p>
							</div>
						</div>
		                            <div class="form-actions fluid">
		                                <div class="col-md-offset-3 col-md-9">
		                                    <button type="button" class="btn blue" onclick="window.location='<?php echo $this->webroot.'admin/colleges'; ?>'">Go Back</button>
		                                </div>
		                            </div>
		                        </div>
		                        <?php echo $this->Form->end(); ?>
		                        <!-- END FORM-->
		                    </div>
                	</div>
            </div>
        </div>
    </div>
</div>