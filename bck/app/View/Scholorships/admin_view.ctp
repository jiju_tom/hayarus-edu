<?php $this->Html->addCrumb('Scholarship', '/admin/scholorships'); ?>
<?php $this->Html->addCrumb('View', ''); //pr($scholorships); ?>
<div style='padding-bottom:10px;'><?php echo $this->Session->flash(); ?></div>

<div class="row">
    <div class="col-md-12">
        <div class="tab-content">
            <div class="tab-pane active" id="tab_0">
                 <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                        	<i class="fa fa-reorder"></i><?php echo __('View Scholarship'); ?>                        				</div>
	                        <div class="tools">
	                            <a href="javascript:;" class="collapse"></a>
	                        </div>
	                    </div>
                    	<div class="portlet-body form scholorships">
		                    <div class="alert alert-danger display-hide">
		                        <button data-close="alert" class="close"></button>
		                        You have some form errors. Please check below.
		                    </div>
		                    <div class="tabbable tabbable-custom tabbable-custom-profile">
											
											<div class="tab-content">
												<div class="tab-pane active">
													<div class="portlet-body">
														<p>Name: <b><?php echo $scholorships[0]['User']['first_name']; ?></b></p>
														<p>Email: <b><?php echo $scholorships[0]['User']['email']; ?></b></p>
														<p>School: <b><?php echo $scholorships[0]['User']['school']; ?></b></p>
														<p>Phone: <b><?php echo $scholorships[0]['User']['phone']; ?></b></p>
														<p>Wallet Code: <b><?php echo $scholorships[0]['User']['code']; ?></b></p>
														
														<div class="table-responsive">
														<?php echo $this->Form->create('Scholorship', array('class' => '','role' => 'form', 'url' => array('controller' => 'scholorships', 'action' => 'admin_edit'))); ?>


															
										                <table class="table table-striped table-bordered table-hover " id="sample_1">
										                  <thead>
										                      <tr>
										                        <th>College</th>  
										                        <th>Course</th>
										                        <th>Status</th>
										                        <th>Update</th>
										                        
										                      </tr>
										                  </thead>
										                  
										                  <tbody>
											                  <?php if(!empty($scholorships)){

											                   foreach ($scholorships as $key => $value) { ?>
												                  <tr>
											                     		<td>
											                     			<?php echo $this->Form->input('id', array('class' => 'form-control input-medium', 'label' => false, 'required' => false, 'value' => $value['Scholorship']['id'], 'type' => 'hidden'));?>

											                     			<?php echo $this->Form->input('user_id', array('class' => 'form-control input-medium', 'label' => false, 'required' => false, 'value' => $value['Scholorship']['user_id'], 'type' => 'hidden'));?>


											                     			<select class="form-control input select2me" data-placeholder="Select College" name="data[Scholorship][college_id]">
																				<option value=""></option>
																				<?php foreach($colleges as $key=> $val){ ?>
																						<option value="<?php echo $key ?>" <?php if(isset($value['Scholorship']['college_id']) && ($value['Scholorship']['college_id'] == $key)){echo "selected"; } ?> ><?php echo $val; ?></option>
																				<?php } ?>
																			</select>

											                     		</td>
											        					<td>

											        						<select class="form-control input select2me" data-placeholder="Select Course" name="data[Scholorship][course_id]">
																				<option value=""></option>
																				<?php foreach($courses as $key=> $val){ ?>
																						<option value="<?php echo $key ?>" <?php if(isset($value['Scholorship']['course_id']) && ($value['Scholorship']['course_id'] == $key)){echo "selected"; } ?>><?php echo $val; ?></option>
																				<?php } ?>
																			</select>

																		
													             		</td>
											                  			<td>
											                  				<?php echo $this->Form->input('status', array('class' => 'form-control input-medium', 'label' => false, 'required' => false, 'empty'=>'Select', 'options' => $scholorship_status , 'selected' => $value['Scholorship']['status']));?>
											                  				

																		
													              		</td>
													              		<td><button id="submitReg" type="submit" class="btn blue" >Update</button></td>
												                  </tr>
															  <?php } 
																	}else{ ?>


																	<tr><td colspan='7' align='center'>No Records Found.</td></tr>
																<?php } ?>		
										                   
										                  </tbody>
										                </table>
										                <?php echo $this->Form->end(); ?>
									              </div>

														
													</div>
												</div>
												<!--tab-pane-->
												
											</div>
										</div>
		                    </div>
                	</div>
            </div>
        </div>
    </div>
</div>