<?php $paginationVariables = $this->Paginator->params(); 
$groupId = $this->Session->read('Auth.Group.id');

//pr($userDetails);

$ratedVal = isset($ratedVal) ? $ratedVal : "";

?>
<div style='padding-bottom:10px;'><?php //echo $this->Session->flash(); ?></div>
<br><br>
<style type="text/css">
.feeds .col2 .date {padding: 0px !important;}

.fivestar{ color: #d61717!important; font-weight: 400;" }

</style>
			
	<!-- BEGIN CONTENT -->

			<!-- BEGIN PAGE CONTENT-->

           <form action="<?php echo $this->webroot; ?>admin/dashboard/index/" class="form-horizontal" id="AdminSearchForm" method="get" accept-charset="utf-8" novalidate="novalidate" >
			<div class="row">
				<div class="col-md-12">
					<div class="tabbable tabbable-custom tabbable-full-width">
						
						<div class="tab-content">
							
								<div class="row">
									<div class="col-md-12">
										<?php if($groupId == 5 && $scholorshipCount == 1){  ?>
											<div class="alert alert-danger">
											<strong>   You've already applied. Check your application status in 'My Admission Wallet'.</strong>
											</div>
										<?php  }else if($groupId == 5 && $scholorshipCount < 1){ ?>
											<div class="alert alert-warning">
												<strong> You can only apply in one college. once applied, can't redo.</strong>
											</div>
										<?php  } ?>
										<div class="booking-search">
											<form action="#" role="form">
												<div class="row form-group">
													<div class="col-md-7">
														<!-- <label class="control-label">College Name</label> -->
														<div class="input-icon">

															<select class="form-control input select2me" data-placeholder="Select College" name="college_id">
																<option value=""></option>
																<?php foreach($colleges as $key=> $val){ ?>
																		<option value="<?php echo $key ?>" <?php if(isset($college_id) && ($college_id == $key)){echo "selected"; } ?> ><?php echo $val; ?></option>
																<?php } ?>
															</select>

														</div>
													</div>
													<div class="col-md-4">
														<select class="form-control input select2me" data-placeholder="Select Area of Study" name="areaofstudy_id">
															<option value=""></option>
															<?php foreach($areaofstudies as $key=> $val){ ?>
																	<option value="<?php echo $key ?>" <?php if(isset($areaofstudy_id) && ($areaofstudy_id == $key)){echo "selected"; } ?> ><?php echo $val; ?></option>
															<?php } ?>
														</select>	
															
													</div>

												</div>
												<div class="row form-group">
													
													<div class="col-md-6">
													<select class="form-control input select2me" data-placeholder="Select Course" name="course_id">
																<option value=""></option>
																<?php foreach($courses as $key=> $val){ ?>
																		<option value="<?php echo $key ?>" <?php if(isset($course_id) && ($course_id == $key)){echo "selected"; } ?>><?php echo $val; ?></option>
																<?php } ?>
															</select>
													</div>


													<div class="col-md-4">
													<?php if($groupId != 5 &&  $groupId != 4){ ?>
														<?php echo $this->Form->input('rating', array('class' => 'form-control', 'label' => false, 'required' => false, 'empty' => 'Select Rating' , 'options' => $rating, 'selected' => $ratedVal, 'name' => 'rating'));?>		
														<?php } ?>		
													</div>

		
												</div>

												

												<div class="row form-group ">
													
														<!-- <label class="control-label col-md-3">Course</label> -->
													<div class="col-md-3">

														<select class="form-control input select2me" data-placeholder="Select Country" name="country_id">
															<option value=""></option>
															<?php foreach($countries as $key=> $val){ ?>
																	<option value="<?php echo $key; ?>" <?php if(isset($country_id) && ($key == $country_id)){ echo "selected";  } else if($key == 101){echo "selected"; }?> ><?php echo $val; ?></option>
															<?php } ?>
														</select>

													</div>
													
													<div class="col-md-3">
														
														<select class="form-control input select2me" data-placeholder="Select State" name="state_id">
																<option value=""></option>
																<?php foreach($states as $key=> $val){ ?>
																		<option value="<?php echo $key ?>" <?php if(isset($state_id) && ($state_id == $key)){echo "selected"; } ?>><?php echo $val; ?></option>
																<?php } ?>
															</select>
															
													</div>

													<div class="col-md-3">
														

														<select class="form-control input select2me" data-placeholder="Select City" name="city_id">
																<option value=""></option>
																<?php foreach($cities as $key=> $val){ ?>
																		<option value="<?php echo $key ?>" <?php if(isset($city_id) && ($city_id == $key)){echo "selected"; } ?>><?php echo $val; ?></option>
																<?php } ?>
															</select>
															
													</div>
													<div class="col-md-3">
														<button class="btn blue btn-block ">SEARCH <i class="m-icon-swapright m-icon-white"></i></button>
													</div>

												</div>

												

											</form>
										</div>
									</div>
									<!--end booking-search-->
									
								</div>
								
									<div class="row booking-results">
										<?php  if(isset($collegelists) && sizeof($collegelists)>0) {?>
										<?php foreach($collegelists as $collegelist){ ?>
										<div class="col-md-6" style="margin-bottom: 15px;">
											<div class="booking-result">
												<a href="<?php echo $this->webroot; ?>admin/dashboard/searchdetail/<?php echo $collegelist['College']['id'] ?>">
													<div class="booking-img">
														<img src="<?php if($collegelist['Collegeimage'] != null) {echo $this->webroot.$ImagePath.$collegelist['Collegeimage'][0]['name']; } else{ echo $this->webroot.$ImagePath."no-image.jpg"; } ?>" alt="" class="img-responsive" style="max-height: 140px; min-height: 140px;">
														<!-- <ul class="list-unstyled price-location">
															
															<li>
																
															</li>
														</ul> -->
													</div>
												</a>
												<div class="booking-info" >
													<h2 ><a href="<?php echo $this->webroot; ?>admin/dashboard/searchdetail/<?php echo $collegelist['College']['id'] ?>" <?php if($collegelist['College']['rating'] == 1 && $groupId != 5 &&  $groupId != 4) { ?> class ="fivestar" <?php } ?> ><?php echo $collegelist['College']['name']; ?></a></h2>
													
													<p>
														<div style="width: 100%; font-size: 15px; color: #81bdf1; "><i class="fa fa-map-marker"></i>  <?php echo h($cities[$collegelist['College']['city_id']]); ?>, <?php echo h($states[$collegelist['College']['state_id']]); ?></div>
														<?php echo $this->Text->truncate($collegelist['College']['description'], 150,
													    array(
													        'ellipsis' => '...',
													        'exact' => false
													    ));  ?> 
														<a href="<?php echo $this->webroot; ?>admin/dashboard/searchdetail/<?php echo $collegelist['College']['id'] ?>">read more...</a>
													</p>
												</div>
											</div>
										</div>
										<?php   } ?>
								<?php   } ?>
									</div>
								

								<div class="col-md-7 col-sm-12 pull-right">
		                          <div class="dataTables_paginate paging_bootstrap">
		                            <?php $totalItem = $this->Paginator->counter('{:count}')?><?php $currentItem = $this->Paginator->counter('{:current}')?><?php if($totalItem>$currentItem) {?>
		                            <ul class="pagination" style="visibility: visible;">
		                                <li class="prev disabled">
		                                			<?php echo $this->Paginator->prev('< ' . __(''), array(), null, array('class' => 'prev disabled'));?>
			
		                                </li>
		                                <li>
		                                			<?php		echo $this->Paginator->numbers(array('separator' => ''));	?>
		                                </li>
		                                <li class="next disabled">
		                                			<?php		echo $this->Paginator->next(__('') . ' >', array(), null, array('class' => 'next'));	?>
		                                </li>
		                            </ul>
		                      			<?php }?>
    						        </div>
								<!-- <div class="margin-top-10">
									<ul class="pagination">
										<li>
											<a href="#">Prev</a>
										</li>
										<li>
											<a href="#">1</a>
										</li>
										<li>
											<a href="#">2</a>
										</li>
										<li class="active">
											<a href="#">3</a>
										</li>
										<li>
											<a href="#">4</a>
										</li>
										<li>
											<a href="#">5</a>
										</li>
										<li>
											<a href="#">Next</a>
										</li>
									</ul>
								</div> -->
							
							
						</div>
					</div>
				</div>
				<!--end tabbable-->
			</div>
			<?php echo $this->Form->end(); ?>
			<!-- END PAGE CONTENT-->


 <!-- Modal -->
 <div class="modal fade" id="getCodeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
       <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         <h4 class="modal-title" id="myModalLabel"></h4>
       </div>
       <div class="modal-body" id="getCode"  >
       	<img src="<?php echo $this->webroot; ?>img/congrats.png" style="text-align:center;  width : 100%;  height: 100%; max-width: 100%;  margin:0 auto; overflow-x: hidden; overflow-y: hidden;"/>
        
       </div>
    </div>
   </div>
 </div>


	<!-- END CONTENT -->		
<?php if($userDetails['User']['first_landing'] == 1 && $groupId == 5) { ?>
<script type="text/javascript">
	$(document).ready(function(){
		$("#getCodeModal").modal('show');
	});
</script>
<?php } ?>			