<?php //$this->Html->addCrumb('Admission', '/admin/students'); ?>
<?php //$this->Html->addCrumb('Invoice', ''); 



?>

<div style='padding-bottom:10px;'><?php echo $this->Session->flash(); ?></div>
<script type="text/javascript">
$(document).ready(function(){

	var delay = 1000;
	setTimeout(function() {
 	$('#company_name').val(<?php echo $Invoice['Invoice']['branch']; ?>).trigger('change');
	
	}, delay);
	
	$('#company_name').on('change', function(){ 
		var company_name = $('#company_name').val();
		if(company_name == 1){
			$("#companyImage").attr("src","<?php echo $this->webroot; ?>img/logo-hayarus.png");
			$("#addressHayarusSolutions").show();
			$("#addressHayarusTrust").hide();
		}else{
			$("#companyImage").attr("src","<?php echo $this->webroot; ?>img/trust-logo.png");
			$("#addressHayarusSolutions").hide();
			$("#addressHayarusTrust").show();
		}
		
	});

	$("i.fa").click(function() {
	    $(this).closest("tr").remove();
	});

	$( "#print_now" ).on( "click", function() { 
    	event.preventDefault();  
   		
    	var particulars1 = null;
    	var particulars2 = null;
    	var particulars3 = null;
    	var particulars4 = null;
    	var amount1 = null;
    	var amount2 = null;
    	var amount3 = null;
    	var amount4 = null;

    	var branch = $('#company_name').val();
    	var invoice_no = $('#invoice_no').val();
    	var about = $('#about').val();
    	var issued_to = $('#issued_to').val();
    	var invoice_id = $('#invoice_id').val();

    	if ($("#particulars1").length){  
   			particulars1 = $('#particulars1').val();
   		}
   		if ($("#particulars2").length){  
   			particulars2 = $('#particulars2').val();
   		}
   		
   		if ($("#particulars3").length){  
   			particulars3 = $('#particulars3').val();
   		}

   		if ($("#particulars4").length){  
   			particulars4 = $('#particulars4').val();
   		}


   		if ($("#amount1").length){  
   			amount1 = $('#amount1').val();
   		}
   		if ($("#amount2").length){  
   			amount2 = $('#amount2').val();
   		}
   		
   		if ($("#amount3").length){  
   			amount3 = $('#amount3').val();
   		}

   		if ($("#amount4").length){  
   			amount4 = $('#amount4').val();
   		}

   		if(particulars1 != "" || particulars2 != "" || particulars3 != "" ){ 
   			$.ajax({
	            url: '<?php echo $this->webroot.'admin/invoices/add'; ?>',
	            type: 'POST',
	            data: {"data[Invoice][branch]": branch  , "data[Invoice][invoice_no]": invoice_no, "data[Invoice][about]": about , "data[Invoice][issued_to]": issued_to, "data[Invoice][particular][0]": particulars1, "data[Invoice][particular][1]": particulars2, "data[Invoice][particular][2]": particulars3, "data[Invoice][particular][3]": particulars4, "data[Invoice][amount][0]": amount1, "data[Invoice][amount][1]": amount2, "data[Invoice][amount][2]": amount3, "data[Invoice][amount][3]": amount4, "data[Invoice][id]": invoice_id},
	            success: function (r) {
	              
	             if(r== 0){
	               alert('Something went wrong. try again.');
	              } else{
	                $('#company_name').hide();
			    	window.print(); // print the page
			    	$('#company_name').show();
	              }
	                
	            },
	            cache: false
	        });	
   		}
	});


	$('input.amount').change(function () {
   		//alert(this.value);
   		var amountItem1 = 0;
   		var amountItem2 = 0; 
   		var amountItem3 = 0;
   		var amount4 = 0;
   		var total=  0;

   		<?php if(isset($Invoiceitem) && sizeof($Invoiceitem)>0) { ?>

	   		$('#invoice tbody tr').each(function() {
		      var row = $(this).closest("tr");
		      var particulars = row.find(".particulars").val();
		      var amount = parseFloat(row.find(".amount").val());
		      total = total + amount;
		      //console.log("sdsds" + total);

		      var tr = $('#invoice tr:last');
		    	var discount = parseFloat(tr.find('.amount').val()); 
		    	total = +total - +discount;
			    $('#total').html(total);

		    });
	   		
	   		//finding last value(DISCOUNT)

	   		

		<?php }else{ ?>

			if ($("#amount1").length){  
	   			amountItem1 = $('#amount1').val();
	   		}
	   		if ($("#amount2").length){  
	   			amountItem2 = $('#amount2').val();
	   		}
	   		
	   		if ($("#amount3").length){  
	   			amountItem3 = $('#amount3').val();
	   		}
	   		
	   		amountItem4 = $('#amount4').val();
			var total = +amountItem1 + +amountItem2 + +amountItem3 - +amountItem4;
			$('#total').html(total);

		<?php } ?>
	    
		

	});
	
});

</script>
<style type="text/css">
	.page-breadcrumb{
		display: none;
	}

	@media print {
	  button {
	    display: none !important;
	  }
	  input,
	  textarea {
	    border: none !important;
	    box-shadow: none !important;
	    outline: none !important;
	    resize: none;
	  }
	  i{
	  	display: none !important;
	  }
	  .footer-inner{
	  	display: none !important;
	  }
	}
</style>



<div class="row">
	<div class="col-md-4" style="margin-bottom: 20px;">
		<select class="form-control input" data-placeholder="Select College" name="college_id" id="company_name">
			<option value="0">Hayarus Education Trust</option>
			<option value="1">Hayarus Solutions</option>
		</select>
	</div>

    <div class="col-md-12">
        <div class="invoice">
				<div class="row invoice-logo">
					<div class="col-xs-6 invoice-logo-space">
						<img src="<?php echo $this->webroot; ?>img/trust-logo.png" alt="" id="companyImage" width ="200" />
					</div>
					<div class="col-xs-6">
						<p>
							Invoice # <?php echo $Invoice['Invoice']['invoice_no']; ?>  <span style="font-size: 15px;">Date: <?php echo date('d/m/Y', strtotime($Invoice['Invoice']['created']));  ?></span>
							<!-- <span class="muted">
								Consectetuer adipiscing elit
							</span> -->
						</p>
					</div>
				</div>
				<hr/>
				<div class="row"> <?php //pr($student); ?>
					<div class="col-xs-4">
						<h4 style="margin-left: 9px;">To:</h4>
						<ul class="list-unstyled">
							<li>
								<?php echo $this->Form->input('invoice_id', array('class' => 'form-control input-medium', 'label' => false, 'required' => false, 'value' => $Invoice['Invoice']['id'], 'type' => 'hidden'));?>

								<?php echo $this->Form->input('invoice_no', array('class' => 'form-control input-medium', 'label' => false, 'required' => false, 'value' => $Invoice['Invoice']['invoice_no'], 'type' => 'hidden'));?>

								<textarea rows="5" cols="50" name="data['Invoice'][name]" class="form-control " value="" id="issued_to"><?php echo $Invoice['Invoice']['issued_to']; ?></textarea>
								
							</li>
							
						</ul>
					</div>
					<div class="col-xs-6">
						<h4 style="margin-left: 9px;">Course Details:</h4>
						<ul class="list-unstyled">
							<li>
								<textarea rows="5" cols="50" name="data['Invoice'][about]" class="form-control " value="" id="about"><?php  if(!empty($Invoice['Invoice']['about'])) { echo $Invoice['Invoice']['about']; }else { echo ""; } ?></textarea>
							</li>
							
						</ul>
					</div>
					
				</div>
				<div class="row">
					<div class="col-xs-12">
						<table class="table table-striped table-hover" id="invoice">
						<thead>
						<tr>
							<th>#</th>
							<th>Particulars</th>
							<th></th>
							<th>Amount</th>
							<th></th>
						</tr>
						</thead>
						<tbody>
						<?php $subtotal = 0; $total = 0; $discountItem = 0; $size = sizeof($Invoiceitem); //pr(sizeof($Invoiceitem));

						if(isset($Invoiceitem) && sizeof($Invoiceitem)>0) {   ?>	
						<?php   $slno=0;  
						foreach ($Invoiceitem as $key => $value):  $slno++;    ?>
							<tr id="row1">
							<td><?php echo $slno; ?></td>
							<td width="70%"><input type="text" name="data[Invoice][particulars<?php echo $slno; ?>]" aria-controls="sample_editable_1" class="form-control particulars" value="<?php echo $value['Invoiceitem']['particular']; ?>" id="particulars<?php echo $slno; ?>"></td>
							<td></td>
							<td><input type="text" name="data[Invoice][amount<?php echo $slno; ?>]" aria-controls="sample_editable_1" class="form-control amount" value="<?php echo $value['Invoiceitem']['amount']; ?>" id="amount<?php echo $slno; ?>"></td>
							<!-- <td><i class="fa fa-times-circle" style="font-size: 18px; color: #35aa47; padding: 8px; cursor: pointer;"></i></td> -->
						</tr>
						<?php   if($key != $size-1 ) {
							$total = $total + $value['Invoiceitem']['amount'];
							}
						 endforeach; 
						 $discountItem = $Invoiceitem[$size-1]['Invoiceitem']['amount']; 
						  
						 $subtotal = ($total-$discountItem); 
						} else{   ?>	

							<tr id="row1">
							<td>1</td>
							<td width="70%"><input type="text" name="data[Invoice][particulars1]" aria-controls="sample_editable_1" class="form-control particulars" value="" id="particulars1"></td>
							<td></td>
							<td><input type="text" name="data[Invoice][amount1]" aria-controls="sample_editable_1" class="form-control amount" value="" id="amount1"></td>
							<td><i class="fa fa-times-circle" style="font-size: 18px; color: #35aa47; padding: 8px; cursor: pointer;"></i></td>
						</tr>
						<tr id="row2">
							<td>2</td>
							<td width="70%"><input type="text" name="data[Invoice][particulars2]" aria-controls="sample_editable_1" class="form-control particulars" value="" id="particulars2"></td>
							<td></td>
							<td><input type="text" name="data[Invoice][amount2]" aria-controls="sample_editable_1" class="form-control amount" value="" id="amount2"></td>
							<td><i class="fa fa-times-circle" style="font-size: 18px; color: #35aa47; padding: 8px; cursor: pointer;"></i></td>
						</tr>
						<tr id="row3">
							<td>3</td>
							<td width="70%"><input type="text" name="data[Invoice][particulars3]" aria-controls="sample_editable_1" class="form-control particulars" value="" id="particulars3"></td>
							<td></td>
							<td><input type="text" name="data[Invoice][amount3]" aria-controls="sample_editable_1" class="form-control amount" value="" id="amount3"></td>
							<td><i class="fa fa-times-circle" style="font-size: 18px; color: #35aa47; padding: 8px; cursor: pointer;"></i></td>
						</tr>

						<tr id="row4">
							<td></td>
							<td width="70%"><input type="text" name="data[Invoice][particulars4]" aria-controls="sample_editable_1" class="form-control particulars" value="Scholarship"  id="particulars4"></td>
							<td></td>
							<td><input type="text" name="data[Invoice][amount4]" aria-controls="sample_editable_1" class="form-control amount" value="" id="amount4"></td>
							<td></td>
						</tr>

						<?php } ?>
						
						</tbody>
						</table>
					</div>
				</div>
				</br>
				<div class="row">
					<div class="col-xs-4">
						<div class="well" id="addressHayarusTrust">
							<address>
							<strong>Hayarus Education Trust</strong><br/>
							Level 2, Nellimoottil Building<br/>
							Near KSRTC Bus Stand, Pathanamthitta<br/>
							Phone: 9048 55 1110, 9048 56 1110 </address>
							<address>
							
							</address>
						</div>

						<div class="well" id="addressHayarusSolutions" style="display: none;">
							<address>
							<strong>Hayarus Solutions</strong><br/>
							Level 1, Chakkalayil Complex<br/>
							Aruthooti P.O, Kottayam<br/>
							Phone:  9048 57 1110</address>
							<address>
							
							</address>
						</div>

					</div>
					<div class="col-xs-8 invoice-block">

						<ul class="list-unstyled amounts">
							
							<li>
								<strong>Grand Total:</strong> <i class="fa fa-inr" aria-hidden="true" style="font-size: 12px;"></i><span id="total"><?php echo $subtotal;  ?></span>
							</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>
								<strong>Signature <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></strong> 
							</li>
						</ul>
						<br/>
						<a class="btn btn-lg blue hidden-print" id="print_now">Print <i class="fa fa-print"></i></a>
						<!-- <a class="btn btn-lg green hidden-print">Submit Your Invoice <i class="fa fa-check"></i></a> -->
					</div>
				</div>
			</div>
    </div>
</div>