<?php $this->Html->addCrumb('Student', '/admin/students'); $paginationVariables = $this->Paginator->params();
$groupId = $this->Session->read('Auth.Group.id');
$user = $this->Session->read('Auth.User');
?>
<?php $labelCls = array("1"=> "label-gray", "2"=> "label-purple", "3"=> "label-dark", "4" => "label-info",  "5" => "label-warning" , "6" => "label-danger" , "7" => "label-success" , "10" => "label-purple" , "11" => "label-warning" , "8" => "label-danger"); ?>

<?php 
 $name = isset($name) ? $name : "";
 $phone = isset($phone) ? $phone : "";
 $school = isset($school) ? $school : "";
 $status_id = isset($status_id) ? $status_id : "";
?>

<style type="text/css">
  .label-gray {
    background-color: #b2a0b3;
    background-image: none !important;
  }

  .label-purple {
    background-color: #bf5ec5;
    background-image: none !important;
  }
  .label-dark {
    background-color: #852b99;
    background-image: none !important;
  }

</style>
<!-- <meta http-equiv="refresh" content="15" /> -->
<div style='padding-bottom:10px;'><?php echo $this->Session->flash(); ?></div>

<script type="text/javascript">
  $(document).ready(function(){
    $('#importexcel').on('change',function(){ 
        var fileExtension = ['csv'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            alert("Only CSV format allowed");
        }else{
          $('#StudentImportForm').submit();
        }
      
    });
  });
</script>
<div class="row">
    <div class="col-md-12">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
	                <i class="fa fa-list-alt"></i><?php echo __('Students'); ?>   <span class="badge badge-warning">  <?php echo $studentCount; ?></span>             </div>
                <!-- <div class="tools">
    		        <a href="javascript:;" class="collapse"></a>
            	</div> -->
            </div>
            <div class="portlet-body"><br />
                <div class="table-toolbar">
                    
                    <div class="btn-group pull-right">
                        <div class="btn-group" style="padding-right:15px;">
	                        <?php echo $this->Html->link(__('New Student <i class="fa fa-plus"></i>'), array('action' => 'add'), array('class' => 'btn green','escape' => FALSE)); ?> 
                        </div>

                        
                        <div class="btn-group" style="padding-right:15px;">
                        <form action="<?php echo $this->webroot; ?>admin/Students/import/" class="form-horizontal" id="StudentImportForm" method="post" accept-charset="utf-8" novalidate="novalidate" enctype="multipart/form-data">
                            <div class="fileinput fileinput-new" data-provides="fileinput" bis_skin_checked="1">
                                <span class="btn yellow btn-file">
                                    <span class="fileinput-new"> Import Student Data </span>
                                    <input type="hidden"><input type="file" name="data[Student]" id="importexcel"> </span>
                                <span class="fileinput-filename"> </span> &nbsp;
                                
                            </div>
                        </form>   
                        </div>
                      
                    </div>
                </div>

                <div class="row" style="margin-left: 13px; margin-top: 50px;">
                   <?php echo $this->Form->create('', array('type' => 'get','role' => 'form', 'url' => array('controller' => strtolower('Students'), 'action' => 'index', 'admin' => true))); ?>
                   <?php //if($groupId != 4 && $groupId == 1){ ?>
                    <div class="table-toolbar">
                        <div class="col-md-3 col-sm-12">
                          <div class="dataTables_filter" id="sample_editable_1_filter">
                            <label>Name: <input type="text" name="name" aria-controls="sample_editable_1" class="form-control input-medium" value="<?php echo $name; ?>"></label>
                          </div>
                        </div>

                        <div class="col-md-3 col-sm-12">
                          <div class="dataTables_filter" id="sample_editable_1_filter">
                            <label>Phone: <input type="text" name="phone" aria-controls="sample_editable_1" class="form-control input-medium" value="<?php echo $phone; ?>"></label>
                          </div>
                        </div>

                        <div class="col-md-3 col-sm-12">
                          <div class="dataTables_filter" id="sample_editable_1_filter">
                            <label>Added By: <?php echo $this->Form->input('user_id', array('class' => 'form-control input-medium', 'label' => false, 'required' => false, 'empty'=>'Select', 'selected' => $user_id));?></label>
                          </div>
                        </div>

                        <div class="col-md-3 col-sm-12">
                          <div class="dataTables_filter" id="sample_editable_1_filter">
                            <label>District: <input type="text" name="district" aria-controls="sample_editable_1" class="form-control input-medium" value="<?php echo $district; ?>"></label>
                          </div>
                        </div>

                        <div class="col-md-3 col-sm-12">
                          <div class="dataTables_filter" id="sample_editable_1_filter">
                            <label>Status: <?php echo $this->Form->input('status_id', array('class' => 'form-control input-medium', 'label' => false, 'required' => false, 'empty'=>'Select', 'selected' => $status_id));?></label>
                          </div>
                        </div>

                        <div class="col-md-3 col-sm-12">
                          <div class="dataTables_filter" id="sample_editable_1_filter">
                            <label>School: <input type="text" name="school" aria-controls="sample_editable_1" class="form-control input-medium" value="<?php echo $school; ?>"></label>
                          </div>
                        </div>


                    <div class="col-md-3 col-sm-12">
                      <div class="dataTables_filter" id="sample_editable_1_filter">
                      <label >Added Date:
                      <input class="form-control form-control-inline input-medium date-picker" size="16" name="created" type="text" value="<?php echo  isset($created) ? $created : ""; ?>"/></label>
                      </div>
                    </div>

                        <div class="col-md-3 col-sm-12">
                          <button class="btn green" type="submit" onclick="this.form.submit()" style="    margin-top: 30px;float: left;">Search <i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    <?php //} ?>
                  </form>
                </div>
                <br />
                <div class="table-responsive">
                <?php //if($groupId != 4 && $groupId == 1){ ?>
                <table class="table table-bordered table-striped table-condensed flip-content" id="sample_1">
                  <thead>
                      <tr>
                        <th>#</th>  
                        <th><?php echo $this->Paginator->sort('name'); ?></th>
                        <th><?php echo $this->Paginator->sort('phone'); ?></th>
                        <th><?php echo $this->Paginator->sort('school'); ?></th>
                        <!-- <th>division/ Course</th> -->
                        <th><?php echo $this->Paginator->sort('district'); ?></th>
                        <th>Added By</th>
                        <th ><?php echo $this->Paginator->sort('status_id'); ?></th>
                        <th>Person Attending</th>
                        <th>Attend</th>
                        <?php if($groupId != 4 && $groupId == 1){ ?>
                        <th>Edit</th>
                        <th>Delete</th>
                        <?php } ?>
                      </tr>
                  </thead>
                  <?php  if(isset($students) && sizeof($students)>0) {?>
                  <tbody>
                  <?php $slno=0; foreach ($students as $student): $slno++?>
                  <tr>
                     <td><?php echo $slno+$paginationVariables['limit']*($paginationVariables['page']-1); ?></td>
        							<td><?php echo h($student['Student']['name']); ?>&nbsp;</td>
                  		<td><?php echo h($student['Student']['phone']); ?>&nbsp;</td>
                  		<td><?php echo h($student['Student']['school']); ?>&nbsp;</td>
                     <!--  <td><?php echo h($student['Student']['division']); ?>&nbsp;</td> -->
                  		<td><?php echo h($student['Student']['district']); ?>&nbsp;</td>

                      <?php $added_user = ($student['Student']['user_id'] != 0) ? $users[$student['Student']['user_id']] : "--"; ?>

                      <td><?php echo $added_user; ?>&nbsp;</td>
                  		
                      <td><span class="label label-sm <?php echo $labelCls[$student['Student']['status_id']]; ?>">
											     <?php echo $student['Status']['name']; ?></span>
		                  </td>
                      
                      <?php $updated_user = ($student['Student']['updated_user'] != 0) ? $users[$student['Student']['updated_user']] : "--"; ?>

                      <td><?php echo $updated_user; ?>&nbsp;</td>

                      <td>
                        <?php if($student['Student']['type'] == 1   || ($student['Student']['invoice_id'] != 0 && $groupId != 1)  || ( !empty($student['Student']['updated_user']) && $student['Student']['updated_user'] != $user['id'] ) ) { $disabled = 'disabled'; }else{ $disabled = ''; } ?>

                        <?php if($student['Student']['invoice_id'] != 0 && $groupId != 1){ echo $this->Html->link(__('<i class="fa fa-edit"></i> Invoice'), array('controller' => 'invoices','action' => 'view', $student['Student']['invoice_id']), array('class' => 'btn default btn-xs yellow','escape' => FALSE));
                          }else{ ?>

                          <?php echo $this->Html->link(__('<i class="fa fa-phone"></i> Attend'), array('action' => 'attend', $student['Student']['id']), array('class' => 'btn default btn-xs green','escape' => FALSE, 'disabled' => $disabled )); } ?>
                      </td>
                      <?php if($groupId != 4 && $groupId == 1){ ?>
											<td>

													<?php if($student['Student']['invoice_id'] == 0){ echo $this->Html->link(__('<i class="fa fa-edit"></i> Edit'), array('action' => 'edit', $student['Student']['id']), array('class' => 'btn default btn-xs purple','escape' => FALSE)); 
                          }else{ echo $this->Html->link(__('<i class="fa fa-edit"></i> Invoice'), array('controller' => 'invoices','action' => 'view', $student['Student']['invoice_id']), array('class' => 'btn default btn-xs yellow','escape' => FALSE));}  ?>
											</td>
											<td>
													<?php echo $this->Form->postLink(__('<i class="fa fa-trash-o"></i> Delete'), array('action' => 'delete', $student['Student']['id']), array('confirm' => 'Are you sure you want to delete this?','class' => 'btn red btn-xs black','escape' => FALSE)); ?>
											</td>
                      <?php } ?>
										</tr>
									<?php endforeach; ?>
                   <?php } else {?>
                      <tr><td colspan='12' align='center'>No Records Found.</td></tr>
                  <?php }?>
                  </tbody>
                </table>
                <?php //} ?>

              </div>
              <?php //if($groupId != 4 && $groupId == 1){ ?>
                <div class="row">
    	            <div class="col-md-5 col-sm-12" style="margin-top:10px;">
                      <div id="sample_1_length" class="dataTables_length"> 
                        <?php echo $this->Form->create('', array( 'url' => array('controller' => strtolower('Students'), 'action' => 'index'))); ?>
                            <label>Show 
                          			<?php echo $this->Form->input('limit', array('name' => 'data[Student][limit]','type' => 'select', 'class' => 'form-control input-xsmall', 'label' => false,  'required' => false,'hiddenField' => false, 'style' => 'display:inline-block !important; padding-right:0px', 'onchange' => 'this.form.submit()', 'options' =>$default_limit_dropdown, 'default' => $limit, 'div' => false));?>
                            records
                            </label>
                            <?php echo $this->Form->end(); ?>
                					</div>
                				</div>
                        <div class="col-md-7 col-sm-12"> 
                          <div class="dataTables_paginate paging_bootstrap">
                            <?php $totalItem = $this->Paginator->counter('{:count}')?><?php $currentItem = $this->Paginator->counter('{:current}')?><?php if($totalItem>$currentItem) {?>
                            <ul class="pagination" style="visibility: visible;">
                                <li class="prev disabled">
                                			<?php echo $this->Paginator->prev('< ' . __(''), array(), null, array('class' => 'prev disabled'));?>
	
                                </li>
                                <li>
                                			<?php		echo $this->Paginator->numbers(array('separator' => ''));	?>
                                </li>
                                <li class="next disabled">
                                			<?php		echo $this->Paginator->next(__('') . ' >', array(), null, array('class' => 'next'));	?>
                                </li>
                            </ul>
                      <?php }?>
    						        </div>
                      </div>
    				    </div>
                <?php //} ?>
    			</div>
    	</div>
    <!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
