<?php $this->Html->addCrumb('Student', '/admin/students'); ?>
<?php $this->Html->addCrumb('Attend', ''); ?>
<div style='padding-bottom:10px;'><?php echo $this->Session->flash(); ?></div>
<script type="text/javascript">
$(document).ready(function(){
	var error1 = $('.alert-danger');
	$('#StudentAdminEditForm').validate({
		errorElement: 'span', //default input error message container
		errorClass: 'help-block', // default input error message class
		focusInvalid: false, // do not focus the last invalid input
		ignore: "",
		rules:{
		"data[Student][id]" : {required : true},
		"data[Student][name]" : {required : true},
		"data[Student][phone]" : {required : true},
		"data[Student][address]" : {required : false},
		"data[Student][notes]" : {required : false},
		"data[Student][view_status]" : {required : false},
		"data[Student][status_id]" : {required : false},
		},
		messages:{
		"data[Student][id]" : {required :"Please enter id."},
		"data[Student][name]" : {required :"Please enter name."},
		"data[Student][phone]" : {required :"Please enter phone."},
		"data[Student][address]" : {required :"Please enter address."},
		"data[Student][notes]" : {required :"Please enter notes."},
		"data[Student][view_status]" : {required :"Please enter view_status."},
		"data[Student][status_id]" : {required :"Please enter status_id."},
		},

		invalidHandler: function (event, validator) { //display error alert on form submit              
			//success1.hide();
			error1.show();
			//App.scrollTo(error1, -200);
		},

		highlight: function (element) { // hightlight error inputs
			$(element)
				.closest('.form-group').addClass('has-error'); // set error class to the control group
		},

		unhighlight: function (element) { // revert the change done by hightlight
			$(element)
				.closest('.form-group').removeClass('has-error'); // set error class to the control group
		},

		success: function (label) {
			label
				.closest('.form-group').removeClass('has-error'); // set success class to the control group
			label
				.closest('.form-group').removeClass('error');
		},
	});
});

</script>

<div class="row">
    <div class="col-md-12">
        <div class="tab-content">
            <div class="tab-pane active" id="tab_0">
                 <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                        	<i class="fa fa-reorder"></i><?php echo __('Attend Student'); ?>                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form students">
                    <div class="alert alert-danger display-hide">
                        <button data-close="alert" class="close"></button>
                        You have some form errors. Please check below.
                    </div>
                    <!-- BEGIN FORM-->
                    <?php echo $this->Form->create('Student', array('class' => 'form-horizontal')); ?>
                    <div class="form-body">  
                    	<div class="form-group">
	                        	<label class="col-md-3 control-label" for="">&nbsp;</label>
	                            <div class="col-md-4">
	                                <div class="input text">
										<span class="required" style="color:#F00"> *</span>= Required
									</div>
	                            </div>
	                        </div>                                
                        <div class="form-group">
							 <?php //echo $this->Form->label('id<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('id', array('class' => 'form-control', 'label' => false, 'required' => false));?>

								<?php echo $this->Form->input('invoice_id', array('class' => 'form-control', 'label' => false, 'required' => false, 'type' => 'hidden', 'value' => $this->request->data['Student']['invoice_id']));?>

							</div>
						</div>
						<div class="form-group">
							 <?php echo $this->Form->label('name<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('name', array('class' => 'form-control unselectable', 'label' => false, 'required' => false, 'readonly' => true));?>
							</div>
						</div>
						<div class="form-group">
							 <?php echo $this->Form->label('phone<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('phone', array('class' => 'form-control', 'label' => false, 'required' => false, 'readonly' => true));?>
							</div>
						</div>

						<div class="form-group">
							 <?php echo $this->Form->label('school' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('school', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>

						<div class="form-group">
							 <?php echo $this->Form->label('division/ Course' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('division', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>

						<div class="form-group">
							 <?php echo $this->Form->label('district' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('district', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>

						
						<div class="form-group">
							 <?php echo $this->Form->label('address' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('address', array('class' => 'form-control', 'label' => false, 'required' => false, 'type' => 'textarea'));?>
							</div>
						</div>
						<div class="form-group">
							 <?php echo $this->Form->label('notes' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('notes', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>

						<div class="form-group">
							 <?php echo $this->Form->label('status' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('status_id', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>

						<div class="form-group">
								<label class="col-md-3 control-label">Course Preferred</label>
								<div class='col-md-4'>
								
								<div class="input-icon">

									<select class="form-control input select2me" data-placeholder="Select Course" name="data[Student][course_id]">
										<option value=""></option>
										<?php foreach($courses as $key=> $val){ ?>
												<option value="<?php echo $key ?>" <?php if(isset($this->request->data['Student']['course_id']) && ($this->request->data['Student']['course_id'] == $key)){echo "selected"; } ?>><?php echo $val; ?></option>
										<?php } ?>
									</select>

								</div>
								</div>
							
						</div>

						<div class="form-group">
								<label class="col-md-3 control-label">College Preferred</label>
								<div class='col-md-4'>
								
								<div class="input-icon">

									<select class="form-control input select2me" data-placeholder="Select College" name="data[Student][college_id]">
										<option value=""></option>
										<?php foreach($colleges as $key=> $val){ ?>
												<option value="<?php echo $key ?>" <?php if(isset($this->request->data['Student']['college_id']) && ($this->request->data['Student']['college_id'] == $key)){echo "selected"; } ?>><?php echo $val; ?></option>
										<?php } ?>
									</select>

								</div>
								</div>
							
						</div>


                            <div class="form-actions fluid">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn blue">Submit</button>
                                    <?php if($groupId == 4){ ?>
                                    	<button type="button" class="btn default" onclick="window.location='<?php echo $this->webroot.'admin/students/admission'; ?>'">Cancel</button>
                                    <?php }else{ ?>	
                                    <button type="button" class="btn default" onclick="window.location='<?php echo $prev_url; ?>'">Cancel</button>
                                	<?php } ?>	
                                </div>
                            </div>
                        </div>
                        <?php echo $this->Form->end(); ?>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>