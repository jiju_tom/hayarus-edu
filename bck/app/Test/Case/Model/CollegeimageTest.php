<?php
App::uses('Collegeimage', 'Model');

/**
 * Collegeimage Test Case
 *
 */
class CollegeimageTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.collegeimage',
		'app.college',
		'app.location',
		'app.country',
		'app.state',
		'app.city',
		'app.student',
		'app.status',
		'app.user',
		'app.group',
		'app.expense',
		'app.invoice',
		'app.course',
		'app.areaofstudy',
		'app.courseexpense',
		'app.studentreference'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Collegeimage = ClassRegistry::init('Collegeimage');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Collegeimage);

		parent::tearDown();
	}

}
