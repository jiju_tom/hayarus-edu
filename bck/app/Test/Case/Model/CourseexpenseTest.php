<?php
App::uses('Courseexpense', 'Model');

/**
 * Courseexpense Test Case
 *
 */
class CourseexpenseTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.courseexpense',
		'app.course',
		'app.areaofstudy',
		'app.collegecourse',
		'app.college',
		'app.collegeimage',
		'app.studentreference',
		'app.invoice',
		'app.location',
		'app.city',
		'app.state',
		'app.country',
		'app.student',
		'app.status',
		'app.user',
		'app.group',
		'app.expense'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Courseexpense = ClassRegistry::init('Courseexpense');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Courseexpense);

		parent::tearDown();
	}

}
