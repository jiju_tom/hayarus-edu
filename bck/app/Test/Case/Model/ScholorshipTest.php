<?php
App::uses('Scholorship', 'Model');

/**
 * Scholorship Test Case
 *
 */
class ScholorshipTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.scholorship',
		'app.user',
		'app.group',
		'app.expense',
		'app.invoice',
		'app.college',
		'app.collegecourse',
		'app.course',
		'app.areaofstudy',
		'app.courseexpense',
		'app.studentreference',
		'app.collegeimage',
		'app.location',
		'app.city',
		'app.state',
		'app.country',
		'app.student',
		'app.status'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Scholorship = ClassRegistry::init('Scholorship');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Scholorship);

		parent::tearDown();
	}

}
