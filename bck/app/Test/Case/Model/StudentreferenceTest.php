<?php
App::uses('Studentreference', 'Model');

/**
 * Studentreference Test Case
 *
 */
class StudentreferenceTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.studentreference',
		'app.course'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Studentreference = ClassRegistry::init('Studentreference');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Studentreference);

		parent::tearDown();
	}

}
