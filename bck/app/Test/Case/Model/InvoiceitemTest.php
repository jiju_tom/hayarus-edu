<?php
App::uses('Invoiceitem', 'Model');

/**
 * Invoiceitem Test Case
 *
 */
class InvoiceitemTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.invoiceitem',
		'app.invoice',
		'app.college',
		'app.collegecourse',
		'app.course',
		'app.areaofstudy',
		'app.courseexpense',
		'app.studentreference',
		'app.collegeimage',
		'app.location',
		'app.city',
		'app.state',
		'app.country',
		'app.student',
		'app.status',
		'app.user',
		'app.group',
		'app.expense',
		'app.scholorship'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Invoiceitem = ClassRegistry::init('Invoiceitem');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Invoiceitem);

		parent::tearDown();
	}

}
