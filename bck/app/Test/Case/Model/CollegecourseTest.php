<?php
App::uses('Collegecourse', 'Model');

/**
 * Collegecourse Test Case
 *
 */
class CollegecourseTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.collegecourse',
		'app.college',
		'app.location',
		'app.country',
		'app.state',
		'app.city',
		'app.student',
		'app.status',
		'app.user',
		'app.group',
		'app.expense',
		'app.invoice',
		'app.course',
		'app.courseexpense',
		'app.studentreference',
		'app.collegeimage',
		'app.areaofstudy'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Collegecourse = ClassRegistry::init('Collegecourse');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Collegecourse);

		parent::tearDown();
	}

}
