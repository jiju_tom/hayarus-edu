<?php
App::uses('StudentreferencesController', 'Controller');

/**
 * StudentreferencesController Test Case
 *
 */
class StudentreferencesControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.studentreference',
		'app.course',
		'app.collegecourse',
		'app.college',
		'app.location',
		'app.city',
		'app.state',
		'app.country',
		'app.student',
		'app.status',
		'app.user',
		'app.group',
		'app.expense',
		'app.invoice',
		'app.collegeimage',
		'app.areaofstudy',
		'app.courseexpense'
	);

/**
 * testAdminIndex method
 *
 * @return void
 */
	public function testAdminIndex() {
		$this->markTestIncomplete('testAdminIndex not implemented.');
	}

/**
 * testAdminView method
 *
 * @return void
 */
	public function testAdminView() {
		$this->markTestIncomplete('testAdminView not implemented.');
	}

/**
 * testAdminAdd method
 *
 * @return void
 */
	public function testAdminAdd() {
		$this->markTestIncomplete('testAdminAdd not implemented.');
	}

/**
 * testAdminEdit method
 *
 * @return void
 */
	public function testAdminEdit() {
		$this->markTestIncomplete('testAdminEdit not implemented.');
	}

/**
 * testAdminDelete method
 *
 * @return void
 */
	public function testAdminDelete() {
		$this->markTestIncomplete('testAdminDelete not implemented.');
	}

/**
 * testAdminExport method
 *
 * @return void
 */
	public function testAdminExport() {
		$this->markTestIncomplete('testAdminExport not implemented.');
	}

}
