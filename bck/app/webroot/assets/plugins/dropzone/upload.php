<?php 
$ds          = DIRECTORY_SEPARATOR; 

$dbHost = 'localhost';
$dbUsername = 'root';
$dbPassword = '';
$dbName = 'hayarus_edu';
//connect with the database
$conn = new mysqli($dbHost, $dbUsername, $dbPassword, $dbName);
if($conn->connect_errno){
    echo "Failed to connect to MySQL: (" . $conn->connect_errno . ") " . $conn->connect_error;
}

$storeFolder = 'uploads'; 

$college_id = $_POST['data']['college_id'];
 
if (!empty($_FILES)) {
 
    $tempFile = $_FILES['file']['tmp_name'];         
 
    $targetPath = dirname( __FILE__ ) . $ds. $storeFolder . $ds; 

    $filename = 'img-'.rand(0,9999999).'-'.$_FILES['file']['name'];
 
    $targetFile =  $targetPath. $filename; 
 
    if(move_uploaded_file($tempFile,$targetFile)){
        $conn->query("INSERT INTO he_collegeimages (college_id, name, created) VALUES('".$college_id."','".$filename."','".date("Y-m-d H:i:s")."')");
    }
 
} else {                                                           
    $result  = array();
 
    $files = scandir($storeFolder);                 //1
    if ( false!==$files ) {
        foreach ( $files as $file ) {
            if ( '.'!=$file && '..'!=$file) {       //2
                $obj['name'] = $file;
                $obj['size'] = filesize($storeFolder.$ds.$file);
                $result[] = $obj;
            }
        }
    }
     
    header('Content-type: text/json');              //3
    header('Content-type: application/json');
    echo json_encode($result);
}
?>